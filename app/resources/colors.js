export default {
    header_sub: '#1f7fa0',
    header_sub2: '#f2bfb8',
    itemContent: '#4d4d4d',
    subItemContent: '#b3b3b3',
    subItemContent2: '#898989',
    subItemContent3: '#999999',
    itemNameBack: '#f2f2f2',
    dangerColor: '#dd1c24',
    itemSelected: '#3cce81'
}