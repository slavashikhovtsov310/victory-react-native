import * as actions from "../actions/action-types";
import { Alert } from "react-native";

export function setIds(seasonId, matchId) {
    return {
        type: actions.SET_IDS,
        seasonId: seasonId,
        matchId: matchId
    }
}

export function setScoreDetailHeader(localTeam, localTeamEvents, localTeamScore, currentTime, visitorTeam, visitorTeamEvents, visitorTeamScore) {
    return {
        type: actions.SET_SCOREDETAIL_HEADER,
        localTeam: localTeam,
        localTeamEvents: localTeamEvents,
        localTeamScore: localTeamScore,
        currentTime: currentTime,
        visitorTeam: visitorTeam,
        visitorTeamEvents: visitorTeamEvents,
        visitorTeamScore: visitorTeamScore
    }
}

export function setScoreDetailLeagueAndSeason(leagueName, seasonName){
    return{
        type: actions.SET_LEAGUE_AND_SEASON,
        leagueName: leagueName,
        seasonName: seasonName
    }
}

