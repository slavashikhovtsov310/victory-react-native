import * as actions from "../actions/action-types";
export function setProductId(id) {
    return {
        type: actions.SET_PRODUCT_ID,
        productId: id
    }
}
