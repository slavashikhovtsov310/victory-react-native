import * as actions from "../actions/action-types";

export function setCartList(data) {
    return {
        type: actions.SET_CARTLIST,
        list: data
    }
}
