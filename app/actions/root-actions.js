import * as actions from "../actions/action-types";

export function controlProgress(isShowing) {
    return {
        type: actions.PROGRESS,
        progress: isShowing
    }
}
export function switchLanguage(lang) {
    return {
        type: actions.SWITCHLANGUAGE,
        lang: lang
    }
}
export function setMyFavoriteLeagues(data) {
    return {
        type: actions.SET_MY_FAVORITE_LEAGUES,
        favLeagues: data
    }
} 
export function MyFavoriteLeaguesChanged(flag) {
    return {
        type: actions.CHANGE_MY_FAVORITE_LEAGUES,
        flag: flag
    }
}
