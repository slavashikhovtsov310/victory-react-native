import * as actions from "../actions/action-types";
import { Alert } from "react-native";


export function get_Stats_Fixtures_ByLeagues() {
    return {
        type: actions.GET_STATS_FIXTURES_BY_LEAGUE,
    }
}


export function set_Stats_Fixtures_ByLeagues(res, leagueIndex) {
    return {
        type: actions.GET_STATS_FIXTURES_BY_LEAGUE_SUCCESS,
        data: res,
        leagueIndex: leagueIndex
    }
}

