import * as actions from "../actions/action-types";
export function setWishList(list) {
    return {
        type: actions.SET_WISHLIST,
        list: list
    }
}