import * as actions from "../actions/action-types";
export function setFilter(kit, club, internationalTeams, gender, size, brand, sortBy) {
    return {
        type: actions.SET_FILTER,
        kit: kit,
        club: club,
        internationalTeams: internationalTeams,
        gender: gender,
        size: size,
        brand: brand,
        sortBy: sortBy
    }
}

export function isFilterChanged(flag) {
    return {
        type: actions.FILTER_CHANGE,
        isChanged: flag
    }
}