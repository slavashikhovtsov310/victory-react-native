import * as actions from "../actions/action-types";

export function setCookie(cookie) {
    return {
        type: actions.SET_COOKIE,
        cookie: cookie
    }
}