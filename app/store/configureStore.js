import { autoRehydrate, persistStore } from 'redux-persist-immutable';
import { combineReducers } from 'redux-immutable';
import { Platform, AsyncStorage } from 'react-native';
import createActionBuffer from 'redux-action-buffer';
import { REHYDRATE } from 'redux-persist/constants';
import Immutable from 'immutable';
import { applyMiddleware, compose, createStore } from 'redux';
import { FilesystemStorage } from 'redux-persist-filesystem-storage'
import loginReducer from '../reducers/loginReducer';
import rootReducer from '../reducers/rootReducer';
import statReducer from '../reducers/statReducer';
import scoreDetailReducer from '../reducers/scoreDetailReducer';
import subShopReducer from '../reducers/subShopReducer';
import wishListReducer from '../reducers/wishListReducer';
import filterReducer from '../reducers/filterReducer';
import cartReducer from '../reducers/cartReducer';
import cartWebViewReducer from '../reducers/cartWebViewReducer';
import createSagaMiddleware from 'redux-saga';
import * as loginSaga from '../saga/login-saga';
import * as statSaga from '../saga/stat-saga';

const combinedReducers = combineReducers({
  root: rootReducer,
  login: loginReducer,
  stat: statReducer,
  scoreDetail: scoreDetailReducer,
  subShop: subShopReducer,
  wishList: wishListReducer,
  filter: filterReducer,
  cart: cartReducer,
  cartWebView: cartWebViewReducer
});

const initialState = new Immutable.Map({
  root: Immutable.Map({
    progress: undefined,
    lang: '',
    favLeagues: [],
    isChanged: false
  }),
  login: Immutable.Map({
    isLoggedIn: false,
    idUser: '',
    loginError: {},
    email: '',
    password: '',
  }),
  stat: Immutable.Map({
    worldcup: {},
    uaeleague: {},
    uaedivision1: {},
    uaearabiangulfcup: {},
    ksaproleague: {},
    ksadivision1: {},
    ksakingscup: {},
    esplaliga: {},
    itaseriea: {},
    fraligue1: {},
    gbrpremierleague: {}
  }),
  scoreDetail: Immutable.Map({
    seasonId: '',
    matchId: '',
    localTeam: {},
    localTeamEvents: [],
    localTeamScore: '',
    currentTime: '',
    visitorTeam: {},
    visitorTeamEvents: [],
    visitorTeamScore: '',
    leagueName: '',
    seasonName: ''
  }),
  subShop: Immutable.Map({
    productId: ''
  }),
  wishList: Immutable.Map({
    list: []
  }),
  filter: Immutable.Map({
    kit: [],
    club: [],
    internationalTeams: [],
    gender: [],
    size: [],
    brand: [],
    sortBy: 'ToHigh',
    isChanged: false
  }),
  cart: Immutable.Map({
    list: []
  }),
  cartWebView: Immutable.Map({
    cookie: ''
  }),
});

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    combinedReducers,
    initialState,
    compose(
      applyMiddleware(
        sagaMiddleware,
        createActionBuffer(REHYDRATE)
      ),
      autoRehydrate({ log: true })
    )
  );

  persistStore(store, {
    storage: AsyncStorage, /*Platform.OS === 'android' ? AndroidFileStorage : AsyncStorage,*/
    blacklist: ['root'],
  });
  return {
    ...store,
    runSaga: [
      sagaMiddleware.run(loginSaga.loginFlow),
      sagaMiddleware.run(statSaga.statAllScoresFlow),
    ],
  };
}

