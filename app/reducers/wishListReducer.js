import * as actions from "../actions/action-types";

export default function wishListReducer(state, action = {}) {
    switch (action.type) {
        case actions.SET_WISHLIST:
            return state.withMutations(state => state
                .set('list', action.list));
        default:
            return state
    }
}