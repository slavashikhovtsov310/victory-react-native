import * as actions from "../actions/action-types";

const leagues = [
    ["Worldcup", "732"],
    ["UAE - UAE League", "959"],
    ["UAE - Division 1", "962"],
    ["UAE - Arabian Gulf Cup", "965"],
    ["KSA - Pro League", "944"],
    ["KSA - Division 1", "947"],
    ["KSA - Kings Cup", "950"],
    ["ESP - La Liga", "564"],
    ["ITA - Serie A", "384"],
    ["FRA - Ligue 1", "301"],
    ["GBR - Premier League", "8"]
];
export default function statReducer(state = initialState, action = {}) {
    // console.log('reducer test: ', state);
    switch (action.type) {
        case actions.GET_STATS_FIXTURES_BY_LEAGUE_SUCCESS:
            // console.log('reducer test: ', action.leagueIndex);
            if (action.leagueIndex == 0) {
                return state.withMutations(state => state
                    .set('worldcup', action.data));
            }
            else if (action.leagueIndex == 1) {
                return state.withMutations(state => state
                    .set('uaeleague', action.data));
            }
            else if (action.leagueIndex == 2) {
                return state.withMutations(state => state
                    .set('uaedivision1', action.data));
            }
            else if (action.leagueIndex == 3) {
                return state.withMutations(state => state
                    .set('uaearabiangulfcup', action.data));
            }
            else if (action.leagueIndex == 4) {
                return state.withMutations(state => state
                    .set('ksaproleague', action.data));
            }
            else if (action.leagueIndex == 5) {
                return state.withMutations(state => state
                    .set('ksadivision1', action.data));
            }
            else if (action.leagueIndex == 6) {
                return state.withMutations(state => state
                    .set('ksakingscup', action.data));
            }
            else if (action.leagueIndex == 7) {
                return state.withMutations(state => state
                    .set('esplaliga', action.data));
            }
            else if (action.leagueIndex == 8) {
                return state.withMutations(state => state
                    .set('itaseriea', action.data));
            }
            else if (action.leagueIndex == 9) {
                return state.withMutations(state => state
                    .set('fraligue1', action.data));
            }
            else if (action.leagueIndex == 10) {
                return state.withMutations(state => state
                    .set('gbrpremierleague', action.data));
            }
        default:
            return state
    }
}
