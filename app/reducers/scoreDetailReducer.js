import * as actions from "../actions/action-types";
import { Alert } from 'react-native'

export default function scoreDetailReducer(state, action = {}) {
    switch (action.type) {
        case actions.SET_IDS:
            return state.withMutations(state => state
                .set('seasonId', action.seasonId)
                .set('matchId', action.matchId));
        case actions.SET_SCOREDETAIL_HEADER:
            return state.withMutations(state => state
                .set('localTeam', action.localTeam)
                .set('localTeamEvents', action.localTeamEvents)
                .set('localTeamScore', action.localTeamScore)
                .set('currentTime', action.currentTime)
                .set('visitorTeam', action.visitorTeam)
                .set('visitorTeamEvents', action.visitorTeamEvents)
                .set('visitorTeamScore', action.visitorTeamScore));
        case actions.SET_LEAGUE_AND_SEASON:
            return state.withMutations(state => state
                .set('leagueName', action.leagueName)
                .set('seasonName', action.seasonName));
        default:
            return state
    }
}

