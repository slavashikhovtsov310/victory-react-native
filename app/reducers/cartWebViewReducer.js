import * as actions from "../actions/action-types";

export default function cartWebViewReducer(state, action = {}) {
    switch (action.type) {
        case actions.SET_COOKIE:
            return state.withMutations(state => state
                .set('cookie', action.cookie));
        default:
            return state
    }
}