import * as actions from "../actions/action-types";

export default function CartReducer(state, action = {}) {
    switch (action.type) {
        case actions.SET_CARTLIST:
            return state.withMutations(state => state
                .set('list', action.list));
        default:
            return state
    }
}