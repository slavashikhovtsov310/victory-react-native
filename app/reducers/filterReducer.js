import * as actions from "../actions/action-types";

export default function filterReducer(state, action = {}) {
    switch (action.type) {
        case actions.SET_FILTER:
            return state.withMutations(state => state
                .set('kit', action.kit)
                .set('club', action.club)
                .set('internationalTeams', action.internationalTeams)
                .set('gender', action.gender)
                .set('size', action.size)
                .set('brand', action.brand)
                .set('sortBy', action.sortBy));
        case actions.FILTER_CHANGE:
            return state.withMutations(state => state
                .set('isChanged', action.isChanged));
        default:
            return state
    }
}