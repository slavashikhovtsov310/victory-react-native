import * as actions from "../actions/action-types";
import { Alert } from 'react-native'

export default function subShopReducer(state, action = {}) {
    switch (action.type) {
        case actions.SET_PRODUCT_ID:
            // Alert.alert('error: ' + action.error);
            return state.withMutations(state => state
                .set('productId', action.productId));
        default:
            return state
    }
}