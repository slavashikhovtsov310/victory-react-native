import { call, put, take } from "redux-saga/effects";
import * as actions from "../actions/action-types";
import * as Api from "../api";
import Base64 from "../utils/Base64";
import * as statActions from "../actions/stat-actions";
import * as rootActions from "../actions/root-actions";
import { Alert } from "react-native";

function* dispatchReadmeSuccess(res, i) {
    // if (readMe.content) {
    // const content = Base64.atob(readMe.content);
    yield put(statActions.set_Stats_Fixtures_ByLeagues(res, i));
    // return res;
    // }
}

function* get_Stats_Fixtures_ByLeagues() {
    // Alert.alert('saga test: ' + idUser);
    try {
        for (var i = 0; i < 11; i++) {
            const res = yield call(Api.get_Stats_Fixtures_ByLeagues, i);
            yield call(dispatchReadmeSuccess, res, i);
        }
    } catch (error) {
        yield put(profileActions.setError(error));
    }
}

export function* statAllScoresFlow() {
    while (true) {
        yield take(actions.GET_STATS_FIXTURES_BY_LEAGUE);
        yield put(rootActions.controlProgress(true));
        yield call(get_Stats_Fixtures_ByLeagues);
        yield put(rootActions.controlProgress(false));
    }
}