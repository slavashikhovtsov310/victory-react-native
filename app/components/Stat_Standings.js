import React from 'react';
import { Content, Text, View } from 'native-base';
import {
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import strings from '../resources/strings';
import PropTypes from 'prop-types';
import * as Immutable from '../../node_modules/immutable/dist/immutable';
import ImmutableComponent from './ImmutableComponent';
import colors from '../resources/colors';
import dimens from '../resources/dimens';
import arrow_go_icon from '../assets/icons/arrow_go.png';
import leagueLogo from '../assets/icons/rm.png';
import SearchTextInput from './SearchTextInput';
import axios from 'axios'
import null_logo from '../assets/teamlogo_null.png';
import null_Img from '../assets/playerImage_null.png';

const { width, height } = Dimensions.get('window');
const CacelToken = axios.CancelToken;
let cancel;
const leagues =
  [
    ["World Cup", "732", "5796"],
    ["UAE League", "959", "13020"],
    ["Division 1", "962", "10038"],
    ["Arabian Gulf Cup", "965", "13010"],
    ["Pro League", "944", "12941"],
    ["Division 1", "947", "13178"],
    ["Kings Cup", "950", "12001"],
    ["La Liga", "564", "13133"],
    ["Serie A", "384", "13158"],
    ["Ligue 1", "301", "12935"],
    ["Premier League", "8", "12962"]
  ];
export default class Stat_Standings extends ImmutableComponent {
  constructor(props: {}) {
    super(props);

    this.state = {
      data: Immutable.Map({
        error: '',
        showDefaultValue: true,
      }),
      isTable: false,
      table: [],
      topScorers: [],
      states: [],
    };
  }
  componentDidMount() {
    let table = [];
    let topScorers = [];
    let states = [];
    for (var i = 0; i < leagues.length; i++) {
      table.push([]);
      topScorers.push([]);
      states.push(false);
    }
    this.setState({ table: table, topScorers: topScorers, states: states });
    for (var i = 0; i < leagues.length; i++) {
      this.getStandings(i);
      this.getTopScorers(i);
    }
  }
  componentWillUnmount() {
    if (cancel != undefined)
      cancel();
  }
  getStandings(leagueIndex) {
    const query = `
                    query standings {
                      getStandings(seasonId: ${leagues[leagueIndex][2]}) {
                        teamName
                        overall {
                          gamesPlayed
                          won
                          lost
                        }
                        total {
                          points
                        }
                        status
                        teamInclude {
                          team {
                            logoPath
                          }
                        }
                      }
                    }
                `;
    const variables = {
      cancelToken: new CacelToken(function executor(c) {
        cancel = c;
      })
    };
    try {
      return axios.post('https://victory.softwarehouse.io/graphql', {
        query
      }, variables).then(res => {
        if (res.data.data.getStandings.length == 0)
          return null;
        let data = [];
        data = this.state.table;
        data[leagueIndex] = res.data.data.getStandings;
        this.setState({ table: data });
        this.forceUpdate();
      });
    } catch (error) {
      return null;
    }
  }
  getTopScorers(leagueIndex) {
    const query = `
                    query topscorer {
                      getTopscorers(seasonId: ${leagues[leagueIndex][2]}) {
                        goals
                        playerInclude {
                          player {
                            positionId
                            commonName
                            imagePath
                          }
                        }
                      }
                    }
                `;
    const variables = {
      cancelToken: new CacelToken(function executor(c) {
        cancel = c;
      })
    };
    try {
      return axios.post('https://victory.softwarehouse.io/graphql', {
        query
      }, variables).then(res => {
        if (res.data.data.getTopscorers.length == 0)
          return null;
        let data = [];
        data = this.state.topScorers;
        data[leagueIndex] = res.data.data.getTopscorers;
        this.setState({ topScorers: data });
        this.forceUpdate();
      });
    } catch (error) {
      return null;
    }
  }

  itemExpand = (leagueindex) => {
    this.state.states[leagueindex] = !this.state.states[leagueindex];
    for (var i = 0; i < leagues.length; i++) {
      if (leagueindex == i)
        continue;
      this.state.states[i] = false;
    }
    this.forceUpdate();
  };

  setSearch = (text) => {

  }
  getStandingContent() {
    return leagues.map((league, index) => {
      return (
        <View
          key={index}
          style={[Styles.unit, this.state.states[index] ? { borderBottomColor: '#aaaaaa', borderBottomWidth: 1 } : {}]}>
          <TouchableOpacity
            style={Styles.league}
            onPress={(leagueindex) => this.itemExpand(index)}>
            <Image style={Styles.markImg} source={leagueLogo} />
            <View
              style={[
                Styles.subleague,
                this.state.states[index] ? { borderBottomWidth: 0 } : {},
              ]}>
              <Text style={[Styles.contentItem, this.state.states[index] ? { color: '#57BD7E' } : {}]}>{league[0]}</Text>
              <Image
                style={[Styles.iconImg,
                this.state.states[index] ? { transform: [{ rotate: '90deg' }], tintColor: '#57BD7E' } : {}]}
                source={arrow_go_icon} />
            </View>
          </TouchableOpacity>
          {this.getContent(index)}
        </View>
      );
    });
  }
  getContent(index) {
    if (!this.state.states[index])
      return null;
    if (this.state.isTable)
      return (
        <View style={Styles.table}>
          <View style={Styles.tableHdr}>
            <Text style={Styles.tableName}>{leagues[index][0]} Table</Text>
            <Text style={Styles.tableCell}>P</Text>
            <Text style={Styles.tableCell}>W</Text>
            <Text style={Styles.tableCell}>L</Text>
            <Text style={Styles.tableCell}>Pts</Text>
          </View>
          {this.getTableContent(index)}
        </View>
      );
    else
      return (
        <View style={Styles.table}>
          <View style={Styles.tableHdr}>
            <Text style={Styles.topScoresName}>{leagues[index][0]} Top Scorers</Text>
            <Text style={Styles.topScoresCell}>Goals</Text>
            <Text style={Styles.topScoresCell}>Position</Text>
          </View>
          {this.getTopScorerContent(index)}
        </View>
      );
  }
  getTopScorerContent(leagueIndex) {

    var count = this.state.topScorers[leagueIndex].length;
    return this.state.topScorers[leagueIndex].map((item, index) => {
      return (
        <View
          key={index}
          style={[Styles.tableRow, count == index + 1 ? { borderBottomWidth: 0 } : {}]}>
          <View style={Styles.topScoresplayer}>
            <Image
              style={Styles.topScoresplayerMarkImg}
              source={item.playerInclude.player.imagePath == '' ? null_Img : { uri: item.playerInclude.player.imagePath }} />
            <Text style={Styles.topScoresplayerName}>{item.playerInclude.player.commonName}</Text>
          </View>
          <Text style={Styles.topScoresCell}>{item.goals}</Text>
          <Text style={Styles.topScoresCell}>{this.getPosition(item.playerInclude.player.positionId)}</Text>
        </View>
      )
    });
  }
  getPosition(positionId) {
    switch (positionId) {
      case 1:
        return 'GoalKeeper';
      case 2:
        return 'Defender';
      case 3:
        return 'Midfielder';
      case 4:
        return 'Forward';
      default:
        return '';
    }
  }
  getTableContent(leagueIndex) {
    var count = this.state.table[leagueIndex].length;
    return this.state.table[leagueIndex].map((item, index) => {
      return (
        <View
          key={index}
          style={[Styles.tableRow, count == index + 1 ? { borderBottomWidth: 0 } : {}]}>
          <Text style={Styles.num}>{index + 1}</Text>
          <View style={Styles.player}>
            <View style={this.getStatus(item.status)} />
            <Image
              style={Styles.playerMarkImg}
              source={item.teamInclude.team.logoPath == '' ? null_logo : { uri: item.teamInclude.team.logoPath }} />
            <Text style={Styles.playerName}>{item.teamName}</Text>
          </View>
          <Text style={Styles.tableCell}>{item.overall.gamesPlayed}</Text>
          <Text style={Styles.tableCell}>{item.overall.won}</Text>
          <Text style={Styles.tableCell}>{item.overall.lost}</Text>
          <Text style={Styles.tableCell}>{item.total.points}</Text>
        </View>
      )
    });
  }
  getStatus(status) {
    switch (status) {
      case 'same':
        return Styles.circle;
      case 'up':
        return Styles.triangle_up;
      case 'down':
        return Styles.triangle_down;
      default:
        return Styles.rectangle;
    }
  }
  onPressTable() {
    this.setState({ isTable: true });
  }
  onPressTopScores() {
    this.setState({ isTable: false });
  }
  render() {
    return (
      <Content contentContainerStyle={Styles.contentStyle} >
        <View style={Styles.tab_bar}>
          <TouchableOpacity
            style={[
              Styles.left_tab,
              this.state.isTable ? { backgroundColor: '#57BD7E' } : {}]}
            onPress={() => this.onPressTable()} >
            <Text
              style={[
                Styles.btnText,
                this.state.isTable ? { color: 'white' } : {}]}>
              {strings.table}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              Styles.right_tab,
              this.state.isTable ? {} : { backgroundColor: '#57BD7E' }]}
            onPress={() => this.onPressTopScores()}>
            <Text
              style={[
                Styles.btnText,
                this.state.isTable ? {} : { color: 'white' }]}>
              {strings.top_scores}
            </Text>
          </TouchableOpacity>{/**/}
        </View>
        <SearchTextInput
          onChangeText={text => this.setSearch(text)}
          placeholder={strings.search_league}
          color={'black'}
          secureTextEntry={false}
        />
        <View style={Styles.subdesc}>
          <Text style={Styles.sep}>{strings.competition}</Text>
        </View>
        {this.getStandingContent()}
      </Content >
    );
  }
}

const Styles = {
  contentStyle: {
    alignItems: 'center',
    alignSelf: 'center',
  },
  tab_bar: {
    width: width,
    padding: height / 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: 'white',
    marginTop: height / 50,
    width: width * 2 / 3.5,
    height: height / 25,
    borderRadius: height / 50,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: '#57BD7E',
  },
  left_tab: {
    width: width / 3.5,
    height: height / 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  right_tab: {
    width: width / 3.5,
    height: height / 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontFamily: 'Roboto-Regular',
    fontSize: 13,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    color: colors.itemContent
  },
  subdesc: {
    width: width,
    paddingLeft: width / 20,
    backgroundColor: colors.itemNameBack,
    padding: width / 30,
  },
  sep: {
    fontFamily: 'Roboto-Medium',
    fontSize: dimens.itemTitle,
    color: colors.itemContent
  },
  unit: {
    backgroundColor: 'white',
    width: width,
  },
  league: {
    flexDirection: 'row',
    width: width,
    alignItems: 'center',
    // padding: 5,
    paddingLeft: width / 20,
    paddingRight: width / 20,
  },
  subleague: {
    marginLeft: width / 20,
    width: width * 3 / 4,
    height: width / 10,
    paddingRight: width / 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#aaaaaa',
    padding: 5,
  },
  markImg: {
    width: width / 30 * 2,
    height: width / 30 * 2,
    resizeMode: 'stretch',
  },
  tableHdr: {
    width: width,
    flexDirection: 'row',
    padding: 3,
    backgroundColor: colors.itemNameBack,
    paddingLeft: width / 20,
    alignItems: 'center'
  },
  tableName: {
    fontFamily: 'Roboto-Regular',
    width: width / 2,
    fontSize: dimens.itemTitle,
    color: colors.itemContent
  },
  tableCell: {
    fontFamily: 'Roboto-Regular',
    width: width / 10,
    fontSize: dimens.small,
    color: colors.itemContent,
    textAlign: 'center'
  },
  topScoresName: {
    fontFamily: 'Roboto-Regular',
    width: width / 5 * 3 - width / 10,
    fontSize: dimens.itemTitle,
    color: colors.itemContent
  },
  topScoresCell: {
    width: width / 20 * 4,
    fontSize: dimens.small,
    textAlign: 'center',
    color: colors.itemContent
  },
  tableRow: {
    flexDirection: 'row',
    marginLeft: width / 20,
    width: width / 20 * 19,
    borderBottomColor: '#aaaaaa',
    borderBottomWidth: 1,
    alignItems: 'center',
    padding: 5,
  },
  num: {
    width: width / 20,
    fontSize: dimens.small,
  },
  player: {
    flexDirection: 'row',
    width: width / 10 * 4 + width / 20 - width / 80,
    alignItems: 'center',
  },
  topScoresplayer: {
    flexDirection: 'row',
    width: width / 30 * 18 - width / 50 - width / 10,
    alignItems: 'center',
  },
  playerMarkImg: {
    marginLeft: width / 60,
    height: width / 20,
    width: width / 20,
    resizeMode: 'contain',
    marginRight: width / 60,
  },
  topScoresplayerMarkImg: {
    height: width / 20,
    width: width / 20,
    resizeMode: 'contain',
    marginRight: width / 30,
  },
  circle: {
    height: width / 50,
    width: width / 50,
    borderRadius: width / 100,
    backgroundColor: '#8bc7d3',
  },
  triangle_up: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: width / 100,
    borderRightWidth: width / 100,
    borderBottomWidth: width / 50,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#3cce81',
  },
  triangle_down: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: width / 100,
    borderRightWidth: width / 100,
    borderBottomWidth: width / 50,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#fb2e1b',
    transform: [{ rotate: '180deg' }],
  },
  rectangle: {
    width: width / 50,
    height: width / 50,
    backgroundColor: 'red',
  },
  playerName: {
    fontFamily: 'Roboto-Medium',
    fontSize: dimens.small,
    color: colors.itemContent
  },
  topScoresplayerName: {
    fontFamily: 'Roboto-Medium',
    fontSize: 13,
    color: colors.itemContent
  },
  contentItem: {
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent,
    fontSize: dimens.itemTitle - 1
  }
};

Stat_Standings.propTypes = {
  // You can declare that a prop is a specific JS primitive. By default, these
  // are all optional.
  defaultValue: PropTypes.string,
};
