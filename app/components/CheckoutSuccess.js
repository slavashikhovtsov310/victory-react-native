import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, Picker } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Simple_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import sample from '../assets/cart_sample.png';
import stat_icon from '../assets/icons/stat.png';
import shop_icon from '../assets/icons/shop.png';
import swipe_icon from '../assets/icons/swipe.png';
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class CheckoutSuccess extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
    }

    componentDidUpdate() {
        this.proceed()
    }

    proceed() {
        const loginError = this.props.Login.get('loginError');
        const isLoggedIn = this.props.Login.get('isLoggedIn');
        const toastStyles = {
            width: width,
            height: height / 6,
            backgroundColor: '#34343400',
            color: 'red',
            fontSize: dimens.text_size_button,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center'
        }
        // Alert.alert(this.state.email + ', ' + loginError.msg)
        // if (isLoggedIn)
        //     Alert.alert('1')
        // if (this.state.isGoneAlready)
        //     Alert.alert('2')
        // if (this.isObject(loginError.msg))
        //     Alert.alert('3')
        // if (loginError.msg)
        //     Alert.alert('4')
        if (this.isObject(loginError) && loginError && !this.isObject(loginError.msg) && loginError.msg) {
            Toast.show(loginError.msg, Toast.SHORT, Toast.CENTER, toastStyles);
            // Alert.alert(loginError + '');
            this.props.dispatch(loginActions.setError({}))
        } else if (isLoggedIn && !this.state.isGoneAlready) {
            this.setState({ isGoneAlready: true });
            this.props.navigation.navigate(consts.DASHBOARD_SCREEN);
        }
    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    // noinspection JSMethodCanBeStatic
    setEmail(text) {
        this.setState({ email: text });
    }
    setPassword(text) {
        this.setState({ password: text });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    setSearch = (text) => {

    }
    render() {
        return (

            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header title={strings.checkout} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.pannel}>
                        <Text style={Styles.thankyou}>{strings.thank_you_for_ordering_with_us}</Text>
                        <Text style={Styles.succssresult}>{strings.your_order_has_been_placed_successfully}</Text>
                    </View>
                    <TouchableOpacity style={Styles.btn}>
                        <Text style={Styles.btnText}>{strings.returnhomepage}</Text>
                    </TouchableOpacity>
                    {this.renderProgress()}
                </Content>
                <Footer navigation={this.props.navigation}/>
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        this.setState({ isGoneAlready: false });
        this.props.dispatch(loginActions.login(this.state.email, this.state.password));
    }
    onSignupNowPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
}


const Styles = {
    containerStyle: {
        // alignItems: 'center',
        backgroundColor: '#f0f0f0'
    },
    contentStyle: {
        // position: 'absolute',
        // top: height / 40 * (3.8 + 2),
        // height: height * 9 / 10 - StatusBar.currentHeight,
        // flex: 1,
        // flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        // height: height,
        resizeMode: 'contain',
    },
    btn: {
        marginTop: height / 20,
        width: width / 20 * 17,
        height: height / 40 * 3,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    btnText: {
        fontSize: 16
    },
    pannel: {
        marginTop: height / 10,
        height: height / 2,
        width: width / 20 * 17,
        backgroundColor: 'white',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    thankyou: {
        color: 'black',
        fontSize: dimens.thankyou,
        width: width / 3 * 2,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    }
};

const mapStateToProps = (state) => ({
    CheckoutSuccess: state.get('checkoutSuccess'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(CheckoutSuccess)