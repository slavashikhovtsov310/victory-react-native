import React, { Component } from "react";
import { Image, StatusBar, Text, Dimensions, TouchableOpacity, Platform } from "react-native";
import { Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Simple_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import * as cartActions from "../actions/cart-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import trash from '../assets/icons/trash.png';
import Orientation from 'react-native-orientation';
import { Dropdown } from 'react-native-material-dropdown';
import axios from 'axios';

console.ignoredYellowBox = ['Warning:'];
console.disableYellowBox = true
const CacelToken = axios.CancelToken;
let cancel;
const { width, height } = Dimensions.get('window');
const domain = "https://victory.softwarehouse.io/";
let list = [];
export class Cart extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            list: [],
            products: [],
            expands: []
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false));
        list = this.props.Cart.get('list');
        this.initialize();
    }

    initialize() {
        let products = [];
        let expands = [];
        let i = 0;
        for (i = 0; i < list.length; i++) {
            products.push([]);
            expands.push(false);
        }
        this.setState({ list: list, products: products, expands: expands });
        for (i = 0; i < list.length; i++) {
            this.getProductDetail(i);
        }
    }

    getProductDetail(index) {
        const query = `
                    query productDetail {
                        getProductDetails(productId: ${list[index]}) {
                            name
                            description
                            price
                            gender
                            image
                            categoryId
                            kitCode
                            thumbnail
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                let data = this.state.products;
                data[index] = res.data.data.getProductDetails;
                this.setState({ products: data });
                this.forceUpdate();
            });
        } catch (error) {
            return null;
        }
    }

    setSearch = (text) => {

    }



    getPickerGenders(genders) {
        genders = ['Male', 'Female'];
        let genderList = [];
        for (let i = 0; i < genders.length; i++) {
            let gender = genders[i];
            let flag = true;
            for (let j = 0; j < genderList.length; j++) {
                if (genderList[j] == gender) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                genderList.push({ value: gender });
        }
        if (genderList.length == 0)
            return null;

        return (
            <Dropdown
                label='gender'
                data={genderList}
                containerStyle={[Styles.smallpickerStyle, { borderRightWidth: 1 }]}
                lineWidth={0}
                inputContainerPadding={0}
                labelHeight={0}
                inputContainerStyle={Styles.inputContainerStyle}
                textColor={colors.itemContent}
                fontSize={dimens.item}
                fontFamily='Roboto-Medium' />
        );
    }

    getPickerQtys(qtys) {
        qtys = ['1', '2'];
        let qtyList = [];
        for (let i = 0; i < qtys.length; i++) {
            let qty = qtys[i];
            let flag = true;
            for (let j = 0; j < qtyList.length; j++) {
                if (qtyList[j] == qty) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                qtyList.push({ value: qty });
        }
        if (qtyList.length == 0)
            return null;

        return (
            <Dropdown
                label='QTY'
                data={qtyList}
                containerStyle={Styles.smallpickerStyle}
                lineWidth={0}
                inputContainerPadding={0}
                labelHeight={0}
                inputContainerStyle={Styles.inputContainerStyle}
                textColor={colors.itemContent}
                fontSize={dimens.item}
                fontFamily='Roboto-Medium' />
        );
    }

    getItems() {
        return this.state.products.map((product, index) => {
            if (product.length == 0)
                return null;
            return (
                <View key={index} style={Styles.cartItem}>
                    <View style={Styles.cartImgView}>
                        <Image style={Styles.cartImg} source={{ uri: domain + product[0].thumbnail }} />
                    </View>
                    <TouchableOpacity activeOpacity={0.9} style={Styles.descPan} onPress={() => this.toggleDelete(index)}>
                        <Text style={Styles.itemTitle}>{product[0].name}</Text>
                        <Text style={Styles.itemDesc}>{product[0].description}</Text>
                        <View style={Styles.pickerRow}>
                            {this.getPickerQtys()}
                            {this.getPickerGenders()}
                        </View>
                    </TouchableOpacity>
                    {this.state.expands[index] ?
                        <TouchableOpacity activeOpacity={0.5} style={Styles.deleteBtn}
                            onPress={() => this.deleteItem(index)}>
                            <Image style={Styles.deleteImg} source={trash} />
                            <Text style={Styles.deleteText}>Delete</Text>
                        </TouchableOpacity>
                        : null}
                </View>
            );
        });
    }

    deleteItem = (index) => {
        list.splice(index, 1);
        this.initialize();
        this.saveData(list)
    }

    saveData(list) {
        this.props.dispatch(cartActions.setCartList(list));
    }

    toggleDelete = (index) => {
        let data = this.state.expands
        data[index] = !data[index];
        for (var i = 0; i < data.length; i++) {
            if (i == index)
                continue;
            data[i] = false;
        }
        this.setState({ expands: data });
    }

    getTotalPrice() {
        let sum = 0;
        for (let i = 0; i < this.state.products.length; i++) {
            if (this.state.products[i].length == 0)
                continue;
            sum = sum + this.state.products[i][0].price;
        }
        return sum;
    }

    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header title={strings.cart} goback={this.goback} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <Text style={Styles.total}>{this.state.list.length} items: total of {this.getTotalPrice()} aed</Text>
                    {this.getItems()}
                    <View style={Styles.btnRow}>
                        <TouchableOpacity style={Styles.btn} onPress={this.onCheckoutPress}>
                            <Text style={Styles.btnText}>{strings.checkout}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={Styles.btn}>
                            <Text style={Styles.btnText}>{strings.continueshopping}</Text>
                        </TouchableOpacity>
                    </View>
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    onCheckoutPress = () => {
        this.props.navigation.navigate(consts.CHECKOUT_SCREEN);
    }
    goback = () => {
        this.props.navigation.pop();
    }
}

const Styles = {
    containerStyle: {
        backgroundColor: 'white'
    },
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
        paddingBottom: height / 7
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        // height: height,
        resizeMode: 'contain',
    },
    item: {
        width: width / 20 * 17,
        height: height * 1.7 / 3,
        resizeMode: 'cover',
        borderRadius: 15,
        marginTop: height / 40,
    },
    cartItem: {
        flexDirection: 'row',
        width: width,
        paddingLeft: width / 15,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: '#aaaaaa',
        marginTop: height / 40,
        justifyContent: 'flex-end'
    },
    pickerRow: {
        flexDirection: 'row',
        width: width / 2
    },
    cartImgView: {
        width: width / 7 * 2,
        height: width / 7 * 2 * 123 / 100,
        backgroundColor: colors.itemNameBack,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
        alignItems: 'center'
    },
    cartImg: {
        width: width / 7 * 2,
        height: width / 7 * 2 * 123 / 100,
        resizeMode: 'contain',
        borderRadius: 5,
    },
    pickerStyle: {
        color: 'black',
        width: width / 4
    },
    itemTitle: {
        width: width / 20 * 11,
        fontSize: dimens.h2,
        color: colors.itemContent,
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
    },
    itemDesc: {
        fontSize: dimens.item,
        color: colors.subItemContent2,
        width: width / 5 * 3,
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Rg' : 'Aller-Regular',
        marginTop: width / 80
    },
    descPan: {
        marginLeft: width / 30,
        width: width * (1 - 1 / 15 - 2 / 7 - 1 / 30),
        paddingTop: 10,
        paddingBottom: 10,
    },
    btn: {
        marginTop: height / 15,
        width: width / 20 * 17,
        height: height / 20,
        backgroundColor: colors.itemNameBack,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    btnText: {
        fontSize: dimens.itemTitle,
        fontFamily: 'OpenSans-Semibold',
        color: colors.itemContent
    },
    total: {
        marginTop: height / 40,
        backgroundColor: 'white',
        color: colors.itemContent,
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.small
    },
    smallpickerStyle: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderColor: '#cccccc',
        width: width / 4,
        paddingLeft: 5,
        justifyContent: 'center',
        paddingTop: width / 50,
        paddingBottom: width / 80,
        marginTop: width / 30,
    },
    deleteBtn: {
        backgroundColor: colors.dangerColor,
        justifyContent: 'center',
        width: width / 4.5
    },
    deleteImg: {
        width: width / 5,
        height: width / 5 * 92 / 75,
        marginLeft: -width / 13,
        resizeMode: 'contain'
    },
    deleteText: {
        marginTop: width / 30,
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.small,
        color: 'white',
        alignItems: 'center',
        textAlign: 'center'
    },
    btnRow: {
        flex: 1,
        flexDirection: 'row',
        width: width,
        justifyContent: 'space-between',
        paddingLeft: width / 15,
        paddingRight: width / 15,
        marginTop: height / 10,
        alignItems: 'center',
    },
    btn: {
        width: width / 5 * 2,
        height: height / 20,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    btnText: {
        fontSize: 16
    },
};

const mapStateToProps = (state) => ({
    Cart: state.get('cart'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Cart)