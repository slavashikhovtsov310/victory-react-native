import React, { Component } from "react";
import { Content, Input, Item, Label, Text, Left, View } from "native-base";
import { Alert, Image, Dimensions, TextInput } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import back_icon from '../assets/icons/back.png'
import cross_icon from '../assets/icons/cross.png'
import colors from "../resources/colors";
import dimens from "../resources/dimens";

const { width, height } = Dimensions.get('window');


export default class Back_Header extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            })
        };
        // Alert.alert(width + ', ' + height);
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <View
                    shouldRasterizeIOS
                    renderToHardwareTextureAndroid
                    style={styles.row} scrollEnabled={false}>
                    <Image source={back_icon}
                        style={styles.optionStyle} />
                    <Text style={styles.labelStyle}>{this.props.title}</Text>
                    <Image source={cross_icon}
                        style={styles.exitStyle} />
                </View>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        // position: 'absolute',
        // top: 0,
        // flex: 1,
        // flexDirection: 'row',
        // width: width,
        height: height / 40 * 3.8,
        // backgroundColor: colors.accentColor,
        // alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        shadowColor: '#333333',
        // shadowOffset: 13,
        // shadowRadius: 13,
        borderBottomWidth: 1,
        borderColor: '#aaaaaa',
        // justifyContent: 'center',
        zIndex: 1000
    },
    row: {
        flexDirection: 'row',
        width: width,
        height: height / 40 * 3.8,
        backgroundColor: colors.accentColor,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionStyle: {
        position: 'absolute',
        left: 20,
        width: width / 15,
        resizeMode: 'contain'
    },
    labelStyle: {
        fontSize: dimens.header,
        color: 'black',
        fontWeight: 'bold',
    },
    sublabel: {
        color: colors.header_sub,
        position: 'absolute',
        bottom: 0,
        paddingLeft: width / 10,
        fontStyle: 'italic'
    },
    exitStyle: {
        position: 'absolute',
        right: 20,
        width: width / 15,
        resizeMode: 'contain'
    },
};

Back_Header.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};