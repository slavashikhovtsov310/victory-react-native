import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableHighlight, AsyncStorage } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Shop_Header";
import SubShop from "./SubShop";
import Footer from "./Stat_Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import sample from '../assets/sample.png';
import Orientation from 'react-native-orientation';
import arrow_go_icon from '../assets/icons/arrow_go.png'
import Toast from 'react-native-toast-native';
import { DrawerNavigator } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import rm_icon from '../assets/icons/rm.png';
import fc_icon from '../assets/icons/fc.png';
import star_icon from '../assets/icons/star.png';
import filled_star_icon from '../assets/icons/filled_star.png';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
const leagues =
    [
        ["World Cup", "732"],
        ["UAE League", "959"],
        ["Division 1", "962"],
        ["Arabian Gulf Cup", "965"],
        ["Pro League", "944"],
        ["Division 1", "947"],
        ["Kings Cup", "950"],
        ["La Liga", "564"],
        ["Serie A", "384"],
        ["Ligue 1", "301"],
        ["Premier League", "8"]
    ];
console.ignoredYellowBox = ['Warning:'];
export class SelectLeague extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false,
            itemSel: [
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false },
                { selected: false }
            ]
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        let myFavLeague = [];
        myFavLeague = this.props.root.get('favLeagues');
        for (var i = 0; i < myFavLeague.length; i++) {
            for (var j = 0; j < leagues.length; j++) {
                if (myFavLeague[i][1] == leagues[j][1]) {
                    this.state.itemSel[j].selected = true;
                }
            }
        }
        this.forceUpdate();
    }

    componentDidUpdate() {
        this.proceed()
    }

    proceed() {
    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    setEmail(text) {
        this.setState({ email: text });
    }
    setPassword(text) {
        this.setState({ password: text });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    setSearch = (text) => {

    }
    itemSel = (itemIndex) => {
        this.state.itemSel[itemIndex].selected = !this.state.itemSel[itemIndex].selected;
        let data = [];
        for (var i = 0; i < this.state.itemSel.length; i++) {
            if (this.state.itemSel[i].selected)
                data.push(leagues[i])
        }
        this.saveKey(data);
        this.props.dispatch(rootActions.setMyFavoriteLeagues(data));
        this.forceUpdate()
    }
    getLeagues() {
        return leagues.map((league, index) => {
            return (
                <View style={Styles.itemRow}>
                    <Image style={Styles.leagueImg} source={rm_icon} />
                    <View style={Styles.itemGroup}>
                        <View style={Styles.itemDesc}>
                            <Text style={Styles.leagueName}>{league[0]}</Text>
                            <Text style={Styles.teams}>6 {strings.teams}</Text>
                        </View>
                        <TouchableHighlight underlayColor='transparent' style={Styles.optBtn} onPress={(itemIndex) => this.itemSel(index)}>
                            <Image
                                style={[Styles.optImg, this.state.itemSel[index].selected ? {} : { tintColor: colors.itemContent }]}
                                source={this.state.itemSel[index].selected ? filled_star_icon : star_icon} />
                        </TouchableHighlight>
                    </View>
                </View>
            );
        });
    }
    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <LinearGradient
                    start={{ x: 0, y: 1 }}
                    end={{ x: 1, y: 1 }}
                    colors={['#81d7af', '#8091da']}
                    style={Styles.hdr}>
                    <TouchableHighlight underlayColor='transparent' style={Styles.backBtn} onPress={this.goBack}>
                        <Image style={Styles.backBtnImg} source={arrow_go_icon} />
                    </TouchableHighlight>
                    <View style={{ width: width / 8 * 7 }}>
                        <SearchTextInput
                            onChangeText={(text) => this.setSearch(text)}
                            placeholder={strings.searchyourleague}
                            color={'black'}
                            secureTextEntry={false} />
                    </View>
                </LinearGradient>
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.classTitle}>
                        <Text style={Styles.classTitleText}>{strings.popularleague}</Text>
                    </View>
                    {this.getLeagues()}
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container >
        );
    }
    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        this.setState({ isGoneAlready: false });
        this.props.dispatch(loginActions.login(this.state.email, this.state.password));
    }
    onSignupNowPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
    onSubShop = (index) => {
        this.props.navigation.navigate(consts.SUBSHOP_SCREEN);
    }
    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
    }
    onPressWishList = () => {
        this.props.navigation.navigate(consts.WISHLIST_SCREEN);
    }
    onPressOpt = () => {
        this.props.navigation.navigate('DrawerOpen')
    }
    goBack = () => {
        this.props.dispatch(rootActions.MyFavoriteLeaguesChanged(true));
        this.props.navigation.pop();
    }

    async saveKey(value) {
        try {
            await AsyncStorage.setItem('@Victory:favLeagues', JSON.stringify(value));
        } catch (error) {
            console.warn("Error saving data" + error);
        }
    }

    async resetKey() {
        try {
            await AsyncStorage.removeItem('@Victory:favLeagues');
            const value = await AsyncStorage.getItem('@Victory:favLeagues');
        } catch (error) {
            console.log("Error resetting data" + error);
        }
    }
}


const Styles = {
    containerStyle: {
        paddingBottom: height / 9,
    },
    contentStyle: {
        width: width,
        marginTop: width / 20,
        alignItems: 'center',
        paddingBottom: height / 9,
        alignSelf: 'center',
        backgroundColor: 'white'
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        resizeMode: 'cover',
    },
    hdr: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width,
        paddingLeft: width / 10,
        padding: width / 30,
    },
    backBtnImg: {
        transform: [
            { rotate: '180deg' }
        ],
        tintColor: 'white',
        paddingTop: height / 50,
        paddingBottom: height / 50,
        resizeMode: 'contain'
    },
    itemRow: {
        flexDirection: 'row',
        paddingLeft: width / 20,
        width: width,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    more: {
        width: width,
        paddingLeft: width / 5 * 0.8,
        paddingTop: height / 60,
        paddingBottom: height / 60
    },
    leagueImg: {
        width: width / 15,
        height: width / 15,
        resizeMode: 'stretch'
    },
    itemGroup: {
        paddingRight: width / 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#aaaaaa',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: width / 5 * 4.2,
        paddingTop: height / 70,
        paddingBottom: height / 70
    },
    classTitle: {
        flexDirection: 'row',
        paddingLeft: width / 20,
        paddingRight: width / 20,
        backgroundColor: colors.itemNameBack,
        paddingBottom: height / 70,
        paddingTop: height / 70,
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    teams: {
        fontSize: 10
    },
    optImg: {
        height: width / 15,
        width: width / 15,
        resizeMode: 'contain',
    }
};

const mapStateToProps = (state) => ({
    SelectLeague: state.get('selectLeague'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(SelectLeague)