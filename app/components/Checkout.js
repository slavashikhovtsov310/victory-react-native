import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, Picker } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Simple_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import sample from '../assets/cart_sample.png';
import arrow_go_icon from '../assets/icons/arrow_go.png';
import paypal_icon from '../assets/icons/paypal.png';
import cash_icon from '../assets/icons/cash.png';
import creditcard_icon from '../assets/icons/creditcard.png';
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class Checkout extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
    }

    componentDidUpdate() {
        this.proceed()
    }

    proceed() {
        const loginError = this.props.Login.get('loginError');
        const isLoggedIn = this.props.Login.get('isLoggedIn');
        const toastStyles = {
            width: width,
            height: height / 6,
            backgroundColor: '#34343400',
            color: 'red',
            fontSize: dimens.text_size_button,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center'
        }
        // Alert.alert(this.state.email + ', ' + loginError.msg)
        // if (isLoggedIn)
        //     Alert.alert('1')
        // if (this.state.isGoneAlready)
        //     Alert.alert('2')
        // if (this.isObject(loginError.msg))
        //     Alert.alert('3')
        // if (loginError.msg)
        //     Alert.alert('4')
        if (this.isObject(loginError) && loginError && !this.isObject(loginError.msg) && loginError.msg) {
            Toast.show(loginError.msg, Toast.SHORT, Toast.CENTER, toastStyles);
            // Alert.alert(loginError + '');
            this.props.dispatch(loginActions.setError({}))
        } else if (isLoggedIn && !this.state.isGoneAlready) {
            this.setState({ isGoneAlready: true });
            this.props.navigation.navigate(consts.DASHBOARD_SCREEN);
        }
    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    // noinspection JSMethodCanBeStatic
    setEmail(text) {
        this.setState({ email: text });
    }
    setPassword(text) {
        this.setState({ password: text });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    setSearch = (text) => {

    }
    render() {
        return (

            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header title={strings.checkout} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <Text style={Styles.total}>2 items: total of 100 aed</Text>
                    <View style={Styles.pannel}>
                        <Text style={Styles.subtitle}>{strings.mybag}</Text>
                        <View style={Styles.itemrow}>
                            <Image style={Styles.itemImg} source={sample} />
                            <Image style={Styles.itemImg} source={sample} />
                            <Image style={Styles.itemImg} source={sample} />
                            <Image style={Styles.itemImg} source={sample} />
                        </View>
                        <View style={Styles.itemrow}>
                            <Image style={Styles.itemImg} source={sample} />
                            <Image style={Styles.itemImg} source={sample} />
                        </View>
                    </View>
                    <View style={Styles.pannel}>
                        <View style={Styles.deliveryRow}>
                            <Text style={Styles.subtitle}>{strings.delivery}</Text>
                            <TouchableOpacity style={Styles.countrySel}>
                                <Text>
                                    {strings.united_arab_emirates}
                                </Text>
                                <Image source={arrow_go_icon} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={Styles.pannel}>
                        <View style={Styles.deliveryRow}>
                            <Text style={Styles.subtitle}>{strings.totalprice}</Text>
                            <Text>
                                100 AED
                            </Text>
                        </View>
                    </View>
                    <View style={Styles.pannel}>
                        <Text style={[Styles.subtitle, { width: width / 10 * 9 }]}>{strings.promocode}</Text>
                        <Text>
                            {strings.codegoeshere}
                        </Text>
                    </View>
                    <View style={Styles.pannel}>
                        <Text style={[Styles.subtitle, { width: width / 10 * 9 }]}>{strings.paymentmethod}</Text>
                        <View style={Styles.paymentRow}>
                            <TouchableOpacity style={Styles.paymentCell}>
                                <Image style={Styles.paymentIcon} source={cash_icon} />
                                <Text style={Styles.paymentText}>{strings.cash}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={Styles.paymentCell} onPress={this.onCreditCardPress}>
                                <Image style={Styles.paymentIcon} source={creditcard_icon} />
                                <Text style={Styles.paymentText}>{strings.creditcard}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={Styles.paymentCell}>
                                <Image style={Styles.paymentIcon} source={paypal_icon} />
                                <Text style={Styles.paymentText}>{strings.paypal}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {this.renderProgress()}
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        this.setState({ isGoneAlready: false });
        this.props.dispatch(loginActions.login(this.state.email, this.state.password));
    }
    onSignupNowPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
    onCreditCardPress = () => {
        this.props.navigation.navigate(consts.CREDITCARD_SCREEN);
    }
}


const Styles = {
    containerStyle: {
        // alignItems: 'center',
        backgroundColor: '#f0f0f0'
    },
    contentStyle: {
        // position: 'absolute',
        // top: height / 40 * (3.8 + 2),
        // height: height * 9 / 10 - StatusBar.currentHeight,
        // flex: 1,
        // flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        paddingBottom: height / 8,
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        // height: height,
        resizeMode: 'contain',
    },
    itemrow: {
        width: width * 9 / 10,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf: 'center',
        paddingTop: width / 40,
    },
    pannel: {
        width: width,
        padding: width / 20,
        paddingLeft: width / 20,
        paddingRight: width / 20,
        marginTop: width / 20,
        backgroundColor: 'white'
    },
    itemImg: {
        width: width / 5,
        // height: width / 9,
        height: height / 6,
        resizeMode: 'stretch',
        marginRight: width / 30,
    },
    deliveryRow: {
        flexDirection: 'row',
        alignItems: 'center',
        // alignSelf: 'center'
        // justifyContent: 'center'
    },
    countrySel: {
        flexDirection: 'row',
        width: width / 20 * 13,
        justifyContent: 'space-between'
    },
    subtitle: {
        fontSize: dimens.h2,
        color: 'black',
        width: width / 4
    },
    paymentRow: {
        flexDirection: 'row',
        width: width / 10 * 9,
        paddingTop: height / 40,
        justifyContent: 'space-between'
    },
    paymentCell: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    paymentIcon: {
        height: height / 15,
        resizeMode: 'contain'
    },
    total: {
        marginTop: height / 40
    }
};

const mapStateToProps = (state) => ({
    Checkout: state.get('checkout'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Checkout)