import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableHighlight } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Profile_Header";
import SubShop from "./SubShop";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import LeftSideMenu from "./Stat_LeftSideMenu";
import background from '../assets/shop_background.png';
import sample from '../assets/sample.png';
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
import { DrawerNavigator } from 'react-navigation';
import avatar from '../assets/avatar.png'
import LinearGradient from 'react-native-linear-gradient';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class Profile extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
    }

    componentDidUpdate() {
        this.proceed()
    }

    proceed() {
        // const loginError = this.props.Login.get('loginError');
        // const isLoggedIn = this.props.Login.get('isLoggedIn');
        // const toastStyles = {
        //     width: width,
        //     height: height / 6,
        //     backgroundColor: '#34343400',
        //     color: 'red',
        //     fontSize: dimens.text_size_button,
        //     alignItems: 'center',
        //     alignSelf: 'center',
        //     justifyContent: 'center'
        // }
        // // Alert.alert(this.state.email + ', ' + loginError.msg)
        // // if (isLoggedIn)
        // //     Alert.alert('1')
        // // if (this.state.isGoneAlready)
        // //     Alert.alert('2')
        // // if (this.isObject(loginError.msg))
        // //     Alert.alert('3')
        // // if (loginError.msg)
        // //     Alert.alert('4')
        // if (this.isObject(loginError) && loginError && !this.isObject(loginError.msg) && loginError.msg) {
        //     Toast.show(loginError.msg, Toast.SHORT, Toast.CENTER, toastStyles);
        //     // Alert.alert(loginError + '');
        //     this.props.dispatch(loginActions.setError({}))
        // } else if (isLoggedIn && !this.state.isGoneAlready) {
        //     this.setState({ isGoneAlready: true });
        //     this.props.navigation.navigate(consts.DASHBOARD_SCREEN);
        // }
    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    // noinspection JSMethodCanBeStatic
    setEmail(text) {
        this.setState({ email: text });
    }
    setPassword(text) {
        this.setState({ password: text });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    setSearch = (text) => {

    }
    render() {
        return (

            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header
                    isChanged={this.state.dummy}
                    onPressEdit={this.onPressEdit}
                    onPressOpt={this.onPressOpt} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.avatar}>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#81d7af', '#8091da']}
                            style={Styles.outter}>
                            <View style={Styles.inner}>
                                <Image style={Styles.profileImg} source={avatar} />
                            </View>
                        </LinearGradient>
                        <Text style={Styles.name}>Mo Abdllah</Text>
                    </View>
                    <TouchableHighlight underlayColor='transparent' style={Styles.title}>
                        <Text style={Styles.itemTex}>{strings.preference}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor='transparent' style={Styles.itemRow}>
                        <Text style={Styles.itemTex}>{strings.historyorders}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor='transparent' style={Styles.itemRow}>
                        <Text style={Styles.itemTex}>{strings.edit_my_account_details}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor='transparent' style={Styles.itemRow}>
                        <Text style={Styles.itemTex}>{strings.edit_my_favorite}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor='transparent' style={Styles.itemRow}>
                        <Text style={Styles.itemTex}>{strings.set_as_homepage}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor='transparent' style={Styles.itemRow}>
                        <Text style={Styles.itemTex}>{strings.signout}</Text>
                    </TouchableHighlight>
                </Content>
                <Footer isChanged={this.state.dummy} navigation={this.props.navigation} />
                {this.state.onLeftSideMenu ?
                    <LeftSideMenu
                        refresh={this.refresh}
                        navigation={this.props.navigation}
                        hideLeftMenu={this.hideLeftMenu} />
                    : null}
            </Container >
        );
    }
    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }
    refresh = () => {
        if (strings.getLanguage() === 'en') {
            strings.setLanguage('ar')
        }
        else {
            strings.setLanguage('en')
        }
        // this.forceUpdate();
        this.props.dispatch(rootActions.switchLanguage(strings.getLanguage()));
        this.setState({ dummy: !this.state.dummy })
    }

    hideLeftMenu = () => {
        this.setState({ onLeftSideMenu: false });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        this.setState({ isGoneAlready: false });
        this.props.dispatch(loginActions.login(this.state.email, this.state.password));
    }
    onSignupNowPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
    onSubShop = (index) => {
        // Alert.alert('' + index);
        this.props.navigation.navigate(consts.SUBSHOP_SCREEN);
    }
    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
        // Alert.alert('test')
    }
    onPressWishList = () => {
        // this.props.navigation.navigate(consts.WISHLIST_SCREEN);
        // Alert.alert('test')
    }
    onPressEdit = () => {

    }
    onPressOpt = () => {
        this.setState({ onLeftSideMenu: true });
    }
}


const Styles = {
    containerStyle: {
        // alignItems: 'center',
        // backgroundColor: '#ffffff'
        paddingBottom: height / 9,
    },
    contentStyle: {
        // position: 'absolute',
        // top: height / 40 * (3.8 + 2),
        // height: height * 9 / 10 - StatusBar.currentHeight,
        // flex: 1,
        // flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        // height: height,
        resizeMode: 'contain',
    },
    avatar: {
        width: width,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        padding: width / 20,
        marginTop: width / 20,
        marginBottom: width / 10,
    },
    outter: {
        width: width / 4,
        height: width / 4,
        borderRadius: width / 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: width / 30
    },
    inner: {
        width: width / 4 - 6,
        height: width / 4 - 6,
        borderRadius: width / 8 - 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'center',
        backgroundColor: 'white'
    },
    profileImg: {
        width: width / 4 - 6,
        height: width / 4 - 6,
        borderRadius: width / 8 - 3,
        alignItems: 'center',
    },
    name: {
        fontSize: dimens.h2
    },
    title: {
        paddingBottom: height / 70,
        paddingTop: height / 70,
        paddingLeft: width / 20,
        paddingRight: width / 20,
        width: width,
        backgroundColor: '#aaaaaa',
        justifyContent: 'center',
    },
    itemRow: {
        paddingBottom: height / 50,
        paddingTop: height / 50,
        paddingLeft: width / 20,
        paddingRight: width / 20,
        width: width,
        borderColor: '#aaaaaa',
        borderBottomWidth: 1,
        justifyContent: 'center',
        backgroundColor: 'white'
    }
};

const mapStateToProps = (state) => ({
    Profile: state.get('profile'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Profile)