import React, { Component } from "react";
import { Modal, Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, TouchableHighlight, AsyncStorage } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./SubShop_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as subShopActions from "../actions/subShop-actions";
import * as rootActions from "../actions/root-actions";
import * as wishListActions from "../actions/wishList-actions";
import background from '../assets/shop_background.png';
import filter_icon from '../assets/icons/filter.png'
import sample from '../assets/sample.png';
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
import LeftSideMenu from "./Stat_LeftSideMenu";
import axios from 'axios';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
const CacelToken = axios.CancelToken;
let cancel;
const domain = "https://victory.softwarehouse.io/";
export class SubShop extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            isGoneAlready: false,
            onLeftSideMenu: false,
            dummy: false,
            collectionCode: 'any',
            products: []
        }
    }

    componentWillUnmount() {
        if (cancel != undefined)
            cancel();
    }
    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false));
        this.getProducts(this.state.collectionCode, "", "");
    }

    componentDidUpdate() {
        var flag = this.props.filter.get('isChanged');
        console.log(flag)
        // if (flag == true) {
        //     console.log('---------Update-------');
        // }
    }

    getProducts(collectionCode, kitCode, productSort) {
        if (cancel != undefined)
            cancel();
        this.setState({ products: [] });
        this.forceUpdate();
        const query = `
                    query products {
                        getProducts(collectionId: ${0}, collectionCode: "${collectionCode}", kitCode: "${kitCode}", productSort: "${productSort}") {
                            name
                            price
                            thumbnail
                            id
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.setState({ products: res.data.data.getProducts });
                this.forceUpdate();
            });
        } catch (error) {
            return null;
        }
    }


    isObject(obj) {
        return typeof obj === 'object';
    }

    setSearch = (text) => {

    }

    displayProducts() {
        if (this.state.products.length == 0)
            return null;
        let myProducts = [];
        for (let i = 0; i < (this.state.products.length - this.state.products.length % 2) / 2; i++) {
            myProducts.push(
                <View key={i} style={Styles.cellrow}>
                    <TouchableHighlight key={i * 2} underlayColor='transparent' style={Styles.cell} onPress={() => this.selectGoods(this.state.products[i * 2].id)}>
                        <View style={{ justifyContent: 'flex-start' }}>
                            <Image style={Styles.itemImg} source={{ uri: domain + this.state.products[i * 2].thumbnail }} />
                            <Text style={Styles.name}>{this.state.products[i * 2].name}</Text>
                            <Text style={Styles.price}>Aed {this.state.products[i * 2].price}</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight key={i * 2 + 1} underlayColor='transparent' style={Styles.cell} onPress={() => this.selectGoods(this.state.products[i * 2 + 1].id)}>
                        <View style={{ justifyContent: 'flex-start' }}>
                            <Image style={Styles.itemImg} source={{ uri: domain + this.state.products[i * 2 + 1].thumbnail }} />
                            <Text style={Styles.name}>{this.state.products[i * 2 + 1].name}</Text>
                            <Text style={Styles.price}>Aed {this.state.products[i * 2 + 1].price}</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            );
        }
        if (this.state.products.length % 2 == 1) {
            myProducts.push(
                <View key={this.state.products.length - 1} style={Styles.cellrow}>
                    <TouchableHighlight underlayColor='transparent' style={Styles.cell} onPress={(index) => this.selectGoods(this.state.products[this.state.products.length - 1].id)}>
                        <View>
                            <Image style={Styles.itemImg} source={{ uri: domain + this.state.products[this.state.products.length - 1].thumbnail }} />
                            <Text style={Styles.name}>{this.state.products[this.state.products.length - 1].name}</Text>
                            <Text style={Styles.price}>Aed {this.state.products[this.state.products.length - 1].price}</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            );
        }
        return myProducts;
    }
    setCollectionCode = (collectionCode) => {
        if (cancel != undefined)
            cancel();
        this.setState({ collectionCode: collectionCode });
        this.getProducts(collectionCode, "", "");
    }
    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header
                    setCollectionAsClubs={(collectionCode) => this.setCollectionCode('clubs')}
                    setCollectionAsWorldcup={(collectionCode) => this.setCollectionCode('worldcup')}
                    setCollectionAsPlayers={(collectionCode) => this.setCollectionCode('players')}
                    setCollectionAsMerchandise={(collectionCode) => this.setCollectionCode('merchandise')}
                    isChanged={this.state.dummy}
                    onPressWishList={this.onPressWishList}
                    onPressOption={this.onPressOpt} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <SearchTextInput
                        onChangeText={(text) => this.setSearch(text)}
                        placeholder={strings.search}
                        color={'black'}
                        secureTextEntry={false} />
                    <View style={Styles.cellhdr}>
                        <TouchableOpacity style={Styles.filterBtn} onPress={this.onFilterPress}>
                            <Image style={Styles.filterBtnImg} source={filter_icon} />
                        </TouchableOpacity>
                        <Text style={Styles.total}>{this.state.products.length} items</Text>
                    </View>
                    {this.displayProducts()}
                    {this.renderProgress()}
                </Content>
                {this.state.onLeftSideMenu ?
                    <LeftSideMenu
                        refresh={this.refresh}
                        navigation={this.props.navigation}
                        hideLeftMenu={this.hideLeftMenu} />
                    : null}
                <Footer
                    isChanged={this.state.dummy}
                    navigation={this.props.navigation} />
            </Container >
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }
    refresh = () => {
        if (strings.getLanguage() === 'en') {
            strings.setLanguage('ar')
        }
        else {
            strings.setLanguage('en')
        }
        // this.forceUpdate();
        this.props.dispatch(rootActions.switchLanguage(strings.getLanguage()));
        this.setState({ dummy: !this.state.dummy })
    }

    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
    }
    onFilterPress = () => {
        this.props.navigation.navigate(consts.FILTER_SCREEN);
    }
    onPressWishList = () => {
        this.props.navigation.navigate(consts.WISHLIST_SCREEN);
    }
    selectGoods(index) {
        // console.log('test component: ', index);
        // console.log('test in sub shop: ', this.state.products[index].id);
        this.props.dispatch(subShopActions.setProductId(index));
        this.props.navigation.navigate(consts.GOODS_SCREEN);
    }
    onPressOpt = () => {
        this.setState({ onLeftSideMenu: true });
    }
    hideLeftMenu = () => {
        this.setState({ onLeftSideMenu: false });
    }
}


const Styles = {
    containerStyle: {
        paddingBottom: height / 9,
    },
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        resizeMode: 'contain',
    },
    cellhdr: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: width * 17 / 20,
        marginTop: height / 90,
        marginBottom: height / 100
    },
    filterBtn: {

    },
    filterBtnImg: {
        width: width / 10,
        height: width / 20,
        resizeMode: 'contain'
    },
    total: {
        fontFamily: 'Roboto-Regular',
        color: colors.itemContent,
        fontSize: dimens.item
    },
    cellrow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems: 'center',
        alignSelf: 'center',
        width: width * 17 / 20,
        marginTop: height / 100,
    },
    cell: {
        width: width / 20 * 7.875,
    },
    itemImg: {
        width: width / 20 * 7.875,
        height: width / 20 * 7.875 * 186 / 156,
        borderRadius: 10,
        backgroundColor: colors.itemNameBack,
        resizeMode: 'contain',
        overflow: 'hidden'
    },
    name: {
        fontSize: dimens.item,
        color: colors.itemContent,
        fontFamily: 'Roboto-Medium'
    },
    price: {
        fontSize: dimens.small,
        color: colors.subItemContent,
        fontFamily: 'Roboto-Regular'
    }
};

const mapStateToProps = (state) => ({
    SubShop: state.get('subShop'),
    root: state.get('root'),
    filter: state.get('filter'),
});

export default connect(mapStateToProps)(SubShop)