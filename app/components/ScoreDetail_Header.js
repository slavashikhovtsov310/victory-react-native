
import React, { Component } from "react";
import { Content, Input, Item, Label, Text, Left, View, } from "native-base";
import { Alert, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, Platform } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import opt_icon from '../assets/icons/opt.png'
import star_icon from '../assets/icons/bell.png'
import { connect } from "react-redux";
import * as rootActions from "../actions/root-actions";
import victory_star_icon from '../assets/icons/victory_star.png'
import colors from "../resources/colors";
import dimens from "../resources/dimens";
import LinearGradient from 'react-native-linear-gradient';
import yellowCard_icon from '../assets/icons/yellowCard.png'
import redCard_icon from '../assets/icons/redCard.png'
import shot_icon from '../assets/icons/shot.png'
import substitution_icon from '../assets/icons/substitution.png'
import styles from "../resources/styles";
import null_logo from '../assets/teamlogo_null.png';

const { width, height } = Dimensions.get('window');


export class ScoreDetail_Header extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            }),
            localTeam: {},
            localTeamEvents: [],
            localTeamScore: '',
            currentTime: '',
            visitorTeam: {},
            visitorTeamEvents: [],
            visitorTeamScore: ''
        };
        // Alert.alert(width + ', ' + height);
    }
    componentDidMount() {
        this.setState({ localTeam: this.props.scoreDetail.get('localTeam') });
        this.setState({ localTeamEvents: this.props.scoreDetail.get('localTeamEvents') });
        this.setState({ localTeamScore: this.props.scoreDetail.get('localTeamScore') });
        this.setState({ currentTime: this.props.scoreDetail.get('currentTime') });
        this.setState({ visitorTeam: this.props.scoreDetail.get('visitorTeam') });
        this.setState({ visitorTeamEvents: this.props.scoreDetail.get('visitorTeamEvents') });
        this.setState({ visitorTeamScore: this.props.scoreDetail.get('visitorTeamScore') });
    }
    getEventImg(type) {
        switch (type) {
            case 'yellowcard':
                return yellowCard_icon;
            case 'goal':
                return shot_icon;
            case 'substitution':
                return substitution_icon;
            case 'redcard':
                return redCard_icon;
            default:
                shot_icon;
        }
    }
    getLocalTeamEvents() {
        return this.state.localTeamEvents.map((event, index) => {
            return (
                <View key={index} style={Styles.A_Team_name_group}>
                    <Image style={[Styles.shot_Mark, { marginLeft: width / 30 }, event.type == 'goal' ? { tintColor: 'white' } : {}]} source={this.getEventImg(event.type)} />
                    <Text style={[Styles.eventText, { marginLeft: width / 90 }]}>{event.minute} {event.playerName}</Text>
                </View>
            );
        });
    }
    getVisitorTeamEvents() {
        return this.state.visitorTeamEvents.map((event, index) => {
            return (
                <View key={index} style={Styles.B_Team_name_group}>
                    <Text style={[Styles.eventText, { marginRight: width / 90 }]}>{event.playerName} {event.minute}</Text>
                    <Image style={[Styles.shot_Mark, { marginRight: width / 30 }, event.type == 'goal' ? { tintColor: 'white' } : {}]} source={this.getEventImg(event.type)} />
                </View>
            );
        });
    }
    render() {
        return (
            <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={Styles.containerStyle}>
                <View
                    style={Styles.row}>
                    <TouchableOpacity style={Styles.optionBtn} onPress={this.props.onPressOpt}>
                        <Image source={opt_icon}
                            style={Styles.optionStyle} />
                    </TouchableOpacity>
                    <Text style={Styles.labelStyle}>Victory</Text>
                    <TouchableOpacity style={Styles.exitBtn} onPress={this.props.onPressWishList} >
                        <Image source={star_icon}
                            style={Styles.exitStyle} />
                    </TouchableOpacity>
                </View>
                <Text style={Styles.sublabel}>Stat</Text>
                <View style={Styles.match}>
                    <View style={Styles.A_Team}>
                        <View style={Styles.A_Team_name_group}>
                            <Image style={Styles.Team_Mark}
                                source={this.state.localTeam.logoPath != '' ? { uri: this.state.localTeam.logoPath } : null_logo} />
                            <Text style={[Styles.Team_name, { marginLeft: width / 90 }]}>{this.state.localTeam.name}</Text>
                        </View>
                        {this.getLocalTeamEvents()}
                    </View>
                    <View style={Styles.match_result}>
                        {this.state.localTeamScore > this.state.visitorTeamScore ?
                            <Image style={[Styles.victory_star, { left: -10 }]} source={victory_star_icon} /> :
                            null}
                        <Text style={Styles.vs}>{this.state.localTeamScore} : {this.state.visitorTeamScore}</Text>
                        <Text style={{ color: 'white', fontFamily: 'Roboto-Regular', fontSize: dimens.small }}>{this.state.currentTime == null ? '' : this.state.currentTime}</Text>
                        {this.state.localTeamScore < this.state.visitorTeamScore ?
                            <Image style={[Styles.victory_star, { right: -10 }]} source={victory_star_icon} /> :
                            null}
                    </View>
                    <View style={Styles.B_Team}>
                        <View style={Styles.B_Team_name_group}>
                            <Text style={[Styles.Team_name, { marginRight: width / 90 }]}>{this.state.visitorTeam.name}</Text>
                            <Image style={Styles.Team_Mark}
                                source={this.state.visitorTeam.logoPath != '' ? { uri: this.state.visitorTeam.logoPath } : null_logo} />
                        </View>
                        {this.getVisitorTeamEvents()}
                    </View>
                </View>
                <View style={Styles.tabPan}>
                    <View style={Styles.tabbar}>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.maingradientBorder} />
                        <TouchableOpacity style={Styles.tabCell}
                            onPress={this.props.onPressGameinfo}>
                            <Text style={Styles.tabItem}>{strings.gameinfo}</Text>
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={this.props.isGameinfo ? ['#81d8ae', '#8091da'] : ['transparent', 'transparent']}
                                style={Styles.subgradientBorder} />
                        </TouchableOpacity>
                        <TouchableOpacity style={Styles.tabCell}
                            onPress={this.props.onPressLineup}>
                            <Text style={Styles.tabItem}>{strings.lineup}</Text>
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={this.props.isLineup ? ['#81d8ae', '#8091da'] : ['transparent', 'transparent']}
                                style={Styles.subgradientBorder} />
                        </TouchableOpacity>
                        <TouchableOpacity style={Styles.tabCell}
                            onPress={this.props.onPressGamestats}>
                            <Text style={Styles.tabItem}>{strings.gamestats}</Text>
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={this.props.isGamestats ? ['#81d8ae', '#8091da'] : ['transparent', 'transparent']}
                                style={Styles.subgradientBorder} />
                        </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient >
        );
    }
}

const Styles = {
    containerStyle: {
        alignItems: 'center',
        zIndex: 1000
    },
    row: {
        flexDirection: 'row',
        width: width,
        marginTop: height / 50,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionBtn: {
        position: 'absolute',
        left: 20,
        width: width / 15,
        height: width / 15,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionStyle: {
        resizeMode: 'contain',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        tintColor: 'white',
        width: width / 15,
        height: width / 15,
    },
    labelStyle: {
        fontSize: dimens.header,
        color: 'white',
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
    },
    sublabel: {
        color: colors.header_sub2,
        paddingLeft: width / 10,
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
        fontStyle: 'italic',
        fontSize: 10
    },
    exitBtn: {
        position: 'absolute',
        right: 20,
        width: width / 15,
        height: width / 15,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    exitStyle: {
        width: width / 15,
        height: width / 15,
        tintColor: 'white',
        resizeMode: 'contain',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabPan: {
        backgroundColor: 'white',
        width: width,
        justifyContent: 'center',
        padding: width / 20
    },
    tabbar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: height / 50,
        paddingBottom: -3,
        backgroundColor: 'white',
        width: width / 10 * 9
    },
    tabItem: {
        fontSize: dimens.item,
        color: colors.itemContent,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        fontFamily: 'Roboto-Medium'
    },
    tabCell: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginBottom: -3,
        paddingBottom: 3
    },
    match: {
        flexDirection: 'row',
        width: width,
        paddingLeft: width / 20,
        paddingRight: width / 20,
        justifyContent: 'space-between',
        paddingTop: width / 30,
        paddingBottom: width / 50,
    },
    A_Team_name_group: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    B_Team_name_group: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    Team_Mark: {
        width: width / 30 * 2,
        height: width / 30 * 2,
        resizeMode: 'contain'
    },
    Team_name: {
        fontSize: dimens.item,
        fontFamily: 'Roboto-Medium',
        color: 'white',
    },
    shot_Mark: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    card_Mark: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    match_result: {
        alignItems: 'center'
    },
    vs: {
        fontSize: dimens.h2,
        fontFamily: 'Roboto-Medium',
        color: 'white'
    },
    victory_star: {
        position: 'absolute',
        top: -2,
    },
    subgradientBorder: {
        alignSelf: 'stretch',
        height: 3,
        zIndex: 10000
    },
    maingradientBorder: {
        position: 'absolute',
        bottom: 0,
        width: width / 10 * 9,
        height: 3,
        // marginTop: ,
        // zIndex: -100
    },
    A_Team: {
        width: width / 5 * 2
    },
    B_Team: {
        width: width / 5 * 2
    },
    eventText: {
        color: 'white',
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.small
    }
};

ScoreDetail_Header.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};

const mapStateToProps = (state) => ({
    root: state.get('root'),
    scoreDetail: state.get('scoreDetail')
});

export default connect(mapStateToProps)(ScoreDetail_Header)