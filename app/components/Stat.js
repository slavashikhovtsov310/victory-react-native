import React, { Component } from "react";
import { Image, StatusBar, Text, Dimensions, AsyncStorage } from "react-native";
import { Container, Spinner, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Stat_Header";
import LeftSideMenu from "./Stat_LeftSideMenu";
import RightSideMenu from "./Stat_RightSideMenu";
import AllScores from "./Stat_AllScores";
import MyScores from "./Stat_MyScores";
import Standings from "./Stat_Standings";
import Footer from "./Stat_Footer";
import strings from "../resources/strings";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import Orientation from 'react-native-orientation';
import { NavigationActions, StackActions  } from 'react-navigation';
import { call, put, take } from "redux-saga/effects";
const { width, height } = Dimensions.get('window');

console.ignoredYellowBox = ['Warning:'];
export class Stat extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            allscores: true,
            myscores: false,
            standings: false,
            table: true,
            topscores: false,
            onLeftSideMenu: false,
            onRightSideMenu: false,
            dummy: true
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
        this.getKey();
        // const resetAction = StackActions.reset({
        //     index: 0,
        //     actions: [
        //         NavigationActions.navigate({ routeName: consts.STAT_SCREEN })
        //     ]
        // });
        // this.props.navigation.dispatch(resetAction);
        // console.log('test', this.props.navigation);
    }


    componentDidUpdate() {
        this.proceed();
    }
    async getKey() {
        try {
            const value = await AsyncStorage.getItem('@Victory:favLeagues');
            let myFavLeague = [];
            if (value == null)
                return;
            myFavLeague = JSON.parse(value);
            this.props.dispatch(rootActions.setMyFavoriteLeagues(myFavLeague))
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }
    proceed() {
        const lang = this.props.root.get('lang');
        if (lang) {
            strings.setLanguage(lang);
            this.forceUpdate;
        }
        else {
            strings.setLanguage('en');
            this.forceUpdate;
        }
    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    setSearch = (text) => {

    }
    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header
                    isChange={this.state.dummy}
                    isAllScores={this.state.allscores ? true : false}
                    isMyScores={this.state.myscores ? true : false}
                    isStandings={this.state.standings ? true : false}
                    onPressOpt={this.onPressOpt}
                    onPressFavorite={this.onPressFavorite}
                    onPressAllscores={this.onPressAllscores}
                    onPressMyScores={this.onPressMyScores}
                    onPressStandings={this.onPressStandings} />
                {this.state.allscores ?
                    <AllScores isChange={this.state.dummy} navigation={this.props.navigation} />
                    : null}
                {this.state.myscores ?
                    <MyScores isChange={this.state.dummy} navigation={this.props.navigation} />
                    : null}
                {this.state.standings ?
                    <Standings
                        isChange={this.state.dummy} />
                    : null}
                <Footer navigation={this.props.navigation} isChange={this.state.dummy} />
                {this.state.onLeftSideMenu ?
                    <LeftSideMenu
                        navigation={this.props.navigation}
                        hideLeftMenu={this.hideLeftMenu}
                        refresh={this.refresh} />
                    : null}
                {this.state.onRightSideMenu ?
                    <RightSideMenu
                        navigation={this.props.navigation}
                        hideRightMenu={this.hideRightMenu} />
                    : null}
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }
    renderTabPannel() {
        if (this.state.allscores)
            return AllScores;
    }
    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
    }
    onFilterPress = () => {
        this.props.navigation.navigate(consts.FILTER_SCREEN);
    }
    onPressOpt = () => {
        this.setState({ onLeftSideMenu: true });
    }
    hideLeftMenu = () => {
        this.setState({ onLeftSideMenu: false });
    }
    refresh = () => {
        if (strings.getLanguage() === 'en') {
            strings.setLanguage('ar')
        }
        else {
            strings.setLanguage('en')
        }
        this.saveKey(strings.getLanguage());
        this.props.dispatch(rootActions.switchLanguage(strings.getLanguage()));
        this.setState({ dummy: !this.state.dummy })
    }
    async saveKey(value) {
        try {
            await AsyncStorage.setItem('@Victory:key', value);
        } catch (error) {
            console.warn("Error saving data" + error);
        }
    }

    async resetKey() {
        try {
            await AsyncStorage.removeItem('@Victory:key');
            const value = await AsyncStorage.getItem('@Victory:key');
            this.setState({ myKey: value });
        } catch (error) {
            console.log("Error resetting data" + error);
        }
    }
    hideRightMenu = () => {
        this.setState({ onRightSideMenu: false });
    }
    onPressFavorite = () => {
        this.setState({ onRightSideMenu: true });
    }
    onPressAllscores = () => {
        this.setState({
            allscores: true,
            myscores: false,
            standings: false,
        });
    }
    onPressMyScores = () => {
        this.setState({
            allscores: false,
            myscores: true,
            standings: false,
        });
    }
    onPressStandings = () => {
        this.setState({
            allscores: false,
            myscores: false,
            standings: true,
        });
    }
    onPressTable = () => {
        this.setState({
            table: true,
            topscores: false
        });
    }
    onPressTopScores = () => {
        this.setState({
            table: false,
            topscores: true
        });
    }
}


const Styles = {
    containerStyle: {
        paddingBottom: height / 9,
    },
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {

    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        resizeMode: 'stretch',
    },
};

const mapStateToProps = (state) => ({
    Stat: state.get('stat'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Stat)