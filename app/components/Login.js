import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, AsyncStorage } from "react-native";
import { Button, Container, Content, Spinner, View } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/background.png'
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
const { width, height } = Dimensions.get('window');
// const window = Dimensions.get('window')
// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class Login extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false,
            dummy: false
        }
        this.getKey();
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
        this.props.navigation.navigate(consts.SETUP_SCREEN);// for prototype
    }

    componentWillMount() {
        // let lang = this.props.root.get('lang');
        // console.warn(lang + 'test')
        // strings.setLanguage(lang)
        // strings.setLanguage('ar')
        // this.setState({ dummy: !this.state.dummy })
        // this.forceUpdate();
    }
    componentDidUpdate() {
        this.proceed()
    }
    async getKey() {
        try {
            const value = await AsyncStorage.getItem('@Victory:key');
            strings.setLanguage(value);
            this.setState({ dummy: true });
            this.props.dispatch(rootActions.switchLanguage(value))
            this.forceUpdate();
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
        this.setState({ dummy: true });
    }
    proceed() {
        // if (lang) {
        //     strings.setLanguage(lang);
        //     // this.setState({ dummy: !this.state.dummy });
        // }
        const loginError = this.props.Login.get('loginError');
        const isLoggedIn = this.props.Login.get('isLoggedIn');
        const toastStyles = {
            width: width,
            height: height / 6,
            backgroundColor: '#34343400',
            color: 'red',
            fontSize: dimens.text_size_button,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center'
        }
        // Alert.alert(this.state.email + ', ' + loginError.msg)
        // if (isLoggedIn)
        //     Alert.alert('1')
        // if (this.state.isGoneAlready)
        //     Alert.alert('2')
        // if (this.isObject(loginError.msg))
        //     Alert.alert('3')
        // if (loginError.msg)
        //     Alert.alert('4')
        // if (this.isObject(loginError) && loginError && !this.isObject(loginError.msg) && loginError.msg) {
        //     Toast.show(loginError.msg, Toast.SHORT, Toast.CENTER, toastStyles);
        //     // Alert.alert(loginError + '');
        //     this.props.dispatch(loginActions.setError({}))
        // } else if (isLoggedIn && !this.state.isGoneAlready) {
        //     this.setState({ isGoneAlready: true });
        //     this.props.navigation.navigate(consts.DASHBOARD_SCREEN);
        // }
    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    // noinspection JSMethodCanBeStatic
    setEmail(text) {
        this.setState({ email: text });
    }
    setPassword(text) {
        this.setState({ password: text });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    render() {
        return (

            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.form}>
                        {this.state.dummy ?
                            <ValidationTextInput
                                isChanged={this.state.dummy}
                                lang={strings.getLanguage()}
                                validate={this.validateEmail}
                                label={'email'}
                                onChangeText={(text) => this.email = text}
                                style={Styles.emailStyle}
                                color={'black'} /> : null}
                        {this.state.dummy ?
                            <ValidationTextInput
                                isChanged={this.state.dummy}
                                validate={this.validateEmail}
                                label={'password'}
                                secureTextEntry={true}
                                onChangeText={(text) => this.email = text}
                                style={Styles.emailStyle}
                                color={'black'} /> : null}
                        <TouchableOpacity clear={'false'} style={Styles.txtbtn}>
                            <Text style={Styles.btnText}>{strings.forgotpassword}</Text>
                        </TouchableOpacity>
                        <Button style={Styles.rectbtn} onPress={this.onLoginPress}>
                            <Text style={Styles.rectbtnText}>{strings.signin}</Text>
                        </Button>
                        <TouchableOpacity style={Styles.txtbtn}>
                            <Text style={Styles.btnText}>{strings.continueasaguest}</Text>
                            <Text style={Styles.txt}>{strings.donthaveanaccount}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={Styles.txtbtn} onPress={this.onSignupPress}>
                            <Text style={Styles.btnText}>{strings.signup}</Text>
                        </TouchableOpacity>
                    </View>
                    {this.renderProgress()}
                </Content>
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        // this.setState({ isGoneAlready: false });
        // this.props.dispatch(loginActions.login(this.state.email, this.state.password));
        this.props.navigation.navigate(consts.SETUP_SCREEN);
    }
    onSignupPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
}


const Styles = {
    containerStyle: {

        backgroundColor: '#ffffff'
    },
    contentStyle: {
        flex: 0,
        flexDirection: 'column',
        alignItems: 'center',
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        height: height,
        resizeMode: 'stretch',
    },
    emailStyle: {
        alignSelf: 'stretch',
    },
    form: {
        backgroundColor: 'white',
        shadowColor: '#333333',
        // shadowRadius: 10,
        // shadowOffset: 7,
        borderRadius: 15,
        width: width * 19 / 22,
        paddingLeft: width / 44 * 3,
        paddingRight: width / 44 * 3,
        paddingTop: width / 44 * 3,
        paddingBottom: width / 44 * 2.7,
        marginTop: height / 4,
    },
    txtbtn: {
        // backgroundColor: 'transparent',
        alignSelf: 'center',
        borderRadius: 0,
        // shadowColor: 'transparent',
        // shadowOffset: 0,
        marginBottom: width / 20,
    },
    btnText: {
        textDecorationLine: 'underline',
    },
    rectbtn: {
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        borderRadius: 10,
        marginBottom: width / 20,
    },
    rectbtnText: {
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center'
    },
    txt: {
        alignSelf: 'center',
    }
};

const mapStateToProps = (state) => ({
    Login: state.get('login'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Login)