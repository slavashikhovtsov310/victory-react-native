import React, { Component } from "react";
import { Content, Input, Item, Label, Text, Left, View, } from "native-base";
import { Alert, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, Platform } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import opt_icon from '../assets/icons/opt.png'
import star_icon from '../assets/icons/star.png'
import colors from "../resources/colors";
import dimens from "../resources/dimens";
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get('window');


export default class Stat_Header extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            })
        };
    }

    render() {
        return (
            <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={styles.containerStyle}>
                <View
                    style={styles.row}>
                    <TouchableOpacity style={styles.optionBtn} onPress={this.props.onPressOpt}>
                        <Image source={opt_icon}
                            style={styles.optionStyle} />
                    </TouchableOpacity>
                    <Text style={styles.labelStyle}>Victory</Text>
                    <TouchableOpacity style={styles.exitBtn} onPress={this.props.onPressFavorite} >
                        <Image source={star_icon}
                            style={styles.exitStyle} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.sublabel}>Stat</Text>
                <View style={styles.tabbar}>
                    <TouchableOpacity style={[styles.tabCell, this.props.isAllScores ? { borderColor: 'white', borderBottomWidth: 3 } : {}]}
                        onPress={this.props.onPressAllscores}>
                        <Text style={styles.tabItem}>{strings.allscores}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.tabCell, this.props.isMyScores ? { borderColor: 'white', borderBottomWidth: 3 } : {}]}
                        onPress={this.props.onPressMyScores}>
                        <Text style={styles.tabItem}>{strings.myscores}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.tabCell, this.props.isStandings ? { borderColor: 'white', borderBottomWidth: 3 } : {}]}
                        onPress={this.props.onPressStandings}>
                        <Text style={styles.tabItem}>{strings.standings}</Text>
                    </TouchableOpacity>
                </View>
            </LinearGradient >
        );
    }
}

const styles = {
    containerStyle: {
        alignItems: 'center',
        paddingBottom: height / 60,
        zIndex: 1000
    },
    row: {
        flexDirection: 'row',
        width: width,
        marginTop: height / 50,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionBtn: {
        position: 'absolute',
        left: 20,
        width: width / 15,
        height: width / 15,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionStyle: {
        resizeMode: 'contain',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 15,
        height: width / 15,
        tintColor: 'white'
    },
    labelStyle: {
        fontSize: dimens.header,
        color: 'white',
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
    },
    sublabel: {
        color: colors.header_sub,
        paddingLeft: width / 10,
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
        fontStyle: 'italic',
        fontSize: 10
    },
    exitBtn: {
        position: 'absolute',
        right: 20,
        width: width / 15,
        height: width / 15,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    exitStyle: {
        resizeMode: 'contain',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 15,
        height: width / 15,
        tintColor: 'white'
    },
    tabbar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: height / 50,
        borderBottomWidth: 3,
        paddingBottom: -3,
        borderColor: '#81d7af',
        width: width / 10 * 9
    },
    tabItem: {
        fontSize: 16,
        color: 'white',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        fontFamily: 'Roboto-Medium',
    },
    tabCell: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginBottom: -3,
        paddingBottom: 3
    }
};

Stat_Header.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};