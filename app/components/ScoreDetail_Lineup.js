import React from "react";
import { Content, Text, View, } from "native-base";
import { Image, Dimensions, TouchableOpacity, } from "react-native";
import strings from "../resources/strings"
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import colors from "../resources/colors";
import dimens from "../resources/dimens";
import { connect } from "react-redux";
import playground from '../assets/icons/playground.png'
import cart_icon from '../assets/icons/cart.png'
import playermark1_icon from '../assets/icons/table_item1.png'

const { width, height } = Dimensions.get('window');
const play_w = width / 10 * 9;
const play_h = width / 10 * 9 / 324 * 292;
export class ScoreDetail_Lineup extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            }),
            isTeamA: true,
            isTeamB: false
        };
    }
    onPressTeamA() {
        this.setState({
            isTeamA: true,
            isTeamB: false
        });
    }
    onPressTeamB() {
        this.setState({
            isTeamA: false,
            isTeamB: true
        });
    }
    getPlayersForBack() {
        let playerAttr = ['Ali Khasif', 'Ali Khasif', 'Ali Khasif', 'Ali Khasif'];
        return playerAttr.map((item, index) => {
            let flag = false;
            if (index == 0 || index == playerAttr.length - 1) {
                flag = true;
            }
            return (
                <View key={index} style={[Styles.player, flag ? { paddingBottom: play_h / 12 } : {}]}>
                    <Image style={Styles.playerImg} source={playermark1_icon} />
                    <Text style={Styles.playerName}>{item}</Text>
                </View>
            );
        });
    }
    getPlayersForMid() {
        let playerAttr = ['Ali Khasif', 'Ali Khasif', 'Ali Khasif'];
        return playerAttr.map((item, index) => {
            let flag = false;
            if (index == 0 || index == playerAttr.length - 1) {
                flag = true;
            }
            return (
                <View key={index} style={[Styles.player, flag ? { paddingBottom: play_h / 12 } : {}]}>
                    <Image style={Styles.playerImg} source={playermark1_icon} />
                    <Text style={Styles.playerName}>{item}</Text>
                </View>
            );
        });
    }
    getPlayersForForward() {
        let playerAttr = ['Ali Khasif', 'Ali Khasif', 'Ali Khasif'];
        return playerAttr.map((item, index) => {
            let flag = true;
            if (index == 0 || index == playerAttr.length - 1) {
                flag = false;
            }
            return (
                <View key={index} style={[Styles.player, flag ? { paddingBottom: play_h / 14 } : {}]}>
                    <Image style={Styles.playerImg} source={playermark1_icon} />
                    <Text style={Styles.playerName}>{item}</Text>
                </View>
            );
        });
    }
    render() {
        return (
            <Content contentContainerStyle={Styles.contentStyle}>
                <View style={Styles.tab_bar_back}>
                    <View style={Styles.tab_bar}>
                        <TouchableOpacity style={[Styles.left_tab, this.state.isTeamA ? { backgroundColor: '#57BD7E' } : {}]} onPress={this.onPressTeamA.bind(this)}>
                            <Text style={[Styles.btnText, this.state.isTeamA ? { color: 'white' } : {}]}>{this.props.scoreDetail.get('localTeam').name}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[Styles.right_tab, this.state.isTeamB ? { backgroundColor: '#57BD7E' } : {}]} onPress={this.onPressTeamB.bind(this)}>
                            <Text style={[Styles.btnText, this.state.isTeamB ? { color: 'white' } : {}]}>{this.props.scoreDetail.get('visitorTeam').name}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={Styles.hdr}>
                    <Text style={Styles.teamName}>{this.state.isTeamA ? this.props.scoreDetail.get('localTeam').name : this.props.scoreDetail.get('visitorTeam').name}</Text>
                    <Text style={Styles.teamformation}>-4-3-3</Text>
                </View>
                <View style={Styles.postPan}>
                    <Image style={Styles.playground} source={playground} />
                    <View style={Styles.goalKeeper}>
                        <Text style={Styles.playerName}>Ali Khasif</Text>
                        <Image style={Styles.playerImg} source={playermark1_icon} />
                    </View>
                    <View style={Styles.back}>
                        {this.getPlayersForBack()}
                    </View>
                    <View style={Styles.mid}>
                        {this.getPlayersForMid()}
                    </View>
                    <View style={Styles.forward}>
                        {this.getPlayersForForward()}
                    </View>
                </View>
                <TouchableOpacity style={Styles.buyBtn} onPress={this.buyTeamJersey}>
                    <Image source={cart_icon} />
                    <Text style={Styles.buyBtnText}>{strings.buyteamjersey}</Text>
                </TouchableOpacity>
            </Content>
        );
    }
}

const Styles = {
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    postPan: {
        marginTop: width / 30,
        width: width / 10 * 9,
        height: width / 10 * 9 / 324 * 292,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    tab_bar_back: {
        width: width,
        paddingBottom: width / 30,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    tab_bar: {
        width: width / 3.5 * 2,
        height: height / 25,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: width / 30,
        overflow: 'hidden',
        borderColor: '#57BD7E',
        borderRadius: height / 50,
        borderWidth: 1,
    },
    left_tab: {
        width: width / 3.5,
        height: height / 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    right_tab: {
        width: width / 3.5,
        height: height / 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnText: {
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.item,
        color: colors.itemContent,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    hdr: {
        marginTop: width / 30,
        width: width / 10 * 9,
        justifyContent: 'center',
        alignItems: 'center',
    },
    teamName: {
        fontFamily: 'Roboto-Regular',
        color: colors.itemContent,
        position: 'absolute',
        left: 0,
        fontSize: dimens.h2,
    },
    teamformation: {
        fontSize: dimens.small,
        fontFamily: 'Roboto-Regular',
        color: colors.itemContent
    },
    playground: {
        position: 'absolute',
        top: 0,
        width: width / 10 * 9,
        resizeMode: 'stretch',
        height: width / 10 * 9 / 324 * 292,
    },
    goalKeeper: {
        position: 'absolute',
        bottom: play_h / 292 * 8,
    },
    player: {
        // backgroundColor: 'yellow',
        justifyContent: 'flex-end'
    },
    playerImg: {
        alignSelf: 'center',
        width: width / 10,
        height: width / 10,
        resizeMode: 'contain'
    },
    playerName: {
        fontSize: dimens.small,
        fontFamily: 'Roboto-Regular',
        color: colors.itemContent
    },
    back: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: play_h / 7 * 2,
        width: play_w / 10 * 9,
        // alignSelf: 'center',
        justifyContent: 'space-between'
    },
    mid: {
        flexDirection: 'row',
        position: 'absolute',
        bottom: play_h / 2 - play_h / 24,
        width: play_w / 10 * 7,
        justifyContent: 'space-between'
    },
    forward: {
        flexDirection: 'row',
        position: 'absolute',
        top: play_h / 292 * 8,
        width: play_w / 10 * 8,
        justifyContent: 'space-between'
    },
    buyBtn: {
        backgroundColor: '#57BD7E',
        marginTop: width / 20,
        flexDirection: 'row',
        paddingLeft: width / 30,
        paddingRight: width / 30,
        padding: width / 30,
        alignItems: 'center',
        borderRadius: 15,
    },
    buyBtnText: {
        color: 'white',
        marginLeft: width / 30,
    }
};

ScoreDetail_Lineup.propTypes = {
    defaultValue: PropTypes.string
};


const mapStateToProps = (state) => ({
    root: state.get('root'),
    scoreDetail: state.get('scoreDetail')
});

export default connect(mapStateToProps)(ScoreDetail_Lineup)