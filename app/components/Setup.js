import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, BackHandler, Platform } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/setup_background.png';
import stat_icon from '../assets/icons/stat_big.png';
import shop_icon from '../assets/icons/shop_big.png';
import swipe_icon from '../assets/icons/swipe.png';
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
import Swiper from '@nart/react-native-swiper';
import Stat from '../components/Stat';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class Setup extends Component {

    static navigationOptions = {
        header: null
    };


    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false,
            index: 1
        }
        this.swiper
        // strings.setLanguage('ar')
        if (Platform.OS !== 'android') return;
        // this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        console.log('------------------------------------------------------');
    }
    handleBackButtonClick() {
        // Alert.alert('test1')
        Orientation.lockToPortrait();
        // this.props.navigation.pop();
        // this.props.navigation.navigate(consts.LOGIN_SCREEN)
        return true;
    }
    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        console.log('test----------------------------', this.props.navigation);
        // this.swiper.scrollBy(1);
        console.log('test: -----------------------------------------');
        console.log('' + this.swiper.index)
    }

    componentWillUnmount = () => {
        // Alert.alert('out')
        // if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick)
    }
    componentDidUpdate() {
    }


    indexChanged = (index) => {
        // this.props.navigation.navigate(consts.STAT_SCREEN);
        // this.setState({ index: 0 });
        // this.forceUpdate();
        console.log('test: ', index)
    }

    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Swiper
                    loop={false}
                    showsPagination={false}
                    ref={(swiper) => { this.swiper = swiper; }}
                    index={1}
                    onIndexChanged={this.indexChanged}
                    horizontal={false}
                    showsButtons={false}>
                    <View style={{ height: height, width: width, flex: 1 }}>
                        <View
                            style={[{ width: width, alignItems: 'center' },
                            Platform.OS === 'android' ? { height: height / 9 * 8 - 20 + StatusBar.currentHeight }
                                : { height: height / 9 * 8 - 20 }]}>
                            <Text style={Styles.hi}>{strings.hi}, Mohammed.</Text>
                            <Text style={Styles.subhi}>{strings.please_choose_to_set_up_your_home_screen}</Text>
                            <View style={Styles.row}>
                                <View style={Styles.cellgroup}>
                                    <TouchableOpacity style={Styles.cell} onPress={this.onStatPress}>
                                        <Image style={Styles.btnImg} source={stat_icon} />
                                    </TouchableOpacity>
                                    <Text style={Styles.btnText}>{strings.stat}</Text>
                                </View>
                                <View style={Styles.cellgroup}>
                                    <TouchableOpacity style={Styles.cell} onPress={this.onShopPress}>
                                        <Image style={Styles.btnImg} source={shop_icon} />
                                    </TouchableOpacity>
                                    <Text style={Styles.btnText}>{strings.shop}</Text>
                                </View>
                            </View>
                        </View>
                        <View
                            style={Styles.narator}>
                            <Text style={Styles.naratorText}>{strings.swipe_up_for_todays_match}</Text>
                            <Image source={swipe_icon} />
                        </View>
                    </View>
                    <View style={{ flex: 1, }}>
                        <Stat
                            navigation={this.props.navigation} />
                    </View>
                </Swiper>
                {this.renderProgress()}
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    onShopPress = () => {
        // this.setState({ isGoneAlready: false });
        // this.props.dispatch(loginActions.login(this.state.email, this.state.password));
        if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick)
        this.props.navigation.navigate(consts.SHOP_SCREEN);
    }
    onStatPress = () => {
        if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick)
        this.props.navigation.navigate(consts.STAT_SCREEN);
    }

    onPressSwipeBtn = () => {
        console.log('pressIn test')
    }

}


const Styles = {
    containerStyle: {
        // alignItems: 'center',
        backgroundColor: '#ffffff',
        height: height
    },
    contentStyle: {
        // flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: width,
        // height: height,
        justifyContent: 'space-between',
        height: height
    },
    statusBarStyle: {
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        height: height,
        resizeMode: 'stretch',
    },
    hi: {
        fontFamily: 'Roboto-Bold',
        color: 'white',
        // fontWeight: 'bold',
        marginTop: height / 32 * 9,
        fontSize: 26,
        textAlign: 'center'
    },
    subhi: {
        fontFamily: 'Roboto-Bold',
        color: 'white',
        width: width / 7 * 4,
        fontSize: 15,
        textAlign: 'center',
        alignSelf: 'center'
    },
    row: {
        flexDirection: 'row',
        marginTop: height / 17,
    },
    cellgroup: {
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: width / 10
    },
    cell: {
        width: width / 9.5 * 1.8,
        height: width / 9.5 * 1.8,
        borderRadius: width / 9.5 * 1.8 / 2,
        backgroundColor: 'white',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnImg: {
        width: width / 10,
        height: width / 10,
        resizeMode: 'contain'
    },
    btnText: {
        fontFamily: 'Roboto-Medium',
        color: 'white',
        fontSize: 13,
        marginTop: height / 60
    },
    narator: {
        // position: 'absolute',
        // bottom: 20,
        paddingBottom: 20,
        height: height / 9,
        paddingTop: 0,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    naratorText: {
        fontFamily: 'Roboto-Medium',
        fontSize: 13,
        color: 'white',
        marginBottom: height / 50,
    },
};

const mapStateToProps = (state) => ({
    Setup: state.get('setup'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Setup)