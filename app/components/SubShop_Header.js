import React, { Component } from "react";
import { Content, Input, Item, Label, Text, Left, View, } from "native-base";
import { Alert, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, Platform } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import opt_icon from '../assets/icons/opt.png'
import star_icon from '../assets/icons/star.png'
import colors from "../resources/colors";
import dimens from "../resources/dimens";
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get('window');


export default class SubShop_Header extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            }),
            filter: [false, false, false, false]
        };
    }
    toggleDrawer = () => {

        console.log(this.props.navigationProps);
        Alert.alert('test sandwich')

        this.props.navigationProps.toggleDrawer();

    }
    onPress = (index) => {
        console.log(index);
        let filter = [];
        filter = this.state.filter;
        for (var i = 0; i < filter.length; i++) {
            filter[i] = false;
        }
        filter[index] = true;
        this.setState({ filter: filter });
        // let collectionCode = '';
        switch (index) {
            case 0:
                collectionCode = 'clubs';
                this.props.setCollectionAsClubs();
                break;
            case 1:
                collectionCode = 'worldcup';
                this.props.setCollectionAsWorldcup();
                break;
            case 2:
                collectionCode = 'players';
                this.props.setCollectionAsPlayers();
                break;
            case 3:
                collectionCode = 'merchandise';
                this.props.setCollectionAsMerchandise();
                break;
            default:
                // collectionCode = 'any';
                break;
        }
        this.forceUpdate();
    }
    render() {
        return (
            <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={styles.containerStyle}>
                <View
                    style={styles.row}>
                    <TouchableOpacity style={styles.optionBtn} onPress={this.props.onPressOption} >
                        <Image source={opt_icon}
                            style={styles.optionStyle} />
                    </TouchableOpacity>
                    <Text style={styles.labelStyle}>Victory</Text>
                    <TouchableOpacity style={styles.exitBtn} onPress={this.props.onPressWishList} >
                        <Image source={star_icon}
                            style={styles.exitStyle} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.sublabel}>shop</Text>
                <ScrollView horizontal={true} showsVerticalScrollIndicator={false}>
                    <View style={styles.tabbar}>
                        <TouchableOpacity
                            style={[styles.tabCell, this.state.filter[0] ? { borderColor: 'white', borderBottomWidth: 1, paddingBottom: 2 } : {}]}
                            onPress={() => this.onPress(0)}>
                            <Text style={styles.tabItem}>{strings.club}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.tabCell, this.state.filter[1] ? { borderColor: 'white', borderBottomWidth: 1, paddingBottom: 2 } : {}]}
                            onPress={() => this.onPress(1)}>
                            <Text style={styles.tabItem}>{strings.internationalteam}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.tabCell, this.state.filter[2] ? { borderColor: 'white', borderBottomWidth: 1, paddingBottom: 2 } : {}]}
                            onPress={() => this.onPress(2)}>
                            <Text style={styles.tabItem}>{strings.player}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.tabCell, this.state.filter[3] ? { borderColor: 'white', borderBottomWidth: 1, paddingBottom: 2 } : {}]}
                            onPress={() => this.onPress(3)}>
                            <Text style={styles.tabItem}>{strings.merchandise}</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </LinearGradient >
        );
    }
}

const styles = {
    containerStyle: {
        alignItems: 'center',
        paddingBottom: height / 70,
        zIndex: 1000
    },
    row: {
        flexDirection: 'row',
        width: width,
        marginTop: height / 50,
        // height: height / 40 * 3.8,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionBtn: {
        position: 'absolute',
        left: 20,
        width: width / 15,
        height: width / 15,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionStyle: {
        resizeMode: 'contain',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 15,
        height: width / 15,
        tintColor: 'white'
    },
    labelStyle: {
        fontSize: dimens.header,
        color: 'white',
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
    },
    sublabel: {
        color: colors.header_sub,
        // position: 'absolute',
        // bottom: 0,
        paddingLeft: width / 10,
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
        fontStyle: 'italic',
        fontSize: 10
    },
    exitBtn: {
        position: 'absolute',
        right: 20,
        width: width / 15,
        height: width / 15,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    exitStyle: {
        resizeMode: 'contain',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 15,
        height: width / 15,
        tintColor: 'white'
    },
    tabbar: {
        // flex: 1,
        marginLeft: width / 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: width / 10,
        paddingBottom: 3,
        marginTop: height / 50,
        // height: 50
        // alignSelf: '',
        width: width / 10 * 11
    },
    tabItem: {
        fontSize: 16,
        color: 'white',
        // width: width / 5,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        fontFamily: 'Roboto-Medium'
        // height: 50
    },
    tabCell: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center'
    }
};

SubShop_Header.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};