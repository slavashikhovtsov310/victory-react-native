import React from "react";
import { Content, Text, View, } from "native-base";
import { Alert, Image, Dimensions, TouchableOpacity, TouchableHighlight } from "react-native";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import colors from "../resources/colors";
import dimens from "../resources/dimens";
import { connect } from "react-redux";
import * as rootActions from "../actions/root-actions";
import * as scoreDetailActions from "../actions/scoreDetail-actions";
import yellowCard_icon from '../assets/icons/yellowCard.png'
import redCard_icon from '../assets/icons/redCard.png'
import shot_icon from '../assets/icons/shot.png'
import null_logo from '../assets/teamlogo_null.png';
import consts from "../const";
import axios from 'axios'
import strings from '../resources/strings';

console.ignoredYellowBox = ['Warning:'];
console.disableYellowBox = true
const CacelToken = axios.CancelToken;
let cancel;
const { width, height } = Dimensions.get('window');
export class Stat_AllScores extends ImmutableComponent {
    leagues =
        [
            ["World Cup", "732"],
            ["UAE - UAE League", "959"],
            ["UAE - Division 1", "962"],
            ["UAE - Arabian Gulf Cup", "965"],
            ["KSA - Pro League", "944"],
            ["KSA - Division 1", "947"],
            ["KSA - Kings Cup", "950"],
            ["ESP - La Liga", "564"],
            ["ITA - Serie A", "384"],
            ["FRA - Ligue 1", "301"],
            ["GBR - Premier League", "8"]
        ];
    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            }),
            leagues: [],
            showMore: [],
        };
    }
    componentWillUnmount() {
        if (cancel != undefined)
            cancel();
    }
    // abortController = new window.AbortController();
    componentDidMount() {
        let dataLeague = [];
        let showmoreTemp = [];
        for (var i = 0; i < this.leagues.length; i++) {
            dataLeague.push({});
            showmoreTemp.push([]);
        }
        this.setState({ leagues: dataLeague, showMore: showmoreTemp });
        for (var i = 0; i < this.leagues.length; i++) {
            this.get_Stats_Fixtures_ByLeagues(i);
        }
    }

    get_Stats_Fixtures_ByLeagues(leagueIndex) {
        const query = `
                    query myquery {
                        getLeague(leagueId: ${this.leagues[leagueIndex][1]}) {
                            name
                            seasonsInclude(orderBy: "Id", limit: 3) {
                                seasons {
                                    id
                                    name
                                    fixturesInclude(orderBy: "-StartingAt", limit: 3) {
                                        id
                                        localTeamId
                                        visitorTeamId
                                        time {
                                            startingAt {
                                                timestamp
                                            }
                                            status
                                        }
                                        scores {
                                            localTeamScore
                                            visitorTeamScore
                                        }
                                        localTeamInclude {
                                            localTeam {
                                                name
                                                logoPath
                                            }
                                        }
                                        visitorTeamInclude {
                                            visitorTeam {
                                                name
                                                logoPath
                                            }
                                        }
                                        eventsInclude {
                                            events {
                                                teamId
                                                type
                                                playerName
                                                minute
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                let data = [];
                data = this.state.leagues;
                data[leagueIndex] = res.data.data.getLeague;
                let flagArray = [];
                for (var i = 0; i < res.data.data.getLeague.seasonsInclude.seasons.length; i++) {
                    flagArray.push(false);
                }
                let tempFlagArray = [];
                tempFlagArray = this.state.showMore;
                tempFlagArray[leagueIndex] = flagArray;
                this.setState({ leagues: data, showMore: tempFlagArray });
                this.forceUpdate();
            });
        } catch (error) {
            return null;
        }
    }



    getFixturesByLeague() {
        if (this.leagues == null)
            return;
        return this.leagues.map((league, leagueIndex) => {
            let data = {};
            data = this.state.leagues[leagueIndex];
            if (data == null)
                return;
            return data.seasonsInclude == null ? null : (
                <View key={leagueIndex}>
                    <View style={Styles.subdesc}>
                        <Text style={Styles.sep}>{data.name}</Text>
                    </View>
                    {this.getSeasons(data.seasonsInclude.seasons, leagueIndex)}
                </View>
            )
        });
    }
    getSeasons = (data, leagueIndex) => {
        return data.map((season, seasonIndex) => {
            return (
                <View key={seasonIndex}>
                    <Text
                        style={
                            {
                                fontFamily: 'Roboto-Regular',
                                textAlign: 'left',
                                alignSelf: 'stretch',
                                paddingLeft: width / 20,
                                color: colors.itemContent,
                                fontSize: dimens.itemTitle
                            }
                        }>#Season {season.name}</Text>
                    {this.getFixtures(season.fixturesInclude, leagueIndex, seasonIndex)}
                </View>
            )
        })
    }
    showmore = (leagueId, seasonId) => {
        let data = this.state.showMore;
        data[leagueId][seasonId] = true;
        this.setState({ showMore: data });
        this.forceUpdate();
    }

    getFixtures = (fixtures, leagueIndex, seasonIndex) => {
        return fixtures.map((item, index) => {
            if (!this.state.showMore[leagueIndex][seasonIndex]) {
                if (index > 2) {
                    return null;
                }
                if (index == 2) {
                    return (
                        <TouchableHighlight
                            key={index}
                            underlayColor='white'
                            style={Styles.showmoreBtn}
                            onPress={(leagueId, seasonId) => this.showmore(leagueIndex, seasonIndex)} >
                            <Text style={Styles.showmoreText}>+ show more</Text>
                        </TouchableHighlight>
                    );
                }
            }
            return (
                <TouchableHighlight
                    key={index}
                    style={Styles.matchTouch}
                    onPress={(leagueId, seasonId, matchId) => this.selectMatch(leagueIndex, seasonIndex, index)}>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={item.localTeamInclude.localTeam.logoPath == '' ? null_logo : { uri: item.localTeamInclude.localTeam.logoPath }} />
                                <Text style={[Styles.Team_name, { marginLeft: width / 60 }]}>{item.localTeamInclude.localTeam.name}</Text>
                            </View>
                            {this.getLocalTeamEvents(item)}
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>{item.scores.localTeamScore} : {item.scores.visitorTeamScore}</Text>
                            <Text style={[Styles.vs, { marginTop: 5 }]}>{this.getCurrentTime(item.time.status, item.time.startingAt.timestamp)}</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 60, textAlign: 'right' }]}>{item.visitorTeamInclude.visitorTeam.name}</Text>
                                <Image style={Styles.Team_Mark} source={item.visitorTeamInclude.visitorTeam.logoPath == '' ? null_logo : { uri: item.visitorTeamInclude.visitorTeam.logoPath }} />
                            </View>
                            {this.getVisitorTeamEvents(item)}
                        </View>
                    </View>
                </TouchableHighlight>
            );
        }
        );
    }

    selectMatch = (leagueId, seasonId, matchId) => {
        var currentFixture = this.state.leagues[leagueId].seasonsInclude.seasons[seasonId].fixturesInclude[matchId];
        if (currentFixture.eventsInclude.events.length == 0) {
            Alert.alert('Info', 'There is no data for match detail');
        }
        else {
            var seasonIndex = this.state.leagues[leagueId].seasonsInclude.seasons[seasonId].id;
            var matchIndex = currentFixture.id;
            var leagueName = this.state.leagues[leagueId].name;
            var seasonName = this.state.leagues[leagueId].seasonsInclude.seasons[seasonId].name;
            let localTeamEvents = [];
            let visitorTeamEvents = [];
            let events = [];
            events = currentFixture.eventsInclude.events;
            var localCnt = 0;
            var visitorCnt = 0;
            var localTeamId = currentFixture.localTeamId;
            var visitorTeamId = currentFixture.visitorTeamId;
            for (var i = 0; i < events.length; i++) {
                if (localCnt >= 2 && visitorCnt >= 2)
                    break;
                if (localCnt < 2 &&
                    (events[i].type == 'goal' || events[i].type == 'yellowcard' || events[i].type == 'redcard') &&
                    events[i].teamId == localTeamId) {
                    localTeamEvents.push(events[i]);
                    localCnt++;
                }
                if (visitorCnt < 2 &&
                    (events[i].type == 'goal' || events[i].type == 'yellowcard' || events[i].type == 'redcard') &&
                    events[i].teamId == visitorTeamId) {
                    visitorTeamEvents.push(events[i]);
                    visitorCnt++;
                }
            }
            var localTeamScore = currentFixture.scores.localTeamScore;
            var visitorTeamScore = currentFixture.scores.visitorTeamScore;
            var localTeam = currentFixture.localTeamInclude.localTeam;
            var visitorTeam = currentFixture.visitorTeamInclude.visitorTeam;
            var currentTime = this.getCurrentTime(currentFixture.time.status, currentFixture.time.startingAt.timestamp);
            this.props.dispatch(scoreDetailActions.setIds(seasonIndex, matchIndex));
            this.props.dispatch(scoreDetailActions.setScoreDetailHeader(localTeam, localTeamEvents, localTeamScore, currentTime, visitorTeam, visitorTeamEvents, visitorTeamScore));
            this.props.dispatch(scoreDetailActions.setScoreDetailLeagueAndSeason(leagueName, seasonName));
            this.props.navigation.navigate(consts.SCOREDETAIL_SCREEN);
        }
    }
    getCurrentTime = (status, timestamp) => {
        if (status == 'FT')
            return null;
        if (status == 'NS')
            return null;
        if (status == 'AET')
            return null;
        var min = Math.floor((Date.now() / 1000 - timestamp) / 60);
        return min;
    }
    getEventImg = (type) => {
        switch (type) {
            case 'goal':
                return shot_icon;
                break;
            case 'yellowcard':
                return yellowCard_icon;
                break;
            case 'redcard':
                return redCard_icon;
                break;
            default:
                return shot_icon;
        }
    }
    getLocalTeamEvents = (item) => {
        var teamId = item.localTeamId;
        var status = item.time.status;
        let events = [];
        var count = 0;
        events = item.eventsInclude.events;
        return events.map((event, index) => {
            if (count >= 2) {
                return null;
            }
            if (event.type == 'goal' || event.type == 'yellowcard' || event.type == 'redcard' && event.teamId == teamId) {
                count++;
                return (
                    <View
                        key={index}
                        style={Styles.localTeamEventRow}>
                        <Image
                            style={Styles.localTeamEventImg}
                            source={this.getEventImg(event.type)} />
                        <Text style={Styles.localTeamEventMinutes}>{event.minute}</Text>
                        <Text style={Styles.localTeamEventPlayer}>{event.playerName}</Text>
                    </View>
                );
            }
        });
    }
    getVisitorTeamEvents = (item) => {
        var teamId = item.visitorTeamId;
        var status = item.time.status;
        let events = [];
        var count = 0;
        events = item.eventsInclude.events;
        return events.map((event, index) => {
            if (count >= 2) {
                return null;
            }
            if ((event.type == 'goal' || event.type == 'yellowcard' || event.type == 'redcard') && event.teamId == teamId) {
                count++;
                return (
                    <View
                        key={index}
                        style={Styles.visitorTeamEventRow}>
                        <Text style={Styles.visitorTeamEventPlayer}>{event.playerName}</Text>
                        <Text style={Styles.visitorTeamEventMinutes}>{event.minute}</Text>
                        <Image
                            style={Styles.visitorTeamEventImg}
                            source={this.getEventImg(event.type)} />
                    </View>
                );
            }
        });
    }
    render() {
        return (
            <View style={{ width: width, height: height / 10 * 7.6 }}>
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.desc}>
                        <Text style={{ fontSize: dimens.itemTitle, color: colors.itemContent }}>{strings.all_leagues}</Text>
                    </View>
                    {this.getFixturesByLeague()}
                </Content >
                <TouchableOpacity style={Styles.todayBtn}>
                    <Text style={Styles.todayBtnText}>Today</Text>
                </TouchableOpacity>
                <View style={Styles.dateBar}>
                    <TouchableHighlight style={Styles.dateCell}>
                        <View style={Styles.dateCellView}>
                            <Text style={[Styles.day, { color: '#3cce81' }]}>08</Text>
                            <Text style={[Styles.month, , { color: '#3cce81' }]}>Oct</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight style={Styles.dateCell}>
                        <View style={Styles.dateCellView}>
                            <Text style={Styles.day}>09</Text>
                            <Text style={Styles.month}>Oct</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight style={Styles.dateCell}>
                        <View style={Styles.dateCellView}>
                            <Text style={Styles.day}>10</Text>
                            <Text style={Styles.month}>Oct</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight style={Styles.dateCell}>
                        <View style={Styles.dateCellView}>
                            <Text style={Styles.day}>11</Text>
                            <Text style={Styles.month}>Oct</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight style={Styles.dateCell}>
                        <View style={Styles.dateCellView}>
                            <Text style={Styles.day}>12</Text>
                            <Text style={Styles.month}>Oct</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight style={Styles.dateCell}>
                        <View style={Styles.dateCellView}>
                            <Text style={Styles.day}>13</Text>
                            <Text style={Styles.month}>Oct</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight style={Styles.dateCell}>
                        <View style={Styles.dateCellView}>
                            <Text style={Styles.day}>14</Text>
                            <Text style={Styles.month}>Oct</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

const Styles = {
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
        paddingBottom: height / 10
    },
    desc: {
        width: width,
        paddingLeft: width / 20,
        padding: width / 30,
    },
    subdesc: {
        width: width,
        paddingLeft: width / 20,
        backgroundColor: '#eaeaea',
        padding: width / 30
    },
    match: {
        flexDirection: 'row',
        width: width,
        paddingLeft: width / 20,
        paddingRight: width / 9,
        justifyContent: 'space-between',
        paddingTop: width / 30,
        paddingBottom: width / 30,
        borderBottomWidth: 1,
        backgroundColor: 'white',
        borderBottomColor: '#aaaaaa',
    },
    A_Team: {
        width: width / 3
    },
    A_Team_name_group: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width / 3,
        paddingTop: 5,
        paddingBottom: 5
    },
    B_Team: {
        width: width / 3
    },
    B_Team_name_group: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: width / 3,
        paddingTop: 5,
        paddingBottom: 5
    },
    Team_Mark: {
        width: width / 30 * 2,
        height: width / 30 * 2,
        resizeMode: 'contain'
    },
    Team_name: {
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.small + 1,
        color: colors.itemContent
    },
    shot_Mark: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    card_Mark: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    match_result: {
        alignItems: 'center',
        paddingTop: 5,
        // justifyContent: 'center'
    },
    vs: {
        fontFamily: 'Roboto-Medium',
        fontSize: dimens.small + 1,
        color: colors.itemContent,
        textAlign: 'center',
        // fontWeight: 'bold',
    },
    sep: {
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.itemTitle,
        color: colors.itemContent
    },
    todayBtn: {
        position: 'absolute',
        bottom: height / 30,
        backgroundColor: '#3cce81',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        width: width / 3,
        paddingTop: 5,
        paddingBottom: 5,
    },
    todayBtnText: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: 'white'
    },
    dateBar: {
        position: 'absolute',
        right: width / 100,
        top: 10,
        height: height / 10 * 7,
        backgroundColor: '#33333320',
        borderRadius: 15
    },
    dateCellView: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 5,
        paddingRight: 5
    },
    dateCell: {
        height: height / 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    day: {
        fontFamily: 'Roboto-Regular',
        fontSize: 11,
        color: colors.itemContent
    },
    month: {
        fontFamily: 'Roboto-Regular',
        fontSize: 9,
        color: colors.itemContent
    },
    showmoreText: {
        fontFamily: 'Roboto-Medium',
        fontSize: dimens.itemTitle - 1,
        color: colors.itemContent
    },
    showmoreBtn: {
        paddingBottom: 8,
        paddingTop: 8,
        paddingLeft: width / 8,
        backgroundColor: 'white'
    },
    localTeamEventRow: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: width / 60,
        marginTop: 3,
    },
    localTeamEventImg: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    localTeamEventMinutes: {
        // width: width / 20,
        marginLeft: width / 40,
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.small,
        color: colors.subItemContent
    },
    localTeamEventPlayer: {
        marginLeft: width / 50,
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.small,
        color: colors.subItemContent
    },
    visitorTeamEventRow: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingRight: width / 60,
        marginTop: 3,
    },
    visitorTeamEventImg: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    visitorTeamEventMinutes: {
        // width: width / 20,
        marginRight: width / 40,
        fontFamily: 'Roboto-Regular',
        textAlign: 'right',
        fontSize: dimens.small,
        color: colors.subItemContent
    },
    visitorTeamEventPlayer: {
        marginRight: width / 50,
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.small,
        color: colors.subItemContent,
        textAlign: 'right'
    }
};

Stat_AllScores.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};

const mapStateToProps = (state) => ({
    Stat: state.get('stat'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Stat_AllScores)