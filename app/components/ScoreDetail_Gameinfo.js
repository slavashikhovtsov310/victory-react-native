import React, { Component } from 'react';
import { Content, Input, Item, Label, Text, Left, View } from 'native-base';
import {
  Alert,
  Image,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import strings from '../resources/strings';
import PropTypes from 'prop-types';
import * as Immutable from '../../node_modules/immutable/dist/immutable';
import ImmutableComponent from './ImmutableComponent';
import opt_icon from '../assets/icons/opt.png';
import star_icon from '../assets/icons/star.png';
import colors from '../resources/colors';
import dimens from '../resources/dimens';
import filter_icon from '../assets/icons/filter.png';
import rm_icon from '../assets/icons/rm.png';
import fc_icon from '../assets/icons/fc.png';
import yellowCard_icon from '../assets/icons/yellowCard.png';
import redCard_icon from '../assets/icons/redCard.png';
import panalty_icon from '../assets/icons/panalty.png';
import missedpanalty_icon from '../assets/icons/missedpanalty.png';
import substitution_icon from '../assets/icons/substitution.png';
import shot_icon from '../assets/icons/shot.png';
import sample from '../assets/sample.png';
import null_logo from '../assets/teamlogo_null.png';
import { connect } from "react-redux";
import LinearGradient from 'react-native-linear-gradient';
import playermark1_icon from '../assets/icons/table_item1.png';
import playermark2_icon from '../assets/icons/table_item2.png';
import playermark3_icon from '../assets/icons/table_item3.png';
import playermark4_icon from '../assets/icons/table_item4.png';
import playermark5_icon from '../assets/icons/table_item5.png';
import playermark6_icon from '../assets/icons/table_item6.png';
import playermark7_icon from '../assets/icons/table_item7.png';
import playermark8_icon from '../assets/icons/table_item8.png';

const { width, height } = Dimensions.get('window');

export class ScoreDetail_Gameinfo extends ImmutableComponent {
  constructor(props: {}) {
    super(props);

    this.state = {
      data: Immutable.Map({
        error: '',
        showDefaultValue: true,
      }),
    };
  }

  render() {
    return (
      <Content contentContainerStyle={Styles.contentStyle}>
        <View style={Styles.postPan}>
          <Text style={Styles.season}>#Season {this.props.scoreDetail.get('seasonName')}</Text>
          <View style={Styles.groupRow}>
            <View style={Styles.TeamCol}>
              <Image style={Styles.teamMark}
                source={this.props.scoreDetail.get('localTeam').logoPath != '' ? { uri: this.props.scoreDetail.get('localTeam').logoPath } : null_logo} />
              <Text style={Styles.teamName}>{this.props.scoreDetail.get('localTeam').name}</Text>
              <Text style={Styles.detail}>1 yellow card</Text>
              <Text style={Styles.detail}>1 Goal</Text>
              <Text style={Styles.detail}>1 Substitutions</Text>
            </View>
            <View style={Styles.timeline}>
              <Text style={Styles.leagueName}>{this.props.scoreDetail.get('leagueName')}</Text>
              <Text style={Styles.vs}>KO</Text>
              <View style={Styles.itemGroup}>
                <View style={Styles.sep} />
                <View style={Styles.itemRow}>
                  <View style={[Styles.side, Styles.left]}>
                    <Image style={Styles.sideImg} source={yellowCard_icon} />
                    <View style={Styles.horiSep} />
                  </View>
                  <Text style={Styles.center}>2'</Text>
                </View>
                <View style={Styles.sep} />
                <View style={Styles.itemRow}>
                  <Text style={Styles.center}>20'</Text>
                  <View style={[Styles.side, Styles.right]}>
                    <View style={Styles.horiSep} />
                    <Image style={Styles.sideImg} source={yellowCard_icon} />
                  </View>
                </View>
                <View style={Styles.sep} />
                <View style={Styles.itemRow}>
                  <View style={[Styles.side, Styles.left]}>
                    <Image style={Styles.sideImg} source={shot_icon} />
                    <View style={Styles.horiSep} />
                  </View>
                  <Text style={Styles.center}>29'</Text>
                </View>
                <View style={Styles.sep} />
                <View style={Styles.itemRow}>
                  <View style={[Styles.side, Styles.left]}>
                    <Image style={Styles.sideImg} source={substitution_icon} />
                    <View style={Styles.horiSep} />
                  </View>
                  <Text style={Styles.center}>32'</Text>
                </View>
                <View style={Styles.sep} />
                <View style={Styles.itemRow}>
                  <Text style={Styles.center}>42'</Text>
                  <View style={[Styles.side, Styles.right]}>
                    <View style={Styles.horiSep} />
                    <Image style={Styles.sideImg} source={panalty_icon} />
                  </View>
                </View>
                <View style={Styles.sep} />
                <View style={Styles.itemRow}>
                  <Text style={Styles.center}>43'</Text>
                  <View style={[Styles.side, Styles.right]}>
                    <View style={Styles.horiSep} />
                    <Image style={Styles.sideImg} source={missedpanalty_icon} />
                  </View>
                </View>
              </View>
            </View>
            <View style={Styles.TeamCol}>
              <Image style={[Styles.teamMark, { alignSelf: 'flex-end' }]}
                source={this.props.scoreDetail.get('visitorTeam').logoPath != '' ? { uri: this.props.scoreDetail.get('visitorTeam').logoPath } : null_logo} />
              <Text style={[Styles.teamName, { textAlign: 'right' }]}>{this.props.scoreDetail.get('visitorTeam').name}</Text>
              <Text style={[Styles.detail, { textAlign: 'right' }]}>
                yellow card 1
              </Text>
              <Text style={[Styles.detail, { textAlign: 'right' }]}>Goal 0</Text>
              <Text style={[Styles.detail, { textAlign: 'right' }]}>
                Missed penailty 1
              </Text>
            </View>
          </View>
        </View>
        <View style={Styles.subdesc}>
          <Text style={Styles.sepText}>{strings.gameinfo}</Text>
        </View>
        <View style={Styles.infodesc}>
          <Text style={Styles.field}>{strings.field}</Text>
          <Text style={Styles.fieldData}>Camp Nou Stadium</Text>
          <Text style={Styles.field}>{strings.capacity}</Text>
          <Text style={Styles.fieldData}>99.354</Text>
        </View>
        <View style={Styles.subdesc}>
          <Text style={Styles.sepText}>{strings.previous_games_played}</Text>
        </View>
        <View style={Styles.desc}>
          <View style={Styles.row}>
            <Text style={Styles.name}>Athletic Bilbao</Text>
            <Text style={[Styles.name, { textAlign: 'right' }]}>Barcelona</Text>
          </View>
          <View style={Styles.row}>
            <View style={Styles.teamStrategy}>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={Styles.gradientCircle}>
                <Text>W</Text>
              </LinearGradient>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={Styles.gradientCircle}>
                <Text>L</Text>
              </LinearGradient>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={Styles.gradientCircle}>
                <Text>L</Text>
              </LinearGradient>
            </View>
            <View style={Styles.teamStrategy}>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={Styles.gradientCircle}>
                <Text>W</Text>
              </LinearGradient>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={Styles.gradientCircle}>
                <Text>W</Text>
              </LinearGradient>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#81d7af', '#8091da']}
                style={Styles.gradientCircle}>
                <Text>L</Text>
              </LinearGradient>
            </View>
          </View>
        </View>

        <View style={Styles.table}>
          <View style={Styles.tableHdr}>
            <Text style={Styles.tableName}>{strings.standing}</Text>
            <Text style={Styles.tableCell}>P</Text>
            <Text style={Styles.tableCell}>W</Text>
            <Text style={Styles.tableCell}>L</Text>
            <Text style={Styles.tableCell}>Pts</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>1</Text>
            <View style={Styles.player}>
              <View style={Styles.circle} />
              <Image style={Styles.playerMarkImg} source={playermark1_icon} />
              <Text style={Styles.playerName}>Al Nasr</Text>
            </View>
            <Text style={Styles.tableCell}>1</Text>
            <Text style={Styles.tableCell}>1</Text>
            <Text style={Styles.tableCell}>1</Text>
            <Text style={Styles.tableCell}>1</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>2</Text>
            <View style={Styles.player}>
              <View style={Styles.circle} />
              <Image style={Styles.playerMarkImg} source={playermark2_icon} />
              <Text style={Styles.playerName}>Al Ain</Text>
            </View>
            <Text style={Styles.tableCell}>2</Text>
            <Text style={Styles.tableCell}>2</Text>
            <Text style={Styles.tableCell}>2</Text>
            <Text style={Styles.tableCell}>2</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>3</Text>
            <View style={Styles.player}>
              <View style={Styles.triangle_up} />
              <Image style={Styles.playerMarkImg} source={playermark3_icon} />
              <Text style={Styles.playerName}>Al Wahda</Text>
            </View>
            <Text style={Styles.tableCell}>3</Text>
            <Text style={Styles.tableCell}>3</Text>
            <Text style={Styles.tableCell}>3</Text>
            <Text style={Styles.tableCell}>3</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>4</Text>
            <View style={Styles.player}>
              <View style={Styles.triangle_down} />
              <Image style={Styles.playerMarkImg} source={playermark4_icon} />
              <Text style={Styles.playerName}>Al Shabab</Text>
            </View>
            <Text style={Styles.tableCell}>4</Text>
            <Text style={Styles.tableCell}>4</Text>
            <Text style={Styles.tableCell}>4</Text>
            <Text style={Styles.tableCell}>4</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>5</Text>
            <View style={Styles.player}>
              <View style={Styles.triangle_up} />
              <Image style={Styles.playerMarkImg} source={playermark5_icon} />
              <Text style={Styles.playerName}>Dibba</Text>
            </View>
            <Text style={Styles.tableCell}>5</Text>
            <Text style={Styles.tableCell}>5</Text>
            <Text style={Styles.tableCell}>5</Text>
            <Text style={Styles.tableCell}>5</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>6</Text>
            <View style={Styles.player}>
              <View style={Styles.triangle_down} />
              <Image style={Styles.playerMarkImg} source={playermark6_icon} />
              <Text style={Styles.playerName}>Al Wasl</Text>
            </View>
            <Text style={Styles.tableCell}>6</Text>
            <Text style={Styles.tableCell}>6</Text>
            <Text style={Styles.tableCell}>6</Text>
            <Text style={Styles.tableCell}>6</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>7</Text>
            <View style={Styles.player}>
              <View style={Styles.rectangle} />
              <Image style={Styles.playerMarkImg} source={playermark7_icon} />
              <Text style={Styles.playerName}>Al Jazira</Text>
            </View>
            <Text style={Styles.tableCell}>7</Text>
            <Text style={Styles.tableCell}>7</Text>
            <Text style={Styles.tableCell}>7</Text>
            <Text style={Styles.tableCell}>7</Text>
          </View>
          <View style={Styles.tableRow}>
            <Text style={Styles.num}>8</Text>
            <View style={Styles.player}>
              <View style={Styles.rectangle} />
              <Image style={Styles.playerMarkImg} source={playermark8_icon} />
              <Text style={Styles.playerName}>Al Etehad</Text>
            </View>
            <Text style={Styles.tableCell}>8</Text>
            <Text style={Styles.tableCell}>8</Text>
            <Text style={Styles.tableCell}>8</Text>
            <Text style={Styles.tableCell}>8</Text>
          </View>
        </View>
      </Content>
    );
  }
}

const Styles = {
  contentStyle: {
    alignItems: 'center',
    alignSelf: 'center',
    paddingTop: width / 30,
  },
  postPan: {
    padding: width / 50,
    width: width / 10 * 9,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  groupRow: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    paddingLeft: 3,
    paddingRight: 3,
  },
  team: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  TeamCol: {
    width: width / 16 * 4,
  },
  timeline: {
    width: width / 15 * 5,
    // justifyContent: 'center',
    alignItems: 'center',
  },
  leagueName: {
    fontSize: dimens.item,
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent
  },
  teamMark: {
    width: width / 10,
    height: width / 10,
    marginLeft: 5,
    marginRight: 5,
    resizeMode: 'contain',
  },
  teamName: {
    fontSize: dimens.item,
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent
  },
  detail: {
    fontSize: dimens.small,
    textAlign: 'left',
    fontFamily: 'Roboto-Regular',
    color: colors.subItemContent3
  },
  season: {
    marginBottom: width / 30,
    fontFamily: 'Roboto-Regular',
    fontSize: dimens.h2,
    color: colors.itemContent
  },
  vs: {
    marginTop: width / 20,
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent,
    fontSize: dimens.item
  },
  center: {
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent,
    fontSize: dimens.item
  },
  sep: {
    height: width / 20,
    width: 1,
    backgroundColor: '#aaaaaa',
    alignSelf: 'center',
  },
  itemRow: {
    justifyContent: 'center',
  },
  side: {
    flexDirection: 'row',
    position: 'absolute',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  horiSep: {
    width: width / 20,
    height: 1,
    backgroundColor: '#aaaaaa',
  },
  sideImg: {
    width: width / 30,
    resizeMode: 'contain',
  },
  left: {
    left: -width / 12,
  },
  right: {
    right: -width / 12,
  },
  subdesc: {
    marginTop: width / 20,
    width: width,
    paddingLeft: width / 20,
    backgroundColor: colors.itemNameBack,
    padding: 3,
  },
  sepText: {
    fontSize: dimens.itemTitle,
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent
  },
  infodesc: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width,
    paddingLeft: width / 20,
    paddingRight: width / 20,
    padding: width / 30,
    backgroundColor: 'white',
  },
  desc: {
    width: width,
    paddingLeft: width / 20,
    paddingRight: width / 20,
    padding: width / 30,
    backgroundColor: 'white',
  },
  field: {
    color: colors.itemContent,
    fontFamily: 'Roboto-Regular',
    fontSize: dimens.item
  },
  fieldData: {
    color: colors.subItemContent3,
    fontFamily: 'Roboto-Regular',
    fontSize: dimens.item
  },
  row: {
    flexDirection: 'row',
    width: width / 10 * 9,
    paddingLeft: width / 20,
    paddingRight: width / 20,
    justifyContent: 'space-between',
  },
  name: {
    width: width / 5 * 2,
    fontFamily: 'Roboto-Regular',
    fontSize: dimens.item,
    color: colors.subItemContent3
  },
  gradientCircle: {
    width: width / 10,
    height: width / 10,
    borderRadius: width / 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  teamStrategy: {
    width: width / 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tableHdr: {
    width: width,
    flexDirection: 'row',
    padding: 3,
    backgroundColor: colors.itemNameBack,
    paddingLeft: width / 20,
    alignItems: 'center'
  },
  tableName: {
    width: width / 2,
    fontFamily: 'Roboto-Regular',
    fontSize: dimens.item,
    color: colors.itemContent    
  },
  tableCell: {
    width: width / 10,
    fontSize: dimens.small,
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent    
  },
  topScoresName: {
    width: width / 5 * 3,
    fontSize: dimens.item,
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent   
  },
  topScoresCell: {
    width: width / 20 * 3,
    fontSize: dimens.small,
    textAlign: 'center',
    fontFamily: 'Roboto-Regular',
    color: colors.itemContent   
  },
  tableRow: {
    flexDirection: 'row',
    marginLeft: width / 20,
    width: width / 20 * 19,
    borderBottomColor: '#aaaaaa',
    borderBottomWidth: 1,
    alignItems: 'center',
    padding: 5,
  },
  num: {
    width: width / 10,
    fontSize: dimens.small,
  },
  player: {
    flexDirection: 'row',
    width: width / 10 * 4,
    alignItems: 'center',
  },
  topScoresplayer: {
    flexDirection: 'row',
    width: width / 30 * 18 - width / 50,
    alignItems: 'center',
  },
  playerMarkImg: {
    marginLeft: width / 30,
    height: width / 20,
    width: width / 20,
    resizeMode: 'contain',
    marginRight: width / 30,
  },
  playerName: {
    fontFamily: 'Roboto-Regular',
    fontSize: dimens.item,
    color: colors.itemContent
  },
  topScoresplayerMarkImg: {
    height: width / 20,
    width: width / 20,
    resizeMode: 'contain',
    marginRight: width / 30,
  },
  circle: {
    height: width / 50,
    width: width / 50,
    borderRadius: width / 100,
    backgroundColor: 'red',
  },
  triangle_up: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: width / 100,
    borderRightWidth: width / 100,
    borderBottomWidth: width / 50,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'red',
  },
  triangle_down: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: width / 100,
    borderRightWidth: width / 100,
    borderBottomWidth: width / 50,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'red',
    transform: [{ rotate: '180deg' }],
  },
  rectangle: {
    width: width / 50,
    height: width / 50,
    backgroundColor: 'red',
  },
  table: {
    marginTop: width / 20,
  },
};

ScoreDetail_Gameinfo.propTypes = {
  defaultValue: PropTypes.string,
};


const mapStateToProps = (state) => ({
  root: state.get('root'),
  scoreDetail: state.get('scoreDetail')
});

export default connect(mapStateToProps)(ScoreDetail_Gameinfo)
