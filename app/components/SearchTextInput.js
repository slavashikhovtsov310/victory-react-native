import React, { Component } from "react";
import { Content, Input, Item, Label, Text, Left, View } from "native-base";
import { Alert, Image, Dimensions, TextInput } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import search_icon from '../assets/icons/search.png'
import dimens from "../resources/dimens";
import colors from "../resources/colors";



const { width, height } = Dimensions.get('window');


export default class SearchTextInput extends ImmutableComponent {

    constructor(props: {}) {
        super(props);
        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            })
        };
    }

    render() {
        return (
            <View
                elevation={5}
                shouldRasterizeIOS
                renderToHardwareTextureAndroid
                style={styles.containerStyle} scrollEnabled={false}>

                <Image source={search_icon}
                    style={styles.iconstyle} />
                <Input
                    style={styles.inputStyle}
                    onChangeText={this.handleTextChange}
                    secureTextEntry={this.props.secureTextEntry}
                    placeholder={this.props.placeholder}
                    placeholderTextColor={colors.subItemContent}
                />
            </View>
        );
    }
    alertShow(text) {
    }
    handleTextChange = (text: string) => {
        if (this.props.onChangeText) {
            this.props.onChangeText(text);
        }
    };
}

const styles = {
    containerStyle: {
        flexDirection: 'row',
        shadowColor: colors.itemContent,
        shadowOffset: { width: 1, height: 1 },
        shadowRadius: 1,
        shadowOpacity: 1,
        borderRadius: 7,
        height: height * 1 / 20,
        paddingTop: 0,
        paddingBottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginTop: height / 100 * 3,
        marginLeft: width / 20 * 1.5,
        marginRight: width / 20 * 1.5,
        marginBottom: height / 50,
        backgroundColor: 'white',
    },
    errorTextStyle: {
        color: 'red'
    },
    itemStyle: {
        flex: 0,
        marginTop: 16
    },
    inputStyle: {
        marginLeft: width / 20,
        color: colors.itemContent,
        paddingBottom: 0,
        paddingTop: 0,
        marginTop: 0,
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 0,
        fontFamily: 'OpenSans',
        fontStyle: 'italic',
        fontSize: dimens.itemContent
    },
    iconstyle: {
        width: width / 22.58,
        height: width / 22.58,
        marginLeft: width / 15,
        resizeMode: 'contain'
    }
};

SearchTextInput.propTypes = {
    defaultValue: PropTypes.string
};