import React from "react";
import { Content, Text, View, } from "native-base";
import { Image, Dimensions } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import colors from "../resources/colors";
import dimens from "../resources/dimens";
import { connect } from "react-redux";
import null_logo from '../assets/teamlogo_null.png';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
const { width, height } = Dimensions.get('window');
const persent = 80;
export class ScoreDetail_Gamestats extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            }),
            progress: 0
        };
    }
    componentDidMount() {
        this.animate();
    }

    animate() {
        let progress = 0;
        this.setState({ progress: progress });
        setTimeout(() => {
            this.setState({ indeterminate: false });
            setInterval(() => {
                progress += 1;
                if (progress > persent) {
                    progress = persent
                }
                this.setState({ progress: progress });
            }, 50);
        }, 1500);
    }
    render() {
        return (
            <Content contentContainerStyle={Styles.contentStyle}>
                <View style={Styles.postPan}>
                    <View style={Styles.teamRow}>
                        <View style={Styles.team}>
                            <Image style={Styles.teamMark}
                                source={this.props.scoreDetail.get('localTeam').logoPath != '' ? { uri: this.props.scoreDetail.get('localTeam').logoPath } : null_logo} />
                            <Text style={Styles.teamName}>{this.props.scoreDetail.get('localTeam').name}</Text>
                        </View>
                        <View style={Styles.team}>
                            <Image style={Styles.teamMark}
                                source={this.props.scoreDetail.get('visitorTeam').logoPath != '' ? { uri: this.props.scoreDetail.get('visitorTeam').logoPath } : null_logo} />
                            <Text style={Styles.teamName}>{this.props.scoreDetail.get('visitorTeam').name}</Text>
                        </View>
                    </View>
                    <View style={Styles.percentRow}>
                        <Text style={Styles.percent}>20%</Text>
                        <View
                            style={Styles.positionGroup}>
                            <AnimatedCircularProgress
                                size={width / 5}
                                width={3}
                                fill={80}
                                tintColor="#8091DA"
                                onAnimationComplete={() => console.log('onAnimationComplete')}
                                backgroundColor="#80FFA0"
                                style={{
                                    transform: [
                                        { rotate: '270deg' }
                                    ],
                                    position: 'absolute'
                                }} />
                            <Text style={Styles.positionText}>{strings.position}</Text>
                        </View>
                        <Text style={[Styles.percent, { textAlign: 'right' }]}>80%</Text>
                    </View>
                    <View style={Styles.itemPercentRow}>
                        <Text style={Styles.itemPercentText}>80%</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9def4', '#d9eeda']}
                            style={[Styles.itemPercentBackBar, { justifyContent: 'center' }]} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#8091da', '#81d8ae']}
                                style={[Styles.itemPercentBar, { position: 'absolute', right: 0 }, { width: width / 360 * 75 * 80 / 100 }]} />
                        </LinearGradient>
                        <Text style={Styles.itemText}>{strings.fouls}</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.itemPercentBackBar} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#81d8ae', '#8091da']}
                                style={[Styles.itemPercentBar, { width: width / 360 * 75 * 20 / 100 }]} />
                        </LinearGradient>
                        <Text style={[Styles.itemPercentText, { textAlign: 'right' }]}>20%</Text>
                    </View>
                    <View style={Styles.itemPercentRow}>
                        <Text style={Styles.itemPercentText}>80%</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9def4', '#d9eeda']}
                            style={[Styles.itemPercentBackBar, { justifyContent: 'center' }]} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#8091da', '#81d8ae']}
                                style={[Styles.itemPercentBar, { position: 'absolute', right: 0 }, { width: width / 360 * 75 * 70 / 100 }]} />
                        </LinearGradient>
                        <Text style={Styles.itemText}>{strings.goal}</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.itemPercentBackBar} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#81d8ae', '#8091da']}
                                style={[Styles.itemPercentBar, { width: width / 360 * 75 * 30 / 100 }]} />
                        </LinearGradient>
                        <Text style={[Styles.itemPercentText, { textAlign: 'right' }]}>30%</Text>
                    </View>
                    <View style={Styles.itemPercentRow}>
                        <Text style={Styles.itemPercentText}>40%</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9def4', '#d9eeda']}
                            style={[Styles.itemPercentBackBar, { justifyContent: 'center' }]} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#8091da', '#81d8ae']}
                                style={[Styles.itemPercentBar, { position: 'absolute', right: 0 }, { width: width / 360 * 75 * 40 / 100 }]} />
                        </LinearGradient>
                        <Text style={Styles.itemText}>{strings.exchanges}</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.itemPercentBackBar} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#81d8ae', '#8091da']}
                                style={[Styles.itemPercentBar, { width: width / 360 * 75 * 60 / 100 }]} />
                        </LinearGradient>
                        <Text style={[Styles.itemPercentText, { textAlign: 'right' }]}>60%</Text>
                    </View>
                    <View style={Styles.itemPercentRow}>
                        <Text style={Styles.itemPercentText}>60%</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9def4', '#d9eeda']}
                            style={[Styles.itemPercentBackBar, { justifyContent: 'center' }]} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#8091da', '#81d8ae']}
                                style={[Styles.itemPercentBar, { position: 'absolute', right: 0 }, { width: width / 360 * 75 * 60 / 100 }]} />
                        </LinearGradient>
                        <Text style={Styles.itemText}>{strings.offsides}</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.itemPercentBackBar} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#81d8ae', '#8091da']}
                                style={[Styles.itemPercentBar, { width: width / 360 * 75 * 40 / 100 }]} />
                        </LinearGradient>
                        <Text style={[Styles.itemPercentText, { textAlign: 'right' }]}>40%</Text>
                    </View>
                    <View style={Styles.itemPercentRow}>
                        <Text style={Styles.itemPercentText}>80%</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9def4', '#d9eeda']}
                            style={[Styles.itemPercentBackBar, { justifyContent: 'center' }]} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#8091da', '#81d8ae']}
                                style={[Styles.itemPercentBar, { position: 'absolute', right: 0 }, { width: width / 360 * 75 * 80 / 100 }]} />
                        </LinearGradient>
                        <Text style={Styles.itemText}>{strings.cornerkicks}</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.itemPercentBackBar} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#81d8ae', '#8091da']}
                                style={[Styles.itemPercentBar, { width: width / 360 * 75 * 20 / 100 }]} />
                        </LinearGradient>
                        <Text style={[Styles.itemPercentText, { textAlign: 'right' }]}>20%</Text>
                    </View>
                    <View style={Styles.itemPercentRow}>
                        <Text style={Styles.itemPercentText}>60%</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9def4', '#d9eeda']}
                            style={[Styles.itemPercentBackBar, { justifyContent: 'center' }]} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#8091da', '#81d8ae']}
                                style={[Styles.itemPercentBar, { position: 'absolute', right: 0 }, { width: width / 360 * 75 * 60 / 100 }]} />
                        </LinearGradient>
                        <Text style={Styles.itemText}>{strings.saved}</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.itemPercentBackBar} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#81d8ae', '#8091da']}
                                style={[Styles.itemPercentBar, { width: width / 360 * 75 * 40 / 100 }]} />
                        </LinearGradient>
                        <Text style={[Styles.itemPercentText, { textAlign: 'right' }]}>40%</Text>
                    </View>
                    <View style={Styles.itemPercentRow}>
                        <Text style={Styles.itemPercentText}>80%</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9def4', '#d9eeda']}
                            style={[Styles.itemPercentBackBar, { justifyContent: 'center' }]} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#8091da', '#81d8ae']}
                                style={[Styles.itemPercentBar, { position: 'absolute', right: 0 }, { width: width / 360 * 75 * 80 / 100 }]} />
                        </LinearGradient>
                        <Text style={Styles.itemText}>{strings.ballpositions}</Text>
                        <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            colors={['#d9eeda', '#d9def4']}
                            style={Styles.itemPercentBackBar} >
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}
                                colors={['#81d8ae', '#8091da']}
                                style={[Styles.itemPercentBar, { width: width / 360 * 75 * 20 / 100 }]} />
                        </LinearGradient>
                        <Text style={[Styles.itemPercentText, { textAlign: 'right' }]}>20%</Text>
                    </View>
                </View>
            </Content >
        );
    }
}

const Styles = {
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
        paddingTop: width / 20
    },
    postPan: {
        padding: width / 30,
        width: width / 10 * 9,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    teamRow: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
    },
    team: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    teamMark: {
        height: width / 8,
        width: width / 8,
        resizeMode: 'contain'
    },
    teamName: {
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.itemTitle,
        color: colors.itemContent
    },
    positionGroup: {
        width: width / 5,
        height: width / 5,
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    positionText: {
        fontSize: dimens.small,
        fontFamily: 'Roboto-Regular',
        color: colors.itemContent
    },
    percent: {
        width: width / 7,
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.item,
        color: colors.itemContent
    },
    percentRow: {
        marginBottom: width / 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemPercentRow: {
        marginTop: 3,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemPercentText: {
        fontSize: dimens.small,
        width: width / 360 * 25,
        fontFamily: 'Roboto-Regular',
        color: colors.itemContent,
    },
    itemPercentBackBar: {
        width: width / 360 * 75,
        height: 3
    },
    itemPercentBar: {
        height: 3
    },
    itemText: {
        width: width / 360 * 90,
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        color: colors.itemContent,
        fontSize: dimens.small
    }
};

ScoreDetail_Gamestats.propTypes = {
    defaultValue: PropTypes.string,
};


const mapStateToProps = (state) => ({
    root: state.get('root'),
    scoreDetail: state.get('scoreDetail')
});

export default connect(mapStateToProps)(ScoreDetail_Gamestats)