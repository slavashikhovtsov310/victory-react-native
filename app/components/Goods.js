import React, { Component } from "react";
import { Image, StatusBar, Text, Dimensions, TouchableOpacity, ScrollView, AsyncStorage, TouchableHighlight } from "react-native";
import { Container, Content, Spinner, View, Input, Picker } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Footer from "./Footer";
import strings from "../resources/strings";
import * as wishListActions from "../actions/wishList-actions";
import * as cartActions from "../actions/cart-actions";
import * as cartWebViewActions from "../actions/cartWebView-actions";
import background from '../assets/shop_background.png';
import back_icon from '../assets/icons/back.png'
import star_icon from '../assets/icons/star.png'
import filled_star_icon from '../assets/icons/filled_star.png'
import Orientation from 'react-native-orientation';
import { Dropdown } from 'react-native-material-dropdown';
import axios from 'axios';
import * as Api from "../api";
import { constructDefaultOptions } from "apollo-fetch";

console.ignoredYellowBox = ['Warning:'];
console.disableYellowBox = true
const CacelToken = axios.CancelToken;
let cancel;
const { width, height } = Dimensions.get('window');
const domain = "https://victory.softwarehouse.io/";

export class Goods extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            productId: '',
            productDetails: [],
            products: [],
            wishlist: [],
            selected_sizes: [],
            productSizes: []
        }
    }

    componentWillUnmount() {
        if (cancel != undefined)
            cancel();
    }
    componentDidMount() {
        Orientation.lockToPortrait();
        let ps = [];
        ps.push('M');
        ps.push('XL');
        this.setState({ productSizes: ps });
        var productId = '';
        productId = this.props.subshop.get('productId');
        if (productId != undefined) {
            this.getProductDetail(productId);
        }
        this.getProducts(this.state.collectionCode, "", "");
        let wishlist = [];
        wishlist = this.props.wishList.get('list');
        this.setState({ wishlist: wishlist, productId: productId });
    }

    getProductDetail(productId) {
        this.setState({ products: [] });
        this.forceUpdate();
        const query = `
                    query productDetail {
                        getProductDetails(productId: ${productId}) {
                            name
                            description
                            price
                            gender
                            image
                            categoryId
                            kitCode
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.setState({ productDetails: res.data.data.getProductDetails });
                this.forceUpdate();
            });
        } catch (error) {
            return null;
        }
    }

    getProducts(collectionCode, kitCode, productSort) {
        this.setState({ products: [] });
        this.forceUpdate();
        const query = `
                    query products {
                        getProducts(collectionId: ${0}, collectionCode: "${collectionCode}", kitCode: "${kitCode}", productSort: "${productSort}") {
                            name
                            price
                            thumbnail
                            id
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.setState({ products: res.data.data.getProducts });
                this.forceUpdate();
            });
        } catch (error) {
            return null;
        }
    }
    componentDidUpdate() {
    }



    setSearch = (text) => {

    }
    getPickerkits() {
        let kitList = [];
        for (let i = 0; i < this.state.productDetails.length; i++) {
            let kitCode = this.state.productDetails[i].kitCode;
            let flag = true;
            for (let j = 0; j < kitList.length; j++) {
                if (kitList[j] == kitCode) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                kitList.push({ value: kitCode });
        }
        if (kitList.length == 0)
            return null;
        this.state.selectedGenderPriority = kitList[0];
        return (
            <Dropdown
                label='kit'
                data={kitList}
                containerStyle={Styles.smallpickerStyle}
                lineWidth={0}
                inputContainerPadding={0}
                labelHeight={0}
                inputContainerStyle={Styles.inputContainerStyle}
                textColor={colors.itemContent}
                fontSize={dimens.item}
                fontFamily='Roboto-Medium' />
        );
    }
    getPickerGenders() {
        let genderList = [];
        for (let i = 0; i < this.state.productDetails.length; i++) {
            let gender = this.state.productDetails[i].gender;
            let flag = true;
            for (let j = 0; j < genderList.length; j++) {
                if (genderList[j] == gender) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                genderList.push({ value: gender });
        }
        if (genderList.length == 0)
            return null;

        return (
            <Dropdown
                label='gender'
                data={genderList}
                containerStyle={[Styles.smallpickerStyle, { borderRightWidth: 1 }]}
                lineWidth={0}
                inputContainerPadding={0}
                labelHeight={0}
                inputContainerStyle={Styles.inputContainerStyle}
                textColor={colors.itemContent}
                fontSize={dimens.item}
                fontFamily='Roboto-Medium' />
        );
    }

    getPickerBadges() {
        let badgeList = [];
        for (let i = 0; i < 3; i++) {
            badgeList.push({ value: 'badge' + (i + 1) });
        }
        return (
            <Dropdown
                label='badge'
                data={badgeList}
                containerStyle={[Styles.smallpickerStyle, Styles.pickerStyle]}
                lineWidth={0}
                inputContainerPadding={0}
                labelHeight={0}
                inputContainerStyle={Styles.inputContainerStyle}
                textColor={colors.itemContent}
                fontSize={dimens.item}
                fontFamily='Roboto-Medium' />
        );
    }

    selectAnotherProduct = (index) => {
        if (cancel != undefined)
            cancel();
        this.setState({ productDetails: [], products: [], productId: index });
        this.getProductDetail(index);
        this.getProducts(this.state.collectionCode, "", "");
    }
    getAlsoLike() {
        if (this.state.products.length == 0)
            return null;
        return this.state.products.map((product, index) => {
            return (
                <TouchableOpacity key={index} activeOpacity={0.9} style={Styles.slideItem} onPress={() => this.selectAnotherProduct(product.id)}>
                    <Image style={Styles.slideItemImg} source={{ uri: domain + product.thumbnail }} />
                    <Text style={Styles.slideItemName}>{product.name}</Text>
                    <Text style={Styles.slideItemPrice}>Aed {product.price}</Text>
                    <TouchableOpacity style={Styles.favo} onPress={() => this.onPressfav(product.id)}>
                        <Image style={[Styles.favoImg, this.getStar(product.id) ? {} : { tintColor: colors.itemContent }]}
                            source={this.getStar(product.id) ? filled_star_icon : star_icon} />
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        });
    }

    onPressfav = (index) => {
        let flag = false;
        for (var i = 0; i < this.state.wishlist.length; i++) {
            if (this.state.wishlist[i] == index) {
                flag = true;
                break;
            }
        }
        let wishlist = this.state.wishlist;
        if (flag) {
            for (var i in wishlist) {
                if (wishlist[i] == index) {
                    wishlist.splice(i, 1);
                    break;
                }
            }
        }
        else
            wishlist.push(index);
        this.setState({ wishlist: wishlist });
        this.saveData(wishlist);
    }

    saveData(wishlist) {
        this.props.dispatch(wishListActions.setWishList(wishlist));
        this.saveWishList(wishlist);
    }

    async saveWishList(value) {
        try {
            await AsyncStorage.setItem('@Victory:wishList', JSON.stringify(value));
        } catch (error) {
            console.warn("Error saving data" + error);
        }
    }

    getStar(productId) {
        let flag = false;
        for (var i = 0; i < this.state.wishlist.length; i++) {
            if (this.state.wishlist[i] == productId) {
                flag = true;
                break;
            }
        }
        return flag
    }

    onPressAddToCart() {
        let list = [];
        list = this.props.cart.get('list');
        let flag = true;
        for (var i = 0; i < list.length; i++) {
            if (list[i] == this.state.productId) {
                flag = false;
                break;
            }
        }
        if (flag) {
            list.push(this.state.productId);
            this.props.dispatch(cartActions.setCartList(list));
        }
        Api.addToCart(this.state.productId, 1, '', 1, '').then((res) => {
            // console.log('response: ', res);
            this.props.dispatch(cartWebViewActions.setCookie(res));
        });

    }

    showProductSizes() {
        // console.log('--------------------------------------');
        // console.log(this.state.productSizes);
        // console.log('---------------------------------------');
        return this.state.productSizes.map((productSize, index) => {
            return (
                <TouchableHighlight
                    underlayColor={'transparent'}
                    style={[Styles.sizeItem,
                    this.isSelectedSize(index) ? { borderColor: colors.itemSelected } : {},
                    index != 0 ? { marginLeft: width / 18 } : {}]}
                    onPress={() => this.onPressSizeItem(index)}>
                    <Text style={[Styles.txt, this.isSelectedSize(index) ? { color: colors.itemSelected } : {}]}>{productSize}</Text>
                </TouchableHighlight>
            );
        });
    }

    onPressSizeItem = (index) => {
        let flag = false;
        for (var i = 0; i < this.state.selected_sizes.length; i++) {
            if (this.state.selected_sizes[i] == index) {
                flag = true;
                break;
            }
        }
        let selected_sizes = this.state.selected_sizes;
        if (flag) {
            for (var i in selected_sizes) {
                if (selected_sizes[i] == index) {
                    selected_sizes.splice(i, 1);
                    break;
                }
            }
        }
        else
            selected_sizes.push(index);
        this.setState({ selected_sizes: selected_sizes });
    }
    isSelectedSize(index) {
        let flag = false;
        for (var i = 0; i < this.state.selected_sizes.length; i++) {
            if (index == this.state.selected_sizes[i]) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <TouchableOpacity style={Styles.backbtn} onPress={this.goBack}>
                        <Image style={Styles.img} source={back_icon} />
                    </TouchableOpacity>
                    <TouchableOpacity style={Styles.starbtn}
                        onPress={() => { this.onPressfav(this.state.productId) }} >
                        <Image style={[Styles.img, this.getStar(this.state.productId) ? {} : { tintColor: colors.itemContent }]}
                            source={this.getStar(this.state.productId) ? filled_star_icon : star_icon} />
                    </TouchableOpacity>
                    {this.state.productDetails.length != 0 ?
                        <View>
                            <View style={Styles.goodsImgView}>
                                <Image style={Styles.goodsImg} source={{ uri: domain + this.state.productDetails[0].image }} />
                            </View>
                            <View style={Styles.goodsdescpan}>
                                <Text style={Styles.name}>{this.state.productDetails.length != 0 ? this.state.productDetails[0].name : ''}</Text>
                                <Text style={Styles.price}>AED {this.state.productDetails.length != 0 ? this.state.productDetails[0].price : ''}</Text>
                                <View style={Styles.subfilterCat}>
                                    <View style={Styles.Sizeitems}>
                                        {this.showProductSizes()}
                                    </View>
                                </View>
                                <View style={Styles.selectRow}>
                                    {/* {this.getPickerGenders()} */}
                                    {/* {this.getPickerkits()} */}
                                    <View style={[Styles.half, { borderRightWidth: 1 }]}>
                                        <Text style={Styles.featureText}>{this.state.productDetails.length != 0 ? this.state.productDetails[0].gender : ''}</Text>
                                    </View>
                                    <View style={Styles.half}>
                                        <Text style={Styles.featureText}>{this.state.productDetails.length != 0 ? this.state.productDetails[0].kitCode : ''}</Text>
                                    </View>
                                </View>
                                {this.getPickerBadges()}
                                <TouchableOpacity style={Styles.btn} onPress={() => this.onPressAddToCart()}>
                                    <Text style={Styles.btnText}>{strings.addtocart}</Text>
                                </TouchableOpacity>
                                <Text style={Styles.customizeTitleText}>{strings.customize}</Text>
                                <View style={[Styles.customizeContent, { borderTopWidth: 1 }]}>
                                    <Input style={Styles.customizeInput} placeholder={strings.changename} />
                                </View>
                                <View style={Styles.customizeContent}>
                                    <Input style={Styles.customizeInput} placeholder={strings.changenumber}></Input>
                                </View>
                            </View>
                            <View style={Styles.bottomPan}>
                                <Text style={Styles.bottompanText}>{strings.youmayalsolike}</Text>
                                <ScrollView horizontal={true} showsVerticalScrollIndicator={false} style={Styles.slide}>
                                    <View style={Styles.slideBar}>
                                        {this.getAlsoLike()}
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                        : null}
                    {this.renderProgress()}
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container>
        );
    }
    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    onSubShop = (index) => {
        this.props.navigation.pop();
    }
    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
    }
    onPressWishList = () => {
        this.props.navigation.navigate(consts.WISHLIST_SCREEN);
    }
    goBack = () => {
        this.props.navigation.pop();
    }
}


const Styles = {
    containerStyle: {
        paddingBottom: height / 9,
    },
    contentStyle: {
        width: width,
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
    },
    sizeItem: {
        height: height / 18,
        width: height / 18,
        borderRadius: height / 36,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    txt: {
        fontSize: dimens.small
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        resizeMode: 'contain',
    },
    backbtn: {
        position: 'absolute',
        top: width / 20,
        left: width / 20,
        zIndex: 100000,
    },
    starbtn: {
        position: 'absolute',
        top: width / 20,
        right: width / 20,
        zIndex: 100000,
    },
    img: {
        height: width / 15,
        width: width / 15,
        resizeMode: 'contain'
    },
    goodsImgView: {
        backgroundColor: colors.itemNameBack
    },
    goodsImg: {
        width: width,
        height: width * 457 / 375,
        resizeMode: 'cover'
    },
    goodsdescpan: {
        width: width,
        backgroundColor: 'white',
        padding: width / 20,
        paddingTop: width / 20,
        paddingBottom: width / 20
    },
    name: {
        fontSize: dimens.itemTitle,
        color: colors.itemContent,
        fontFamily: 'Roboto-Bold'
    },
    price: {
        marginTop: width / 50,
        fontSize: dimens.small,
        color: colors.subItemContent,
        fontFamily: 'Roboto-Regular'
    },
    smallpickerStyle: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#aaaaaa',
        width: width / 20 * 9,
        paddingLeft: 5,
        justifyContent: 'center',
        paddingTop: width / 50,
        paddingBottom: width / 80
    },
    half: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#aaaaaa',
        width: width / 20 * 9,
        paddingLeft: 5,
        justifyContent: 'center',
        paddingTop: width / 50,
        paddingBottom: width / 50
    },
    pickerStyle: {
        borderTopWidth: 0,
        width: width / 10 * 9
    },
    inputContainerStyle: {
        paddingTop: 0,
        paddingBottom: 0
    },
    pickerTextStyle: {
        fontFamily: 'Roboto-Medium',
        fontSize: dimens.item,
        fontColor: colors.itemContent,
        fontColor: 'red'
    },
    selectRow: {
        marginTop: width / 50,
        flexDirection: 'row',
    },
    btn: {
        width: width / 10 * 9,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        backgroundColor: colors.itemNameBack,
        borderRadius: 15,
        marginTop: height / 20,
        marginBottom: height / 20,
    },
    btnText: {
        color: '#057EFF',
        fontSize: dimens.itemTitle,
        fontFamily: 'Roboto-Medium'
    },
    customizeTitleText: {
        fontSize: dimens.h2,
        color: colors.itemContent,
        fontFamily: 'Roboto-Medium',
        marginBottom: height / 80
    },
    customizeContent: {
        borderBottomWidth: 1,
        borderColor: '#aaaaaa'
    },
    customizeInput: {
        fontSize: dimens.small,
        paddingBottom: 0,
        paddingTop: 0,
    },
    bottomPan: {
        width: width,
        paddingLeft: width / 20,
        paddingRight: width / 20,
        backgroundColor: 'white',
        marginTop: height / 50
    },
    bottompanText: {
        fontSize: dimens.h2,
        color: colors.itemContent,
        fontFamily: 'Roboto-Medium',
        marginBottom: height / 80
    },
    slideBar: {
        flexDirection: 'row',
    },
    slideItemImg: {
        width: width / 5 * 2,
        height: width / 5 * 2 * 167 / 143,
        resizeMode: 'contain',
        backgroundColor: colors.itemNameBack
    },
    favo: {
        height: 20,
        width: 20,
        position: 'absolute',
        top: 5,
        right: 5,
        zIndex: 1000,
    },
    favoImg: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
        zIndex: 10000
    },
    slide: {
        paddingBottom: width / 20
    },
    slideItem: {
        width: width / 5 * 2,
        marginRight: width / 40,
    },
    slideItemName: {
        fontSize: dimens.small,
        fontFamily: 'Roboto-Medium',
        color: colors.itemContent
    },
    slideItemPrice: {
        fontSize: dimens.small - 2,
        fontFamily: 'Roboto-Regular',
        color: colors.subItemContent
    },
    Sizeitems: {
        flexDirection: 'row',
        width: width / 20 * 17,
        paddingRight: width / 10,
        justifyContent: 'flex-start'
    },
    subfilterCat: {
        width: width / 20 * 17,
        borderColor: '#aaaaaa',
        paddingLeft: width / 20,
        justifyContent: 'center',
        height: height / 12
    },
};

const mapStateToProps = (state) => ({
    Goods: state.get('goods'),
    subshop: state.get('subShop'),
    wishList: state.get('wishList'),
    root: state.get('root'),
    cart: state.get('cart')
});

export default connect(mapStateToProps)(Goods)