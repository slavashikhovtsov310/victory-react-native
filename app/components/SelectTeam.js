import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableHighlight } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Shop_Header";
import SubShop from "./SubShop";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import sample from '../assets/sample.png';
import Orientation from 'react-native-orientation';
import arrow_go_icon from '../assets/icons/arrow_go.png'
import Toast from 'react-native-toast-native';
import { DrawerNavigator } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import rm_icon from '../assets/icons/rm.png';
import fc_icon from '../assets/icons/fc.png';
import star_icon from '../assets/icons/star.png';
import filled_star_icon from '../assets/icons/filled_star.png';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class SelectTeam extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false,
            itemSel: [{ selected: false }, { selected: false }, { selected: false }, { selected: false }, { selected: false }]
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
    }

    componentDidUpdate() {
        this.proceed()
    }

    proceed() {
        // const loginError = this.props.Login.get('loginError');
        // const isLoggedIn = this.props.Login.get('isLoggedIn');
        // const toastStyles = {
        //     width: width,
        //     height: height / 6,
        //     backgroundColor: '#34343400',
        //     color: 'red',
        //     fontSize: dimens.text_size_button,
        //     alignItems: 'center',
        //     alignSelf: 'center',
        //     justifyContent: 'center'
        // }
        // // Alert.alert(this.state.email + ', ' + loginError.msg)
        // // if (isLoggedIn)
        // //     Alert.alert('1')
        // // if (this.state.isGoneAlready)
        // //     Alert.alert('2')
        // // if (this.isObject(loginError.msg))
        // //     Alert.alert('3')
        // // if (loginError.msg)
        // //     Alert.alert('4')
        // if (this.isObject(loginError) && loginError && !this.isObject(loginError.msg) && loginError.msg) {
        //     Toast.show(loginError.msg, Toast.SHORT, Toast.CENTER, toastStyles);
        //     // Alert.alert(loginError + '');
        //     this.props.dispatch(loginActions.setError({}))
        // } else if (isLoggedIn && !this.state.isGoneAlready) {
        //     this.setState({ isGoneAlready: true });
        //     this.props.navigation.navigate(consts.DASHBOARD_SCREEN);
        // }
    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    // noinspection JSMethodCanBeStatic
    setEmail(text) {
        this.setState({ email: text });
    }
    setPassword(text) {
        this.setState({ password: text });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    setSearch = (text) => {

    }
    itemSel = (index) => {
        this.state.itemSel[index].selected = !this.state.itemSel[index].selected;
        this.forceUpdate()
    }
    render() {
        return (

            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <LinearGradient
                    start={{ x: 0, y: 1 }}
                    end={{ x: 1, y: 1 }}
                    colors={['#81d7af', '#8091da']}
                    style={Styles.hdr}>
                    <TouchableHighlight underlayColor='transparent' style={Styles.backBtn} onPress={this.goBack}>
                        <Image style={Styles.backBtnImg} source={arrow_go_icon} />
                    </TouchableHighlight>
                    <View style={{ width: width / 8 * 7 }}>
                        <SearchTextInput
                            onChangeText={(text) => this.setSearch(text)}
                            placeholder={strings.searchyourteam}
                            color={'black'}
                            secureTextEntry={false} />
                    </View>
                </LinearGradient>
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.classTitle}>
                        <Text style={Styles.classTitleText}>{strings.popularleague}</Text>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={rm_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>Premier League</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn} onPress={(index) => this.itemSel(0)}>
                                <Image
                                    style={[Styles.optImg, this.state.itemSel[0].selected ? {} : { tintColor: colors.itemContent }]}
                                    source={this.state.itemSel[0].selected ? filled_star_icon : star_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={fc_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>Serie A</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn} onPress={(index) => this.itemSel(1)}>
                                <Image
                                    style={[Styles.optImg, this.state.itemSel[1].selected ? {} : { tintColor: colors.itemContent }]}
                                    source={this.state.itemSel[1].selected ? filled_star_icon : star_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={rm_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>Premire League</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn} onPress={(index) => this.itemSel(2)}>
                                <Image
                                    style={[Styles.optImg, this.state.itemSel[2].selected ? {} : { tintColor: colors.itemContent }]}
                                    source={this.state.itemSel[2].selected ? filled_star_icon : star_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={fc_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>La Liga</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn} onPress={(index) => this.itemSel(3)}>
                                <Image
                                    style={[Styles.optImg, this.state.itemSel[3].selected ? {} : { tintColor: colors.itemContent }]}
                                    source={this.state.itemSel[3].selected ? filled_star_icon : star_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={rm_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>Champions League</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn} onPress={(index) => this.itemSel(4)}>
                                <Image
                                    style={[Styles.optImg, this.state.itemSel[4].selected ? {} : { tintColor: colors.itemContent }]}
                                    source={this.state.itemSel[4].selected ? filled_star_icon : star_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <TouchableHighlight style={Styles.more}>
                        <Text>+3 {strings.more}</Text>
                    </TouchableHighlight>
                    <View style={Styles.classTitle}>
                        <Text style={Styles.classTitleText}>{strings.countries}</Text>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={rm_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>{strings.england}</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn}>
                                <Image style={Styles.optImg} source={arrow_go_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={fc_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>{strings.france}</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn}>
                                <Image style={Styles.optImg} source={arrow_go_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={rm_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>{strings.spain}</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn}>
                                <Image style={Styles.optImg} source={arrow_go_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={Styles.itemRow}>
                        <Image style={Styles.leagueImg} source={fc_icon} />
                        <View style={Styles.itemGroup}>
                            <View style={Styles.itemDesc}>
                                <Text style={Styles.leagueName}>{strings.italy}</Text>
                                <Text style={Styles.teams}>6 {strings.teams}</Text>
                            </View>
                            <TouchableHighlight underlayColor='transparent' style={Styles.optBtn}>
                                <Image style={Styles.optImg} source={arrow_go_icon} />
                            </TouchableHighlight>
                        </View>
                    </View>
                    <TouchableHighlight style={Styles.more}>
                        <Text>+3 {strings.more}</Text>
                    </TouchableHighlight>
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container >
        );
    }
    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        this.setState({ isGoneAlready: false });
        this.props.dispatch(loginActions.login(this.state.email, this.state.password));
    }
    onSignupNowPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
    onSubShop = (index) => {
        // Alert.alert('' + index);
        this.props.navigation.navigate(consts.SUBSHOP_SCREEN);
    }
    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
        // Alert.alert('test')
    }
    onPressWishList = () => {
        this.props.navigation.navigate(consts.WISHLIST_SCREEN);
        // Alert.alert('test')
    }
    onPressOpt = () => {
        // Alert.alert('test')
        this.props.navigation.navigate('DrawerOpen')
    }
    goBack = () => {
        this.props.navigation.pop();
    }
}


const Styles = {
    containerStyle: {
        // alignItems: 'center',
        // backgroundColor: '#ffffff'
        paddingBottom: height / 9,
    },
    contentStyle: {
        // position: 'absolute',
        // top: height / 40 * (3.8 + 2),
        // height: height * 9 / 10 - StatusBar.currentHeight,
        // flex: 1,
        // flexDirection: 'column',
        // justifyContent: 'center',
        width: width,
        marginTop: width / 20,
        alignItems: 'center',
        paddingBottom: height / 9,
        alignSelf: 'center',
        backgroundColor: 'white'
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        // height: height,
        resizeMode: 'cover',
    },
    hdr: {
        flexDirection: 'row',
        alignItems: 'center',
        width: width,
        paddingLeft: width / 10,
        padding: width / 30,
    },
    backBtnImg: {
        transform: [
            { rotate: '180deg' }
        ],
        tintColor: 'white',
        paddingTop: height / 50,
        paddingBottom: height / 50,
        resizeMode: 'contain'
    },
    itemRow: {
        flexDirection: 'row',
        paddingLeft: width / 20,
        width: width,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    more: {
        width: width,
        paddingLeft: width / 5 * 0.8,
        paddingTop: height / 60,
        paddingBottom: height / 60
    },
    leagueImg: {
        width: width / 15,
        height: width / 15,
        resizeMode: 'stretch'
    },
    itemGroup: {
        paddingRight: width / 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#aaaaaa',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: width / 5 * 4.2,
        paddingTop: height / 70,
        paddingBottom: height / 70
    },
    classTitle: {
        flexDirection: 'row',
        paddingLeft: width / 20,
        paddingRight: width / 20,
        backgroundColor: colors.itemNameBack,
        paddingBottom: height / 70,
        paddingTop: height / 70,
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    teams: {
        fontSize: 10
    },
    optImg: {
        height: width / 15,
        width: width / 15,
        resizeMode: 'contain',
    }
};

const mapStateToProps = (state) => ({
    SelectTeam: state.get('selectTeam'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(SelectTeam)