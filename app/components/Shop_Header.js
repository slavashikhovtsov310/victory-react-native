import React, { Component } from "react";
import { Content, Input, Item, Label, Text, Left, View } from "native-base";
import { Alert, Image, Dimensions, TextInput, TouchableOpacity, Platform } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import opt_icon from '../assets/icons/opt.png'
import star_icon from '../assets/icons/star.png'
import colors from "../resources/colors";
import dimens from "../resources/dimens";

const { width, height } = Dimensions.get('window');


export default class Shop_Header extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            })
        };
        // Alert.alert(width + ', ' + height);
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <View
                    shouldRasterizeIOS
                    renderToHardwareTextureAndroid
                    style={styles.row} scrollEnabled={false}>
                    <TouchableOpacity style={styles.optionBtn} onPress={this.props.onPressOpt}>
                        <Image style={styles.optionStyle} source={opt_icon} />
                    </TouchableOpacity>
                    <Text style={styles.labelStyle}>Victory</Text>
                    <TouchableOpacity style={styles.exitBtn} onPress={this.props.onPressWishList} >
                        <Image style={styles.exitStyle} source={star_icon} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.sublabel}>shop</Text>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        // position: 'absolute',
        // top: 0,
        // flex: 1,
        // flexDirection: 'row',
        // width: width,
        height: height / 40 * 3.8,
        // backgroundColor: colors.accentColor,
        // alignSelf: 'center',
        alignItems: 'center',
        // justifyContent: 'center',
        zIndex: 1000
    },
    row: {
        flexDirection: 'row',
        width: width,
        height: height / 40 * 3.8,
        backgroundColor: colors.accentColor,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    optionBtn: {
        position: 'absolute',
        left: 20,
        width: width / 15,
        height: width / 15,
    },
    optionStyle: {
        width: width / 15,
        resizeMode: 'contain',
        height: width / 15,
        tintColor: colors.itemContent
    },
    labelStyle: {
        fontSize: dimens.header,
        color: colors.itemContent,
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
    },
    sublabel: {
        color: colors.header_sub,
        position: 'absolute',
        bottom: 0,
        paddingLeft: width / 10,
        fontFamily: Platform.OS === 'android' ? 'Aller_Std_Bd' : 'Aller-Bold',
        fontStyle: 'italic',
        fontSize: 10
    },
    exitBtn: {
        position: 'absolute',
        right: 20,
        width: width / 15,
        height: width / 15,
    },
    exitStyle: {
        width: width / 15,
        height: width / 15,
        tintColor: colors.itemContent
    },
};

Shop_Header.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};