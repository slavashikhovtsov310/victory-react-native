import React, { Component } from "react";
import { connect, Provider } from "react-redux";
import { StackNavigator, createStackNavigator } from "react-navigation";
import Login from "./Login";
import Signup from "./Signup";
import Setup from "./Setup";
import Stat from "./Stat";
import Shop from "./Shop";
import Cart from "./Cart";
import CheckoutSuccess from "./CheckoutSuccess";
import SubShop from "./SubShop";
import Filter from "./Filter";
import CreditCard from "./CreditCard";
import Checkout from "./Checkout";
import WishList from "./WishList";
import Goods from "./Goods";
import ScoreDetail from "./ScoreDetail";
import Profile from "./Profile";
import SelectLeague from "./SelectLeague";
import CartWebView from "./CartWebView";
import SelectTeam from "./SelectTeam";

import configureStore from "../store/configureStore";

const store = configureStore();
const Routes = {
  Login: { screen: Login },
  Signup: { screen: Signup },
  Setup: { screen: Setup },
  Shop: { screen: Shop },
  Cart: { screen: Cart },
  SubShop: { screen: SubShop },
  Filter: { screen: Filter },
  CreditCard: { screen: CreditCard },
  CheckoutSuccess: { screen: CheckoutSuccess },
  Checkout: { screen: Checkout },
  WishList: { screen: WishList },
  Stat: { screen: Stat },
  Goods: { screen: Goods },
  ScoreDetail: { screen: ScoreDetail },
  Profile: { screen: Profile },
  SelectLeague: { screen: SelectLeague },
  SelectTeam: { screen: SelectTeam },
  CartWebView: { screen: CartWebView },
};
const Navigator = createStackNavigator(Routes, {
  headerMode: 'screen'
});

export class Navigation extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login,
    signup: state.signup,
    setup: state.setup,
    stat: state.stat,
    shop: state.shop,
    cart: state.cart,
    subShop: state.subShop,
    filter: state.filter,
    creditCard: state.creditCard,
    checkoutSuccess: state.checkoutSuccess,
    checkout: state.checkout,
    wishList: state.wishList,
    goods: state.goods,
    scoreDetail: state.scoreDetail,
    profile: state.profile,
    selectLeague: state.selectLeague,
    selectTeam: state.selectTeam,
    cartWebView: state.cartWebView,
  }
}

export default connect(mapStateToProps)(Navigation);

