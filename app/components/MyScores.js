import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Stat_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import filter_icon from '../assets/icons/filter.png'
import rm_icon from '../assets/icons/rm.png'
import fc_icon from '../assets/icons/fc.png'
import yellowCard_icon from '../assets/icons/yellowCard.png'
import redCard_icon from '../assets/icons/redCard.png'
import shot_icon from '../assets/icons/shot.png'
import sample from '../assets/sample.png';
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class MyScores extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            isGoneAlready: false
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
    }

    componentDidUpdate() {
        this.proceed()
    }

    proceed() {
        const loginError = this.props.Login.get('loginError');
        const isLoggedIn = this.props.Login.get('isLoggedIn');
        const toastStyles = {
            width: width,
            height: height / 6,
            backgroundColor: '#34343400',
            color: 'red',
            fontSize: dimens.text_size_button,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center'
        }
        // Alert.alert(this.state.email + ', ' + loginError.msg)
        // if (isLoggedIn)
        //     Alert.alert('1')
        // if (this.state.isGoneAlready)
        //     Alert.alert('2')
        // if (this.isObject(loginError.msg))
        //     Alert.alert('3')
        // if (loginError.msg)
        //     Alert.alert('4')
        if (this.isObject(loginError) && loginError && !this.isObject(loginError.msg) && loginError.msg) {
            Toast.show(loginError.msg, Toast.SHORT, Toast.CENTER, toastStyles);
            // Alert.alert(loginError + '');
            this.props.dispatch(loginActions.setError({}))
        } else if (isLoggedIn && !this.state.isGoneAlready) {
            this.setState({ isGoneAlready: true });
            this.props.navigation.navigate(consts.DASHBOARD_SCREEN);
        }
    }

    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header label='myscores'
                    onPressWishList={this.onPressWishList}
                    onPressAllscores={this.onPressAllscores}
                    onPressStandings={this.onPressStandings} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.desc}>
                        <Text>#Season 2017</Text>
                        <Text style={{ fontSize: dimens.h2, color: 'black' }}>{strings.favoriteteams}</Text>
                    </View>
                    <View style={Styles.subdesc}>
                        <Text style={Styles.sep}>All Leagues</Text>
                    </View>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={rm_icon} />
                                <Text style={[Styles.Team_name, , { marginLeft: width / 30 }]}>Real Madrid</Text>
                            </View>
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>1 : 1</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 30 }]}>Barcelona</Text>
                                <Image style={Styles.Team_Mark} source={fc_icon} />
                            </View>
                        </View>
                    </View>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={rm_icon} />
                                <Text style={[Styles.Team_name, , { marginLeft: width / 30 }]}>Real Madrid</Text>
                            </View>
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>1 : 1</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 30 }]}>Barcelona</Text>
                                <Image style={Styles.Team_Mark} source={fc_icon} />
                            </View>
                        </View>
                    </View>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={rm_icon} />
                                <Text style={[Styles.Team_name, , { marginLeft: width / 30 }]}>Real Madrid</Text>
                            </View>
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>1 : 1</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 30 }]}>Barcelona</Text>
                                <Image style={Styles.Team_Mark} source={fc_icon} />
                            </View>
                        </View>
                    </View>
                    <View>
                        <Text>Favorite Leagues</Text>
                    </View>
                    <View style={Styles.subdesc}>
                        <Text style={Styles.sep}>La Liga</Text>
                    </View>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={rm_icon} />
                                <Text style={[Styles.Team_name, , { marginLeft: width / 30 }]}>Real Madrid</Text>
                            </View>
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>1 : 1</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 30 }]}>Barcelona</Text>
                                <Image style={Styles.Team_Mark} source={fc_icon} />
                            </View>
                        </View>
                    </View>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={rm_icon} />
                                <Text style={[Styles.Team_name, , { marginLeft: width / 30 }]}>Real Madrid</Text>
                            </View>
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>1 : 1</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 30 }]}>Barcelona</Text>
                                <Image style={Styles.Team_Mark} source={fc_icon} />
                            </View>
                        </View>
                    </View>
                    <View style={Styles.subdesc}>
                        <Text style={Styles.sep}>Serie A</Text>
                    </View>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={rm_icon} />
                                <Text style={[Styles.Team_name, , { marginLeft: width / 30 }]}>Real Madrid</Text>
                            </View>
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>1 : 1</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 30 }]}>Barcelona</Text>
                                <Image style={Styles.Team_Mark} source={fc_icon} />
                            </View>
                        </View>
                    </View>
                    <View style={Styles.match}>
                        <View style={Styles.A_Team}>
                            <View style={Styles.A_Team_name_group}>
                                <Image style={Styles.Team_Mark} source={rm_icon} />
                                <Text style={[Styles.Team_name, , { marginLeft: width / 30 }]}>Real Madrid</Text>
                            </View>
                        </View>
                        <View style={Styles.match_result}>
                            <Text style={Styles.vs}>1 : 1</Text>
                        </View>
                        <View style={Styles.B_Team}>
                            <View style={Styles.B_Team_name_group}>
                                <Text style={[Styles.Team_name, { marginRight: width / 30 }]}>Barcelona</Text>
                                <Image style={Styles.Team_Mark} source={fc_icon} />
                            </View>
                        </View>
                    </View>
                    {this.renderProgress()}
                </Content>
                <Footer onPressBag={this.onPressBag} />
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={loginStyles.progressStyle} />
        )
    }

    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        this.setState({ isGoneAlready: false });
        this.props.dispatch(loginActions.login(this.state.email, this.state.password));
    }
    onSignupNowPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
        // Alert.alert('test')
    }
    onFilterPress = () => {
        this.props.navigation.navigate(consts.FILTER_SCREEN);
        // Alert.alert('test')
    }
    onPressWishList = () => {
        this.props.navigation.navigate(consts.WISHLIST_SCREEN);
        // Alert.alert('test')
    }
    onPressAllscores = () => {
        this.props.navigation.navigate(consts.STAT_SCREEN);
        // Alert.alert('test')
    }
}


const Styles = {
    containerStyle: {
        // alignItems: 'center',
        // backgroundColor: '#ffffff'
        paddingBottom: height / 9,
    },
    contentStyle: {
        // position: 'absolute',
        // top: height / 40 * (3.8 + 2),
        // height: height * 9 / 10 - StatusBar.currentHeight,
        // flex: 1,
        // flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        // height: height,
        resizeMode: 'contain',
    },
    desc: {
        width: width,
        paddingLeft: width / 20,
        padding: width / 30,
    },
    subdesc: {
        width: width,
        paddingLeft: width / 20,
        backgroundColor: '#aaaaaa',
        padding: width / 30
    },
    match: {
        flexDirection: 'row',
        width: width,
        paddingLeft: width / 20,
        paddingRight: width / 9,
        justifyContent: 'space-between',
        paddingTop: width / 30,
        paddingBottom: width / 30,
        borderBottomWidth: 1,
        backgroundColor: 'white',
        borderBottomColor: '#aaaaaa',
    },
    A_Team_name_group: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    B_Team_name_group: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    Team_Mark: {
        width: width / 30 * 2,
        height: width / 30 * 2,
        resizeMode: 'stretch'
    },
    Team_name: {
        fontSize: dimens.h2,
        color: 'black',
    },
    shot_Mark: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    card_Mark: {
        width: width / 30,
        height: width / 30,
        resizeMode: 'contain'
    },
    match_result: {
        alignItems: 'center'
    },
    vs: {
        fontSize: dimens.h2,
        fontWeight: 'Bold',
        color: 'black'
    },
    sep: {
        fontSize: dimens.h2 - 2
    }

};

const mapStateToProps = (state) => ({
    MyScores: state.get('myScores'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(MyScores)