import React, { Component } from "react";
import { Content, Input, Item, Label, Text, Left, View } from "native-base";
import { Alert, Image, Dimensions, TextInput, TouchableOpacity } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import opt_icon from '../assets/icons/opt.png'
import star_icon from '../assets/icons/star.png'
import colors from "../resources/colors";
import { connect } from "react-redux";
import * as rootActions from "../actions/root-actions";
import dimens from "../resources/dimens";
import consts from "../const";
import stats_icon from '../assets/icons/shop_big.png';
import home_icon from '../assets/icons/home_big.png';
import bag_icon from '../assets/icons/bag_big.png';
import { BoxShadow } from 'react-native-shadow';

const { width, height } = Dimensions.get('window');


export class Footer extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            })
        };
        // Alert.alert(width + ', ' + height);
    }
    onPressShop = () => {
        this.props.navigation.navigate(consts.SHOP_SCREEN);
        this.props.dispatch(rootActions.MyFavoriteLeaguesChanged(true));
    }
    onPressHome = () => {
        this.props.navigation.navigate(consts.SETUP_SCREEN);
        this.props.dispatch(rootActions.MyFavoriteLeaguesChanged(true));
    }
    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
        this.props.dispatch(rootActions.MyFavoriteLeaguesChanged(true));
    }
    render() {
        const shadowOpt = {
            width: width,
            height: height / 11,
            color: '#fff',
            radius: 1,
            opacity: 0.6,
            x: 0,
            y: 1,
            style: {
                flexDirection: 'row',
                width: width,
                height: height / 11,
                // zIndex: 1000,
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 5,
            }
        }
        return (
            <View
                style={Styles.footer}>
                <BoxShadow setting={shadowOpt}>
                    <View style={[Styles.left, Styles.cell]}>
                        <TouchableOpacity style={Styles.footerBtn} onPress={this.onPressShop}>
                            <Image style={Styles.footerBtnImg} source={stats_icon} />
                        </TouchableOpacity>
                        <Text style={Styles.btnText}>{strings.shop}</Text>
                    </View>
                    <View style={Styles.cell}>
                        <TouchableOpacity style={Styles.footerBtn} onPress={this.onPressHome}>
                            <Image style={Styles.footerBtnImg} source={home_icon} />
                        </TouchableOpacity>
                        <Text style={Styles.btnText}>{strings.home}</Text>
                    </View>
                    <View style={[Styles.right, Styles.cell]}>
                        <TouchableOpacity style={Styles.footerBtn} onPress={this.onPressBag}>
                            <Image style={Styles.footerBtnImg} source={bag_icon} />
                        </TouchableOpacity>
                        <Text style={Styles.btnText}>{strings.bag}</Text>
                    </View>
                </BoxShadow>
            </View>
        );
    }
}

const Styles = {
    footer: {
        paddingTop: 5,
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        width: width,
        height: height / 11,
        zIndex: 1000,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: colors.itemNameBack,
        shadowOffset: { width: 3, height: 3 },
        backgroundColor: colors.itemNameBack,
        opacity: 1,
        shadowRadius: 10,
        shadowOpacity: 1,
        paddingTop: 5
    },
    left: {
        position: 'absolute',
        left: width / 10
    },
    right: {
        position: 'absolute',
        right: width / 10
    },
    cell: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    footerBtn: {
        height: height / 20,
        width: height / 20,
        borderRadius: height / 40,
        borderColor: '#565656',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    footerBtnImg: {
        height: height / 35,
        width: height / 35,
        resizeMode: 'contain',
        tintColor: 'black'
    },
    btnText: {
        marginTop: 5,
        fontFamily: 'Open Sans',
        fontSize: 10,
        color: colors.itemContent
    }
};

Footer.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string,

};


const mapStateToProps = (state) => ({
    SelectLeague: state.get('selectLeague'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Footer)