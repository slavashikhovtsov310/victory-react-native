import React, { Component } from "react";
import { Image, StatusBar, Text, Dimensions, TouchableOpacity, Platform, WebView } from "react-native";
import { Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Simple_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import * as cartActions from "../actions/cart-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import trash from '../assets/icons/trash.png';
import Orientation from 'react-native-orientation';
import { Dropdown } from 'react-native-material-dropdown';
import axios from 'axios';

console.ignoredYellowBox = ['Warning:'];
console.disableYellowBox = true
const CacelToken = axios.CancelToken;
let cancel;
const { width, height } = Dimensions.get('window');
const domain = "https://victory.softwarehouse.io/cart/";
let list = [];
export class CartWebView extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            list: [],
            products: [],
            expands: [],
            cookie: '',
            dummy: false
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false));
        list = this.props.Cart.get('list');
        this.initialize();
    }

    initialize() {
        var cookie = this.props.cartWebView.get('cookie');
        // console.log('cookie: ', cookie);
        this.setState({ cookie: cookie });
        this.forceUpdate();
    }

    render() {
        return (
            this.state.cookie == '' ? null :
                <WebView
                    source={{
                        uri: domain,
                        headers: {
                            'cookie': 'cookie-consent=false; session=ejCI3xRRYZ4lfUMIcHXe235qmJZ5GwuxWHp8w_enWg8CalgFmdWGA3UoJJRgw2pugTd3nww73q4TxOtfsV_1GstWWGKXAVYD5O4oyKjeynsjlXa9gUcEaE8TrcN3U-m0VrwmTs9V4IXsgcvr4UU-Qr8cdIJfLs68g20B6LNNuVqJ0P0LcSOMwux6r5AbBWjCmPl5DC5d3hoN-YbsGH4l1r6vuK5_RCVOMm-p2xQEkNYIYouv3wdG9SvSCNcIwDQtx2Wy-JJ0oCI5lk13NMYPQrdxcS4pkNNaslhrxgcenktbHwOcqLH3Wx2NmDnKf4p7K2oQMCdTafWubqZz1kLGS2Y6V8L9r1BwFxlQ5J4; quantity=6'
                        }
                    }} />
        );
    }
}

const Styles = {

};

const mapStateToProps = (state) => ({
    Cart: state.get('cart'),
    root: state.get('root'),
    cartWebView: state.get('cartWebView')
});

export default connect(mapStateToProps)(CartWebView)