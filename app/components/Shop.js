import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, AsyncStorage } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Shop_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import SearchTextInput from "./SearchTextInput";
import * as wishListActions from "../actions/wishList-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import sample from '../assets/sample.png';
import Orientation from 'react-native-orientation';
import LeftSideMenu from "./Stat_LeftSideMenu";
import Carousel from 'react-native-snap-carousel';
const { width, height } = Dimensions.get('window');

console.ignoredYellowBox = ['Warning:'];
export class Shop extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false,
            onLeftSideMenu: false,
            dummy: false,
            entry: [1, 2, 3]
        },
            this.onSubShop = this.onSubShop.bind(this);
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false));
        this.getWishList();
    }



    async getWishList() {
        try {
            const value = await AsyncStorage.getItem('@Victory:wishList');
            let wishList = [];
            if (value == null)
                return;
            wishList = JSON.parse(value);
            this.props.dispatch(wishListActions.setWishList(wishList))
        } catch (error) {
            console.log("Error retrieving data" + error);
        }
    }

    componentDidUpdate() {
    }


    setSearch = (text) => {

    }

    onSubShop = (itemIndex) => {
        this.props.navigation.navigate(consts.SUBSHOP_SCREEN);
    }

    renderItem(item) {
        return (
            <TouchableOpacity activeOpacity={0.8} style={Styles.slideitem} onPress={() => this.onSubShop(item)}>
                <Image style={Styles.itemImg} source={sample} />
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header
                    onPressWishList={this.onPressWishList}
                    onPressOpt={this.onPressOpt} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <SearchTextInput
                        onChangeText={(text) => this.setSearch(text)}
                        placeholder={strings.search}
                        color={'black'}
                        secureTextEntry={false} />
                    <TouchableOpacity activeOpacity={0.8}
                        style={[Styles.item, { marginBottom: height / 40 }]}
                        onPress={(itemIndex) => this.onSubShop(0)}>
                        <Image style={Styles.itemImg} source={sample} />
                        <Text style={Styles.featured}>Featured</Text>
                    </TouchableOpacity>
                    <Carousel
                        costyle={Styles.wrapper}
                        data={this.state.entry}
                        renderItem={(item) => this.renderItem(item)}
                        sliderWidth={width}
                        sliderHeight={height * 1.7 / 3}
                        itemWidth={width / 20 * 17} />
                    <TouchableOpacity activeOpacity={0.8} style={Styles.item} onPress={(itemIndex) => this.onSubShop(2)}>
                        <Image style={Styles.itemImg} source={sample} />
                    </TouchableOpacity>
                    {this.renderProgress()}
                </Content>
                <Footer isChanged={this.state.dummy} navigation={this.props.navigation} />
                {this.state.onLeftSideMenu ?
                    <LeftSideMenu
                        refresh={this.refresh}
                        navigation={this.props.navigation}
                        hideLeftMenu={this.hideLeftMenu} />
                    : null}
            </Container >
        );
    }
    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }
    refresh = () => {
        if (strings.getLanguage() === 'en') {
            strings.setLanguage('ar')
        }
        else {
            strings.setLanguage('en')
        }
        this.props.dispatch(rootActions.switchLanguage(strings.getLanguage()));
        this.setState({ dummy: !this.state.dummy })
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onLoginPress = () => {
        this.setState({ isGoneAlready: false });
        this.props.dispatch(loginActions.login(this.state.email, this.state.password));
    }
    onSignupNowPress = () => {
        this.props.navigation.navigate(consts.SIGNUP_SCREEN);
    }
    onPressWishList = () => {
        this.props.navigation.navigate(consts.WISHLIST_SCREEN);
    }
    onPressOpt = () => {
        this.setState({ onLeftSideMenu: true });
    }
    hideLeftMenu = () => {
        this.setState({ onLeftSideMenu: false });
    }
}


const Styles = {
    containerStyle: {
        paddingBottom: height / 9,
    },
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    wrapper: {
        width: width,
        // width: width / 40 * 35,
        height: height * 1.7 / 3,
        marginTop: height / 40,
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width / 20 * 17,
        resizeMode: 'contain',
    },
    item: {
        width: width / 20 * 17,
        height: height * 1.7 / 3,
        marginTop: height / 40,
    },
    slideitem: {
        width: width / 20 * 17,
        // width: width / 40 * 17,
        height: height * 1.7 / 3,
        alignSelf: 'center',
        zIndex: 100000,
    },
    itemImg: {
        width: width / 20 * 17,
        // width: width / 40 * 17,
        height: height * 1.7 / 3,
        resizeMode: 'cover',
        borderRadius: 15,
    },
    featured: {
        position: 'absolute',
        top: width / 2,
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: dimens.featured,
        fontFamily: 'Roboto-Bold',
        color: 'white',
        letterSpacing: width / 60
    }
};

const mapStateToProps = (state) => ({
    Shop: state.get('shop'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Shop)