import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, TouchableHighlight } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Simple_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as filterActions from '../actions/filter-actions';
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import stat_icon from '../assets/icons/stat.png';
import shop_icon from '../assets/icons/shop.png';
import arrow_go_icon from '../assets/icons/arrow_go.png';
import arrow_down_icon from '../assets/icons/arrow_down.png';
import Orientation from 'react-native-orientation';
import axios from 'axios';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
const CacelToken = axios.CancelToken;
let cancel;
export class Filter extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            cat1: false,
            cat2: false,
            cat3: false,
            cat4: false,
            cat5: false,
            cat6: false,
            brands: [],
            productSizes: [],
            genders: [],
            kits: [],
            selected_brands: [],
            selected_kits: [],
            selected_sizes: [],
            selected_teams: [],
            selected_genders: [],
            selected_sortBy: 0,
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false));
        this.getBrands();
        this.getProductKits();
        this.getProductSizes();
        this.getGenders();
    }

    componentDidUpdate() {
    }


    isObject(obj) {
        return typeof obj === 'object';
    }

    // noinspection JSMethodCanBeStatic
    getBrands() {
        const query = `
                    query brands {
                        getBrands {
                            name
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.setState({ brands: res.data.data.getBrands });
            });
        } catch (error) {
            return null;
        }
    }
    getProductSizes() {
        const query = `
                    query ProductSizes {
                        getProductSizes{
                            name
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.setState({ productSizes: res.data.data.getProductSizes });
            });
        } catch (error) {
            return null;
        }
    }
    getProductKits() {
        const query = `
                    query productKits {
                        getProductKits{
                            name
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.setState({ kits: res.data.data.getProductKits });
            });
        } catch (error) {
            return null;
        }
    }
    getGenders() {
        const query = `
                    query genders {
                        getProductGenders{
                            name
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.setState({ genders: res.data.data.getProductGenders });
            });
        } catch (error) {
            return null;
        }
    }
    onPressCat = (index) => {
        switch (index) {
            case 1:
                if (this.state.cat1) {
                    this.setState({ cat1: false });
                }
                else {
                    this.setState({ cat1: true });
                }
                break;
            case 2:
                if (this.state.cat2) {
                    this.setState({ cat2: false });
                }
                else {
                    this.setState({ cat2: true });
                }
                break;
            case 3:
                if (this.state.cat3) {
                    this.setState({ cat3: false });
                }
                else {
                    this.setState({ cat3: true });
                }
                break;
            case 4:
                if (this.state.cat4) {
                    this.setState({ cat4: false });
                }
                else {
                    this.setState({ cat4: true });
                }
                break;
            case 5:
                if (this.state.cat5) {
                    this.setState({ cat5: false });
                }
                else {
                    this.setState({ cat5: true });
                }
            case 6:
                if (this.state.cat6) {
                    this.setState({ cat6: false });
                }
                else {
                    this.setState({ cat6: true });
                }
                break;
        }
    }
    showBrands() {
        return this.state.brands.map((brand, index) => {
            return (
                <TouchableHighlight
                    underlayColor={'transparent'}
                    key={index} style={Styles.subfilterCat}
                    onPress={() => this.onPressBrandItem(index)}>
                    <Text style={[Styles.txt, this.isSelectedBrand(index) ? { color: colors.itemSelected } : {}]}>{brand.name}</Text>
                </TouchableHighlight>
            );
        });
    }

    onPressBrandItem = (index) => {
        let flag = false;
        for (var i = 0; i < this.state.selected_brands.length; i++) {
            if (this.state.selected_brands[i] == index) {
                flag = true;
                break;
            }
        }
        let selected_brands = this.state.selected_brands;
        if (flag) {
            for (var i in selected_brands) {
                if (selected_brands[i] == index) {
                    selected_brands.splice(i, 1);
                    break;
                }
            }
        }
        else
            selected_brands.push(index);
        this.setState({ selected_brands: selected_brands });
    }
    isSelectedBrand(index) {
        let flag = false;
        for (var i = 0; i < this.state.selected_brands.length; i++) {
            if (index == this.state.selected_brands[i]) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    showKits() {
        return this.state.kits.map((kit, index) => {
            return (
                <TouchableHighlight
                    underlayColor={'transparent'}
                    key={index}
                    style={Styles.subfilterCat}
                    onPress={() => this.onPressKitItem(index)}>
                    <Text style={[Styles.txt, this.isSelectedKit(index) ? { color: colors.itemSelected } : {}]}>{kit.name}</Text>
                </TouchableHighlight>
            );
        });
    }

    onPressKitItem = (index) => {
        let flag = false;
        for (var i = 0; i < this.state.selected_kits.length; i++) {
            if (this.state.selected_kits[i] == index) {
                flag = true;
                break;
            }
        }
        let selected_kits = this.state.selected_kits;
        if (flag) {
            for (var i in selected_kits) {
                if (selected_kits[i] == index) {
                    selected_kits.splice(i, 1);
                    break;
                }
            }
        }
        else
            selected_kits.push(index);
        this.setState({ selected_kits: selected_kits });
    }
    isSelectedKit(index) {
        let flag = false;
        for (var i = 0; i < this.state.selected_kits.length; i++) {
            if (index == this.state.selected_kits[i]) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    showProductSizes() {
        return this.state.productSizes.map((productSize, index) => {
            return (
                <TouchableHighlight
                    underlayColor={'transparent'}
                    style={[Styles.sizeItem, this.isSelectedSize(index) ? { borderColor: colors.itemSelected } : {}]}
                    onPress={() => this.onPressSizeItem(index)}>
                    <Text style={[Styles.txt, this.isSelectedSize(index) ? { color: colors.itemSelected } : {}]}>{productSize.name}</Text>
                </TouchableHighlight>
            );
        });
    }

    onPressSizeItem = (index) => {
        let flag = false;
        for (var i = 0; i < this.state.selected_sizes.length; i++) {
            if (this.state.selected_sizes[i] == index) {
                flag = true;
                break;
            }
        }
        let selected_sizes = this.state.selected_sizes;
        if (flag) {
            for (var i in selected_sizes) {
                if (selected_sizes[i] == index) {
                    selected_sizes.splice(i, 1);
                    break;
                }
            }
        }
        else
            selected_sizes.push(index);
        this.setState({ selected_sizes: selected_sizes });
    }
    isSelectedSize(index) {
        let flag = false;
        for (var i = 0; i < this.state.selected_sizes.length; i++) {
            if (index == this.state.selected_sizes[i]) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    showGenders() {
        return this.state.genders.map((gender, index) => {
            return (
                <TouchableHighlight
                    underlayColor={'transparent'}
                    key={index}
                    style={[Styles.subfilterCat, this.state.genders.length - 1 == index ? { borderBottomWidth: 0 } : {}]}
                    onPress={() => this.onPressGenderItem(index)}>
                    <Text style={[Styles.txt, this.isSelectedGender(index) ? { color: colors.itemSelected } : {}]}>{gender.name}</Text>
                </TouchableHighlight>
            );
        });
    }

    onPressGenderItem = (index) => {
        let flag = false;
        for (var i = 0; i < this.state.selected_genders.length; i++) {
            if (this.state.selected_genders[i] == index) {
                flag = true;
                break;
            }
        }
        let selected_genders = this.state.selected_genders;
        if (flag) {
            for (var i in selected_genders) {
                if (selected_genders[i] == index) {
                    selected_genders.splice(i, 1);
                    break;
                }
            }
        }
        else
            selected_genders.push(index);
        this.setState({ selected_genders: selected_genders });
    }
    isSelectedGender(index) {
        let flag = false;
        for (var i = 0; i < this.state.selected_genders.length; i++) {
            if (index == this.state.selected_genders[i]) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    onPressSortbyItem(index) {
        this.setState({ selected_sortBy: index });
    }
    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header title={strings.filter} goback={this.goback} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <View style={Styles.pannel}>
                        <View style={Styles.subupPan}>
                            <TouchableOpacity style={Styles.filterCat} onPress={(index) => this.onPressCat(1)}>
                                <Text style={Styles.txt}>{strings.brand}</Text>
                                <Image source={this.state.cat1 ? arrow_down_icon : arrow_go_icon} />
                            </TouchableOpacity>
                            {this.state.cat1 ? this.showBrands() : null}
                            <TouchableOpacity style={Styles.filterCat} onPress={(index) => this.onPressCat(6)}>
                                <Text style={Styles.txt}>{strings.kit}</Text>
                                <Image source={this.state.cat6 ? arrow_down_icon : arrow_go_icon} />
                            </TouchableOpacity>
                            {this.state.cat6 ? this.showKits() : null}
                            <TouchableOpacity style={Styles.filterCat} onPress={(index) => this.onPressCat(2)}>
                                <Text style={Styles.txt}>{strings.size}</Text>
                                <Image source={this.state.cat2 ? arrow_down_icon : arrow_go_icon} />
                            </TouchableOpacity>
                            {this.state.cat2 ?
                                <View style={Styles.subfilterCat}>
                                    <View style={Styles.Sizeitems}>
                                        {this.showProductSizes()}
                                    </View>
                                </View> : null}
                            <TouchableOpacity style={Styles.filterCat} onPress={(index) => this.onPressCat(3)}>
                                <Text style={Styles.txt}>{strings.popularteams}</Text>
                                <Image source={this.state.cat3 ? arrow_down_icon : arrow_go_icon} />
                            </TouchableOpacity>
                            {this.state.cat3 ? <View>
                                <View style={Styles.subfilterCat}>
                                    <Text style={Styles.txt}>Team1</Text>
                                </View>
                                <View style={Styles.subfilterCat}>
                                    <Text style={Styles.txt}>Team2</Text>
                                </View>
                                <View style={Styles.subfilterCat}>
                                    <Text style={Styles.txt}>Team3</Text>
                                </View>
                            </View> : null}
                            <TouchableOpacity style={[Styles.filterCat, this.state.cat4 ? {} : { borderColor: 'white' }]} onPress={(index) => this.onPressCat(4)}>
                                <Text style={Styles.txt}>{strings.gender}</Text>
                                <Image source={this.state.cat4 ? arrow_down_icon : arrow_go_icon} />
                            </TouchableOpacity>
                            {this.state.cat4 ? this.showGenders() : null}
                        </View>
                        <View style={Styles.subdonwPan}>
                            <TouchableOpacity style={[Styles.filterCat, this.state.cat5 ? {} : { borderColor: 'white' }]} onPress={(index) => this.onPressCat(5)}>
                                <Text style={Styles.txt}>{strings.sortby}</Text>
                                <Image source={this.state.cat5 ? arrow_down_icon : arrow_go_icon} />
                            </TouchableOpacity>
                            {this.state.cat5 ? <View>
                                <TouchableHighlight
                                    underlayColor={'transparent'}
                                    style={Styles.subfilterCat}
                                    onPress={() => this.onPressSortbyItem(0)}>
                                    <Text style={[Styles.txt, this.state.selected_sortBy == 0 ? { color: colors.itemSelected } : {}]}>{strings.lowtohighprice}</Text>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    underlayColor={'transparent'}
                                    style={Styles.subfilterCat}
                                    onPress={() => this.onPressSortbyItem(1)}>
                                    <Text style={[Styles.txt, this.state.selected_sortBy == 1 ? { color: colors.itemSelected } : {}]}>{strings.hightolowprice}</Text>
                                </TouchableHighlight>
                                <TouchableHighlight
                                    underlayColor={'transparent'}
                                    style={[Styles.subfilterCat, { borderColor: 'white' }]}
                                    onPress={() => this.onPressSortbyItem(2)}>
                                    <Text style={[Styles.txt, this.state.selected_sortBy == 2 ? { color: colors.itemSelected } : {}]}>{strings.bestseller}</Text>
                                </TouchableHighlight>
                            </View> : null}
                        </View>
                    </View>
                    {this.renderProgress()}
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    goback = () => {
        console.log('----------------result------------------');
        console.log(this.state.selected_brands);
        let brand = [];
        for (var i = 0; i < this.state.selected_brands.length; i++) {
            brand.push(this.state.brands[this.state.selected_brands[i]]);
        }
        console.log(this.state.selected_kits);
        let kit = [];
        for (var i = 0; i < this.state.selected_kits.length; i++) {
            kit.push(this.state.kits[this.state.selected_kits[i]]);
        }
        let club = [];
        let internationalTeams = [];
        console.log(this.state.selected_sizes);
        let size = [];
        for (var i = 0; i < this.state.selected_sizes.length; i++) {
            size.push(this.state.productSizes[this.state.selected_sizes[i]]);
        }
        console.log(this.state.selected_genders);
        let gender = [];
        for (var i = 0; i < this.state.selected_genders.length; i++) {
            gender.push(this.state.genders[this.state.selected_genders[i]]);
        }
        console.log(this.state.selected_sortBy);
        let sortby = 'ToHigh';
        if (this.state.selected_sortBy == 1)
            sortby = 'ToLow';
        // this.props.dispatch(filterActions.isFilterChanged(false));
        this.props.dispatch(filterActions.setFilter(kit, club, internationalTeams, gender, size, brand, sortby));
        this.props.navigation.pop();
    }
}


const Styles = {
    containerStyle: {
        // alignItems: 'center',
        backgroundColor: '#f0f0f0'
    },
    contentStyle: {
        // position: 'absolute',
        // top: height / 40 * (3.8 + 2),
        // height: height * 9 / 10 - StatusBar.currentHeight,
        // flex: 1,
        // flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        paddingBottom: height / 9,
    },
    statusBarStyle: {
        // backgroundColor: colors.primaryColor
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        // height: height,
        resizeMode: 'contain',
    },
    pannel: {
        borderColor: '#aaaaaa',
        marginTop: height / 10,
        width: width,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        backgroundColor: '#aaaaaa',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    subupPan: {
        backgroundColor: 'white',
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    subdonwPan: {
        marginTop: height / 40,
        backgroundColor: 'white',
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    filterCat: {
        width: width / 20 * 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf: 'center',
        borderBottomWidth: 1,
        borderColor: '#aaaaaa',
        height: height / 12
    },
    subfilterCat: {
        width: width / 20 * 17,
        borderBottomWidth: 1,
        borderColor: '#aaaaaa',
        paddingLeft: width / 20,
        justifyContent: 'center',
        height: height / 12
    },
    txt: {
        fontSize: dimens.small
    },
    Sizeitems: {
        flexDirection: 'row',
        width: width / 20 * 17,
        paddingRight: width / 10,
        justifyContent: 'space-between'
    },
    sizeItem: {
        height: height / 18,
        width: height / 18,
        borderRadius: height / 36,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    }
};

const mapStateToProps = (state) => ({
    Filter: state.get('filter'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(Filter)