import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity, Picker } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./Back_Header";
import Footer from "./Footer";
import strings from "../resources/strings";
import ValidationTextInput from './ValidationTextInput'
import SearchTextInput from "./SearchTextInput";
import * as loginActions from "../actions/login-actions";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import sample from '../assets/cart_sample.png';
import stat_icon from '../assets/icons/stat.png';
import shop_icon from '../assets/icons/shop.png';
import swipe_icon from '../assets/icons/swipe.png';
import Orientation from 'react-native-orientation';
import Toast from 'react-native-toast-native';
const { width, height } = Dimensions.get('window');

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
export class CreditCard extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            isGoneAlready: false
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false))
    }
    
    setSearch = (text) => {

    }
    render() {
        return (

            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header title={strings.creditcard} />
                <Content contentContainerStyle={Styles.contentStyle}>
                    <Text style={Styles.total}>2 items: total of 100 aed</Text>
                    <View style={Styles.pannel}>
                        <Text style={Styles.subtitle}>{strings.cardnumber}</Text>
                        <Text>xxxx-xxxx-xxxx-xxxx</Text>
                    </View>
                    <View style={Styles.pannel}>
                        <Text style={Styles.subtitle}>{strings.name}</Text>
                        <Text>Mohd Ashrafx</Text>
                    </View>
                    <View style={Styles.divpannel}>
                        <View style={[Styles.divcell, { borderRightWidth: 1, borderColor: '#aaaaaa' }]}>
                            <Text style={Styles.subtitle}>{strings.expdate}</Text>
                            <Text>22/12</Text>
                        </View>
                        <View style={Styles.divcell}>
                            <Text style={Styles.subtitle}>{strings.cvr}</Text>
                            <Text >...</Text>
                        </View>
                    </View>
                    <TouchableOpacity style={Styles.btn} onPress={this.onCheckoutSuccess}>
                        <Text style={Styles.btnText}>{strings.checkout}</Text>
                    </TouchableOpacity>
                    {this.renderProgress()}
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }

    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    onCheckoutSuccess = () => {
        this.props.navigation.navigate(consts.CHECKOUTSUCCESS_SCREEN);
    }
}


const Styles = {
    containerStyle: {
        backgroundColor: '#f0f0f0'
    },
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        resizeMode: 'contain',
    },
    btn: {
        marginTop: height / 20,
        width: width / 20 * 17,
        height: height / 40 * 3,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    btnText: {
        fontSize: 16
    },
    pannel: {
        width: width,
        padding: width / 20,
        paddingLeft: width / 20,
        paddingRight: width / 20,
        marginTop: width / 20,
        backgroundColor: 'white'
    },
    divpannel: {
        flexDirection: 'row',
        marginTop: width / 20,
        backgroundColor: 'white',
        width: width
    },
    divcell: {
        padding: width / 20,
        paddingLeft: width / 20,
        width: width / 2,
    },
    subtitle: {
        fontSize: dimens.h2,
        color: 'black',
        width: width / 10 * 9
    },
    total: {
        marginTop: height / 40
    }
};

const mapStateToProps = (state) => ({
    CreditCard: state.get('creditCard'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(CreditCard)