import React, { Component } from "react";
import { Alert, Image, StatusBar, Text, Dimensions, TouchableOpacity } from "react-native";
import { Button, Container, Content, Spinner, View, } from "native-base";
import colors from "../resources/colors";
import { connect } from "react-redux";
import consts from "../const";
import dimens from "../resources/dimens";
import Header from "./ScoreDetail_Header";
import Gameinfo from "./ScoreDetail_Gameinfo";
import Lineup from "./ScoreDetail_Lineup";
import Gamestats from "./ScoreDetail_Gamestats";
import Footer from "./Stat_Footer";
import strings from "../resources/strings";
import * as rootActions from "../actions/root-actions";
import background from '../assets/shop_background.png';
import LeftSideMenu from "./Stat_LeftSideMenu";
import axios from 'axios'
const { width, height } = Dimensions.get('window');
import Orientation from 'react-native-orientation';

// console.ignoredYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
const CacelToken = axios.CancelToken;
let cancel;
export class ScoreDetail extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            gameinfo: true,
            lineup: false,
            gamestats: false,
            table: true,
            topscores: false,
            onLeftSideMenu: false,
            dummy: false
        }
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.dispatch(rootActions.controlProgress(false));
        console.log(this.props.ScoreDetail);
        // this.getMatchDetail(this.props.ScoreDetail.get('seasonId'), this.props.ScoreDetail.get('matchId'));
    }

    componentWillUnmount() {
        // cancel();
    }
    getMatchDetail(seasonId, matchId) {
        const query = `
                    query scoredetail {
                        getFixture(seasonId: 13010, fixtureId: 10343734) {
                            fixture {
                            leagueId
                            seasonId
                            venueId
                            formation {
                                localTeamFormation
                                visitorTeamFormation
                            }
                            time {
                                status
                            }
                            standings {
                                localTeamPosition
                                visitorTeamPosition
                            }
                            eventsInclude {
                                events {
                                teamId
                                type
                                playerName
                                minute
                                }
                            }
                            lineupInclude {
                                players {
                                teamId
                                playerName
                                number
                                position
                                formationPosition
                                posX
                                posY
                                stats {
                                    goals {
                                    scored
                                    conceded
                                    }
                                    fouls {
                                    drawn
                                    comitted
                                    }
                                    cards {
                                    yellowCards
                                    redCards
                                    }
                                    passing {
                                    totalCrosses
                                    crossesAccuracy
                                    passes
                                    passesAccuracy
                                    }
                                    other {
                                    assists
                                    offsides
                                    saves
                                    penScored
                                    penMissed
                                    penSaved
                                    penCommitted
                                    penWon
                                    hitWoodwork
                                    tackles
                                    blocks
                                    interceptions
                                    clearances
                                    minutesPlayed
                                    }
                                }
                                }
                            }
                            }
                            events {
                            localTeamEvents {
                                key
                                value {
                                playerName
                                minute
                                }
                            }
                            visitorTeamEvents {
                                key
                                value {
                                playerName
                                minute
                                }
                            }
                            }
                        }
                    }
                `;
        const variables = {
            cancelToken: new CacelToken(function executor(c) {
                cancel = c;
            })
        };
        try {
            return axios.post('https://victory.softwarehouse.io/graphql', {
                query
            }, variables).then(res => {
                this.count++;
                let data = [];
                data = this.state.leagues;
                data[leagueIndex] = res.data.data.getLeague;
                let flagArray = [];
                for (var i = 0; i < res.data.data.getLeague.seasonsInclude.seasons.length; i++) {
                    flagArray.push(false);
                }
                let tempFlagArray = [];
                tempFlagArray = this.state.showMore;
                tempFlagArray[leagueIndex] = flagArray;
                this.setState({ leagues: data, showMore: tempFlagArray });
                this.forceUpdate();
            });
        } catch (error) {
            return null;
        }
    }

    componentDidUpdate() {
        this.proceed()
    }

    proceed() {

    }

    isObject(obj) {
        return typeof obj === 'object';
    }

    setEmail(text) {
        this.setState({ email: text });
    }
    setPassword(text) {
        this.setState({ password: text });
    }
    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    setSearch = (text) => {

    }
    render() {
        return (
            <Container style={Styles.containerStyle}>
                <Image source={background}
                    style={Styles.imageStyle} />
                <StatusBar style={Styles.statusBarStyle} hidden={true} />
                <Header
                    isChanged={this.state.dummy}
                    isGameinfo={this.state.gameinfo ? true : false}
                    isLineup={this.state.lineup ? true : false}
                    isGamestats={this.state.gamestats ? true : false}
                    onPressOpt={this.onPressOpt}
                    onPressBell={this.onPressBell}
                    onPressGameinfo={this.onPressGameinfo}
                    onPressLineup={this.onPressLineup}
                    onPressGamestats={this.onPressGamestats} />
                {this.state.gameinfo ?
                    <Gameinfo isChanged={this.state.dummy} />
                    : null}
                {this.state.lineup ?
                    <Lineup isChanged={this.state.dummy} /> : null}
                {this.state.gamestats ?
                    <Gamestats isChanged={this.state.dummy} />
                    : null}
                <Footer
                    isChanged={this.state.dummy}
                    navigation={this.props.navigation} />
                {this.state.onLeftSideMenu ?
                    <LeftSideMenu
                        refresh={this.refresh}
                        navigation={this.props.navigation}
                        hideLeftMenu={this.hideLeftMenu} />
                    : null}
            </Container>
        );
    }

    renderProgress() {
        if (this.props.root.get('progress')) {
            return this.spinner()
        } else {
            return null;
        }
    }
    renderTabPannel() {
        if (this.state.gameinfo)
            return AllScores;
    }
    spinner() {
        return (
            <Spinner
                color={colors.secondColor}
                animating={true}
                size={'large'}
                style={Styles.progressStyle} />
        )
    }

    refresh = () => {
        if (strings.getLanguage() === 'en') {
            strings.setLanguage('ar')
        }
        else {
            strings.setLanguage('en')
        }
        this.props.dispatch(rootActions.switchLanguage(strings.getLanguage()));
        this.setState({ dummy: !this.state.dummy })
    }

    validateEmail = (text: string): boolean => consts.EMAIL_REGEX.test(text);

    validatePassword = (text: string): boolean => text.length >= consts.MIN_PASSWORD_LENGTH;

    onPressBag = () => {
        this.props.navigation.navigate(consts.CART_SCREEN);
    }
    onPressOpt = () => {
        this.setState({ onLeftSideMenu: true });
    }
    onFilterPress = () => {
        this.props.navigation.navigate(consts.FILTER_SCREEN);
    }
    onPressWishList = () => {
        this.props.navigation.navigate(consts.WISHLIST_SCREEN);
    }
    onPressGameinfo = () => {
        this.setState({
            gameinfo: true,
            lineup: false,
            gamestats: false,
        });
    }
    onPressLineup = () => {
        this.setState({
            gameinfo: false,
            lineup: true,
            gamestats: false,
        });
    }
    hideLeftMenu = () => {
        this.setState({ onLeftSideMenu: false });
    }
    onPressGamestats = () => {
        this.setState({
            gameinfo: false,
            lineup: false,
            gamestats: true,
        });
    }
    onPressTable = () => {
        this.setState({
            table: true,
            topscores: false
        });
    }
    onPressTopScores = () => {
        this.setState({
            table: false,
            topscores: true
        });
    }
}


const Styles = {
    containerStyle: {
        backgroundColor: '#eeeeee',
        paddingBottom: height / 9,
    },
    contentStyle: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    statusBarStyle: {
    },
    progressStyle: {
        position: 'absolute',
        top: height / 3,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    },
    imageStyle: {
        position: 'absolute',
        top: 0,
        width: width,
        resizeMode: 'contain',
    },
};

const mapStateToProps = (state) => ({
    ScoreDetail: state.get('scoreDetail'),
    root: state.get('root'),
});

export default connect(mapStateToProps)(ScoreDetail)