import React from "react";
import { Text,  View, } from "native-base";
import {  Image, Dimensions, TouchableHighlight } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import avatar from '../assets/avatar.png'
import colors from "../resources/colors";
import LinearGradient from 'react-native-linear-gradient';
import consts from "../const";
const { width, height } = Dimensions.get('window');


export default class Stat_LeftSideMenu extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            }),
            indeterminate: false,
            width: width,
            dummy: true
        };
    }
    componentDidMount() {
        this.animate();
    }

    animate() {
        let progress = width;
        this.setState({ width: progress });
        setTimeout(() => {
            this.setState({ indeterminate: false });
            let intervalId = setInterval(() => {
                progress = progress - 100;
                if (progress < width / 3) {
                    progress = width / 3
                    this.setState({ indeterminate: true })
                    this.setState({ width: progress });
                    clearInterval(intervalId);
                    return;
                }
                this.setState({ width: progress });
            }, 50);
        }, 10);
    }
    onPressBack = () => {
        if (this.state.indeterminate) {
            this.setState({ indeterminate: false })
            let progress = width / 3;
            this.setState({ width: progress });
            setTimeout(() => {
                let intervalId = setInterval(() => {
                    progress = progress + 100;
                    if (progress > width) {
                        progress = width
                        this.setState({ width: progress });
                        clearInterval(intervalId);
                        this.props.hideLeftMenu();
                        return
                    }
                    this.setState({ width: progress });
                }, 50);
            }, 10);
        }
    }
    onPressProfile = () => {
        this.props.hideLeftMenu();
        this.props.navigation.navigate(consts.PROFILE_SCREEN)
    }
    onPressLanguageSwitch = () => {
        this.props.refresh();
        this.forceUpdate();
    }
    render() {
        return (
            <View style={Styles.containerStyle}>
                <View
                    style={Styles.contentStyle}>
                    <LinearGradient
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 1 }}
                        colors={['#81d7af', '#8091da']}
                        style={Styles.outter}>
                        <View style={Styles.inner}>
                            <Image style={Styles.profileImg} source={avatar} />
                        </View>
                    </LinearGradient>
                    <TouchableHighlight
                        onPress={this.onPressProfile}
                        underlayColor='transparent'
                        style={[Styles.itemRow, { borderTopWidth: 1 }]}>
                        <Text style={{ color: colors.itemContent }}>{strings.profile}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor='transparent'
                        style={Styles.itemRow}>
                        <Text style={{ color: colors.itemContent }}>{strings.myfavorite}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor='transparent'
                        style={Styles.itemRow}>
                        <Text style={{ color: colors.itemContent }}>{strings.allscores}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor='transparent'
                        style={Styles.itemRow}>
                        <Text style={{ color: colors.itemContent }}>{strings.standings}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor='transparent'
                        style={Styles.itemRow}>
                        <Text style={{ color: colors.itemContent }}>{strings.settings}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor='transparent'
                        style={Styles.itemRow}>
                        <Text style={{ color: colors.itemContent }}>{strings.search}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        underlayColor='transparent'
                        style={Styles.itemRow}
                        onPress={this.onPressLanguageSwitch}>
                        <Text style={{ color: colors.itemContent }} style={strings.getLanguage() === 'en' ? { textAlign: 'left' } : { textAlign: 'right' }}>{strings.lang}</Text>
                    </TouchableHighlight>
                </View>
                <TouchableHighlight
                    style={[Styles.backStyle, { width: this.state.width }]}
                    underlayColor='#00000020'
                    onPress={this.onPressBack}>
                    <View style={Styles.backStyle} />
                </TouchableHighlight>
            </View>
        );
    }
}

const Styles = {
    containerStyle: {
        flexDirection: 'row',
        position: 'absolute',
        top: 0,
        width: width,
        height: height,
        backgroundColor: '#00000020',
        justifyContent: 'flex-end',
        zIndex: 999999
    },
    contentStyle: {
        width: width / 3 * 2,
        height: height,
        backgroundColor: 'white',
        paddingTop: width / 10,
    },
    backStyle: {
        backgroundColor: '#00000020',
        height: height
    },
    outter: {
        width: width / 4,
        height: width / 4,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: width / 8,
        marginLeft: width / 20,
        marginBottom: width / 10,
    },
    inner: {
        width: width / 4 - 4,
        height: width / 4 - 4,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: width / 8 - 2
    },
    profileImg: {
        width: width / 4 - 4,
        height: width / 4 - 4,
        alignItems: 'center',
        borderRadius: width / 8 - 2
    },
    itemRow: {
        paddingLeft: width / 20,
        paddingRight: width / 20,
        borderBottomWidth: 1,
        borderColor: '#aaaaaa',
        paddingTop: height / 40,
        paddingBottom: height / 50,
        alignSelf: 'stretch',
    }
};

Stat_LeftSideMenu.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};