import React from "react";
import { Text, View, } from "native-base";
import { Image, Dimensions, TextInput, ScrollView, TouchableHighlight, AsyncStorage } from "react-native";
import strings from "../resources/strings";
import PropTypes from "prop-types";
import * as Immutable from "../../node_modules/immutable/dist/immutable";
import ImmutableComponent from "./ImmutableComponent";
import opt_icon from '../assets/icons/opt.png';
import { connect } from "react-redux";
import colors from "../resources/colors";
import consts from "../const";
import dimens from "../resources/dimens";
import LinearGradient from 'react-native-linear-gradient';
import SearchTextInput from "./SearchTextInput";
import rm_icon from '../assets/icons/rm.png';
const { width, height } = Dimensions.get('window');


export class Stat_RightSideMenu extends ImmutableComponent {

    constructor(props: {}) {
        super(props);

        this.state = {
            data: Immutable.Map({
                error: "",
                showDefaultValue: true,
            }),
            indeterminate: false,
            width: width
        };
    }
    componentDidMount() {
        this.animate();
    }
    
    animate() {
        let progress = width;
        this.setState({ width: progress });
        setTimeout(() => {
            this.setState({ indeterminate: false });
            let intervalId = setInterval(() => {
                progress = progress - 100;
                if (progress < width / 5) {
                    progress = width / 5
                    this.setState({ indeterminate: true })
                    this.setState({ width: progress });
                    clearInterval(intervalId);
                    return;
                }
                this.setState({ width: progress });
            }, 50);
        }, 10);
        // this.setState({ width: width / 2 })
    }
    onPressBack = () => {
        if (this.state.indeterminate) {
            this.setState({ indeterminate: false })
            let progress = width / 5;
            this.setState({ width: progress });
            setTimeout(() => {
                let intervalId = setInterval(() => {
                    progress = progress + 200;
                    // Alert.alert('test')
                    if (progress > width) {
                        progress = width
                        this.setState({ width: progress });
                        clearInterval(intervalId);
                        this.props.hideRightMenu();
                        // this.props.parent.setState({ onLeftSideMenu: false });
                        return
                    }
                    this.setState({ width: progress });
                    // this.forceUpdate();
                }, 50);
            }, 10);
        }
    }
    setSearch = (text) => {

    }
    onPressSelectLeague = () => {
        // this.props.hideRightMenu();
        this.props.navigation.navigate(consts.SELECTLEAGUE_SCREEN);
    }
    onPressSelectTeam = () => {
        // this.props.hideRightMenu();
        this.props.navigation.navigate(consts.SELECTTEAM_SCREEN);
    }
    getMyFavoriteLeagues() {
        let myFavLeagues = [];
        myFavLeagues = this.props.root.get('favLeagues');
        return myFavLeagues.map((league, index) => {
            return (
                <View style={Styles.itemRow}>
                    <Image style={Styles.leagueImg} source={rm_icon} />
                    <View style={Styles.itemGroup}>
                        <Text style={Styles.leagueName}>{league[0]}</Text>
                        <TouchableHighlight underlayColor='transparent' style={Styles.optBtn}>
                            <Image style={Styles.optImg} source={opt_icon} />
                        </TouchableHighlight>
                    </View>
                </View>
            );
        });
    }
    render() {
        return (
            <View style={Styles.containerStyle}>
                <TouchableHighlight
                    style={[Styles.backStyle, { width: this.state.width }]}
                    underlayColor='#00000020'
                    onPress={this.onPressBack}>
                    <View style={Styles.backStyle} />
                </TouchableHighlight>
                <View
                    style={Styles.contentStyle}>
                    <ScrollView>
                        <SearchTextInput
                            onChangeText={(text) => this.setSearch(text)}
                            placeholder={strings.search}
                            color={'black'}
                            secureTextEntry={false} />
                        <View style={Styles.classTitle}>
                            <Text style={Styles.classTitleText}>{strings.favoriteleague}</Text>
                            <TouchableHighlight underlayColor='transparent' style={Styles.plusGroup} onPress={this.onPressSelectLeague}>
                                <LinearGradient
                                    start={{ x: 0, y: 1 }}
                                    end={{ x: 1, y: 1 }}
                                    colors={['#81d7af', '#8091da']}
                                    style={Styles.plus}>
                                    <Text style={Styles.plusText}>+</Text>
                                </LinearGradient>
                            </TouchableHighlight>
                        </View>
                        {this.getMyFavoriteLeagues()}
                        <View style={{ height: width / 10 }} />
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const Styles = {
    containerStyle: {
        flexDirection: 'row',
        position: 'absolute',
        top: 0,
        width: width,
        height: height,
        backgroundColor: '#00000020',
        justifyContent: 'flex-start',
        zIndex: 100000
    },
    contentStyle: {
        width: width / 5 * 4,
        height: height,
        backgroundColor: 'white',
        paddingTop: width / 10,
    },
    backStyle: {
        backgroundColor: '#00000020',
        // width: width / 3,
        height: height
    },
    classTitle: {
        flexDirection: 'row',
        paddingLeft: width / 20,
        paddingRight: width / 20,
        backgroundColor: colors.itemNameBack,
        paddingTop: height / 70,
        paddingBottom: height / 70,
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        marginTop: width / 10,
        alignItems: 'center'
    },
    plusGroup: {
        width: width / 15,
        height: width / 15,
        borderRadius: width / 30,
        justifyContent: 'center',
        alignItems: 'center',
    },
    plus: {
        width: width / 15,
        height: width / 15,
        borderRadius: width / 15,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0
    },
    plusText: {
        color: 'white',
        fontSize: dimens.itemTitle,
        padding: 0,
    },
    itemRow: {
        flexDirection: 'row',
        paddingLeft: width / 20,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    leagueImg: {
        width: width / 15,
        height: width / 15,
        resizeMode: 'stretch'
    },
    itemGroup: {
        paddingRight: width / 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderColor: '#aaaaaa',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: width / 5 * 3.2,
        paddingTop: height / 50,
        paddingBottom: height / 50
    },
    optBtn: {
        width: width / 20,
        height: width / 20
    },
    optImg: {
        width: width / 20,
        height: width / 20,
        resizeMode: 'contain',
        tintColor: colors.itemContent
    },
    classTitleText: {
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.itemTitle - 1,
        color: colors.itemContent
    },
    leagueName: {
        fontFamily: 'Roboto-Regular',
        fontSize: dimens.itemTitle - 1,
        color: colors.itemContent
    }
};

const mapStateToProps = (state) => ({
    root: state.get('root'),
});

Stat_RightSideMenu.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    defaultValue: PropTypes.string
};


export default connect(mapStateToProps)(Stat_RightSideMenu)