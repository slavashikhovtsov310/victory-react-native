import Base64 from "./utils/Base64";
import consts from "./const";
import queryString from "query-string";
import { Alert } from 'react-native';
import FormData from 'FormData';
import axios from 'axios';

const domain = 'https://victory.softwarehouse.io/';

export function login(email, password) {
  var formData = new FormData();
  formData.append('email', email);
  formData.append('password', password);
  return fetch('https://victory.softwarehouse.io/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data'
    },
    body: formData
  })
    .then((response) => response.json())
    .then((responsJson) => {
      // Alert.alert('api test: ' + email + ' ' + password)
      // Alert.alert('api result: ' + responsJson.status);
      return responsJson;
    })
    .catch((error) => {
      console.log(error);
    });
}


export function signup(email, password) {
  Alert.alert('api: ' + email + ' ' + password);
  var formData = new FormData();
  formData.append('email', email);
  formData.append('password', password);
  return fetch('https://victory.softwarehouse.io/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data'
    },
    body: formData
  })
    .then((response) => response.json())
    .then((responsJson) => {
      // Alert.alert('api test: ' + email + ' ' + password)
      // Alert.alert('api result: ' + responsJson.status);
      return responsJson;
    })
    .catch((error) => {
      console.log(error);
    });
}

export function logOut(authId, username, password) {
  return fetch(`https://victory.softwarehouse.io/`, {
    method: 'DELETE',
    headers: getAuthHeader(username, password)
  })
    .then((user) => {
      return user.json();
    })
    .catch((error) => {
      console.log(error);
    });
}
export function addToCart(ItemID, ProductSizes, CustomizeName, CustomizeNumber, cookie) {
  var formData = new FormData();
  formData.append('ItemID', ItemID);
  formData.append('ProductSizes', ProductSizes);
  formData.append('CustomizeName', CustomizeName);
  formData.append('CustomizeNumber', CustomizeNumber);//}, 
  return new Promise((resolve, reject) => {
    fetch('https://victory.softwarehouse.io/cart/add', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      body: formData
    })
      .then((res) => {
        // return responsJson;
        var newCookie = res.headers.get('set-cookie');
        // console.log('test: ', newCookie);
        resolve(newCookie);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });

    // console.log('---------test----------')
    // var req = new XMLHttpRequest();
    // req.open('POST', 'https://victory.softwarehouse.io/cart/add', false);
    // req.send(null);
    // var headers = req.getAllResponseHeaders();
    // console.log(headers);
  });
}

