// import { ApolloClient, createNetworkInterface } from 'react-apollo';
// import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';
// import { ApolloProvider } from 'react-apollo';
// import ApolloClient, { createNetworkInterface } from 'apollo-client';
import { ApolloClient, createNetworkInterface, ApolloProvider } from 'react-apollo'
// import ApolloClient from 'apollo-boost'
// import { createNetworkInterface } from 'apollo-client';

// const client = new ApolloClient({ uri: 'https://victory.softwarehouse.io/graphiql' })
const client = new ApolloClient({
    networkInterface: createNetworkInterface({ uri: 'https://victory.softwarehouse.io/graphiql' }),
})
export default client;