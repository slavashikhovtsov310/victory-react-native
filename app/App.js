import React, { Component } from "react";
import { Provider } from "react-redux";
import { ApolloClient, ApolloProvider, createNetworkInterface } from 'react-apollo';
import configureStore from "./store/configureStore";
import Navigation from "./components/Navigation.js";

const store = configureStore();

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}